## Definición
Mapear desde nuestro concepto o modelemos, expresaremos la semnatica en:

* Servicios:
* Entidades: Es algo que es independiente y existe por el solo hecho de ser, por ejemplo un usuario, una venta, etc. 
* Objetos de valor (value objects): Son elementos que representan una entidad simple y es única en nuestra aplicación
por ejemplo los catálogos, poseen atributos pero no una identidad por si mismas.

* Agregados: una colección de objetos que existen o trabajan en conjunto por una Entidad base, por ejemplo, podemos tener un carro
el cual se mueve por medio de llantas, tiene puertas, parabrizas, etc, en este caso el agregado es el carro y funciona para agregar
más entidades a el y trabajaen así en conjunto.

* Repositorios: Los agregados a su vez seran accedidos/almacenados dentro de repositorios, los cuales son nuestra interfaz con el sistema
donde serán obtenidos, por ejemplo una base de datos.

* Evento de dominio: Cada vez que se realizan cambios a los estados de las entidades se publican eventos los cuales indican al
sistema que acciones han ocurrido en el.

* Factorias: son clases o métodos especializados en crear objetos , se usan para crear Entities, Value Objects, y Agregates.

Todos estos conceptos apoyan a la arquitectura en capas