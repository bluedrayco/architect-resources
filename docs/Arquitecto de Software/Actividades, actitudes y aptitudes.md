
### Gestión de los requisitos no funcionales y definición de la arquitectura de software

* Debe de pensar en los requisitos del sistema y como medirlos, no basta con tener un requisito funcional de "debe ser rápido".

* Debe velar por el rendimiento, escalabilidad, disponibilidad, auditoría en los desarrollos de software a su cargo.

* Debe generar y diseñar la introducción de la estructura, directrices, principio y liderazgo de los aspectos técnicos de un proyecto de software.

* Debe tener una idea de como implementar su arquitectura, pero debe estar preparado para aceptar cualquier otra forma que cumpla tambien los objetivos

### Selección de la tecnología

* Debe analizar si la tecnología elegida funciona realmente y se adapta o no al problema que se quiere atacar.

* Debe evaluar costo, licencias, compatibilidad, interoperabilidad,política de actualizaciones de las tecnologías que propone.

* Debe asumir la propiedad del proceso de selección de la tecnología y con ello debe ser responsable del riesgo técnico que esto implica.

### Mejora continua de la Arquitectura

* Debe obtener feedback para la mejora de los sistemas.

* Debe generar procesos de evaluación de los sistemas para saber si cumplen las expectativas del cliente y reducir el riesgo de fracaso del proyecto.

### Lider y formador

* Debe asumir la dirección técnica, asegurandose que todos los aspectos de la arquitectura se implementen de manera correcta.

* Un arquitecto sugiere, no dicta la implamentación porque el programador es el encargado de la inventiva y responsabilidad creativa.

* Debe proporcionar orientación técnica y dar apoyo al equipo de desarrollo, debe estar preparado para entrenar al equipo en las tecnologías seleccionadas 
y abierto a sugerencias.

* Debe de estar listo para renunciar al crédito por las mejoras sugeridas en caso que sean mejores que las suyas.

### Aseguramiento de la calidad

* Debe apoyarse de procesos de integración continua que utilicen herramientas automatizadas de análisis de código fuente, pruebas unitarias, 
cobertura de código fuente, para asegurar el cumplimiento de las normas y políticas que se definieron.

![Arquitectura](./arquitectura_software.jpg "Arquitectura de Software")