#¿Qué es RabbitMQ?

* Es un sistema de mensajería multiprotocolo
* Open source
* Escrito en Erlang/OTP
* Poliglota
    * Java
    * Node JS
    * Erlang
    * PHP
    * Ruby
    * .Net
    * Haskell
    * COBOL

#Protocolos soportados por RabbitMQ
![Protocolos RabbitMQ](./RabbitMQProtocols.png "Protocolos")

* AMQP (Advanced Message Queuing Protocol)
* MQTT (Message Queue Telemetry Transport)
* STOMP (Simple/Streaming Text Oriented Messaging Protocol)

#Empresas que utilizan RabbitMQ actualmente

* Instagram
* Indeed.com
* Telefónica
* Mercado Libre
* NHS
* Mozilla

#Plataformas soportadas

* Windows
* Mac OS
* Linux

#Actores que participan en RabbitMQ

##Exchange
###Tipos

* Fanout
* Direct
* Topic
*

##Productor
##Consumer
##Queue