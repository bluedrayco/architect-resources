##Desarrollo en Cascada

![Etapas cascada](./desarrollo_cascada.png "Etapas")

###Análisis de Requerimientos
Se debe entender que se requiere construir, o cuál es la necesidad del negocio, entendiendo esto podremos
generar Requerimientos Funcionales, Requerimientos No Funcionales y Requerimientos del negocio.

###Diseño de la solución
Analizar el problema y plantear posibles soluciones, el resultado en esta etapa es el detalle de la solución,
en esta etapa es en donde se genera el modelado.

###Desarrollo y evolución
Implementación de la solución, aqui se establecen los "criterios de aceptación", lo cual es la forma en como
se probará o que se va a probar en la solución, tambien se realizan pruebas unitarias, la salida es un artefacto
de software.

###Despliegue
Con apoyo en Infraestructura y operaciones liberar la solución en un ambiente productivo, es aqui donde verificamos
lo que necesitamos para el despliegue del artefacto de software.

###Mantenimiento de la Solución
En esta etapa se detectan errores y se adicionan nuevas funcionalidades a la solución.

###Software Legado
Cuando se determine que ya no posee errores el software o que ya no existen nuevos requerimientos que atacar es cuando
se da por finalizado el ciclo de vida del sistema y el sistema entonces se vuelve un sistema legado.


