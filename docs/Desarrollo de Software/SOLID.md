## Definición
es un acrónimo mnemónico introducido por *Robert C. Martin*​ a comienzos de la década del 2000​ que 
representa cinco principios básicos de la programación orientada a objetos y el diseño. Cuando estos 
principios se aplican en conjunto es más probable que un desarrollador cree un sistema que sea fácil de 
mantener y ampliar con el tiempo.​ Los principios SOLID son guías que pueden ser aplicadas en el desarrollo
de software para eliminar código sucio provocando que el programador tenga que refactorizar el código 
fuente hasta que sea legible y extensible. 

### Single Responsability Principle
Este principio habla de que una clase debe tener solo una razón de existir, es decir, solo debe tener una única
responsabilidad, veamos un ejemplo:

Se tiene una clase rectangulo, esta clase puede sumar las áreas y también posee los datos del rectángulo, el programa en cuestión
pretende calcular las áreas y perímetros de diversos rectángulos:

```php
<?php

class Rectangle {

    private $sides = 4;
    private $height;
    private $width;

    public function __construct($height, $width) {
        $this->height = $height;
        $this->width = $width;
    }

    public static function sumAreas(array $rectangulos) {
        $area = 0;
        foreach ($rectangulos as $rect) {
            $area += $rect->getHeight() * $rect->getWidth();
        }
        return $area;
    }

    public static function sumPerimetros(array $rectangulos) {
        $perimetro = 0;
        foreach ($rectangulos as $rect) {
            $perimetro += 2 * $rect->getHeight() + 2 * $rect->getWidth();
        }
        return $perimetro;
    }

    function getHeight() {
        return $this->height;
    }

    function getWidth() {
        return $this->width;
    }

    function setHeight($height) {
        $this->height = $height;
    }

    function setWidth($width) {
        $this->width = $width;
    }

}

$rectangulos =[
    new Rectangle(10,5),
    new Rectangle(4,6),
    new Rectangle(5,1),
    new Rectangle(8,9)
];

$sumAreas = Rectangle::sumAreas($rectangulos);
$sumPerimetros = Rectangle::sumPerimetros($rectangulos);

echo "el Area es: {$sumAreas} y el perímetro es: {$sumPerimetros}\n";
```
Como se puede apreciar el programa funciona correctamente, el problema radica en que aunque el rectángulo tiene
alto y ancho como datos tambien realiza las operaciones de la suma de áreas y de perímetros por lo que debemos separar
esa funcionalidad en dos clases, una que pueda calcular solo áreas y otra solo perímetros:

```php
<?php

class Rectangulo {

    private $sides = 4;
    public $height;
    public $width;

    public function __construct($height, $width) {
        $this->height = $height;
        $this->width = $width;
    }
}

class AreaOperations {

    public static function sumAreas(array $rectangulos) {
        $area = 0;
        foreach ($rectangulos as $rect) {
            $area += $rect->height * $rect->width;
        }
        return $area;
    }

}

class PerimeterOperations {

    public static function sumPerimetros(array $rectangulos) {
        $perimetro = 0;
        foreach ($rectangulos as $rect) {
            $perimetro += 2 * $rect->height + 2 * $rect->width;
        }
        return $perimetro;
    }

}

$rectangulos = [
    new Rectangulo(10, 5),
    new Rectangulo(4, 6),
    new Rectangulo(5, 1),
    new Rectangulo(8, 9)
];

$sumAreas = AreaOperations::sumAreas($rectangulos);
$sumPerimetros = PerimeterOperations::sumPerimetros($rectangulos);

echo "el Area es: {$sumAreas} y el perímetro es: {$sumPerimetros}\n";
```

Como podemos ver tenemos tres clases que cada una tiene una única responsabilidad:

* **Rectangulo** es una clase que solo tiene como única responsabilidad representar un rectángulo.
* **AreaOperations** es una clase que solo tiene como única responsabilidad el cálculo de áreas.
* **PerimeterOperations** es una clase que solo tiene como única responsabilidad el cáluclo de perímetros.

Con lo cuál nuestro código cumple con este primer principio.

### Open/Close Principle
Este principio estipula que:

*Software entities (classes, modules, functions, etc.) should be open for extension, but closed for 
modification.*

Es decir, se debe pensar en realizar una funcionalidad de tal manera que las peticiones de cambio a nuestro programa
hagan que sea capaz de añadir funcionalidad sin necesidad de modificar la ya existente, esto *siempre que sea posible*.

Esto se logra por medio de **contratos**, estos contratos entre los componentes nos dan la pauta para establecer
acuerdos de que deberán llevar los componentes de software y que *linenamientos deben respetar*.

En el caso de la programación orientada a objetos los contratos pueden ser establecidos de las siguientes formas:

* **Herencia de una clase base**: Esta por lo regular es utilizada cuando pretendemos hacer una generalización de un
funcionamiento (clase base o clase padre) y posteriormente encontramos una especialización de esa clase base 
(clase derivada o clase hija), en esta conocemos los atributos y métodos así como el comportamiento de esos métodos
a la perfección.

* **Herencia de una clase Abstracta**: Esta se utiliza cuando no conocemos en su totalidad la funcionalidad, sin embargo
si hemos identificado que las funcionalidades en cuestión parten de un funcionamiento base.

* **Implementación de una interfaz**: En esta la clase que implementa la interfaz es la responsable de cumplir
lo que la interfaz estipula, este contrato es el que posteriormente se podrá utilizar y sin necesidad de tener conocimiento
quien lo implementa. 


Partiendo del ejemplo anterior aplicaremos las tres estrategias para ver cual es más conveniente en el caso en el
que ahora se quisieran implementar diferentes figuras, no solo rectangulos:

####Ejemplo utilizando Herencia de una clase base

Ahora usaremos una clase base, de esta extenderán nuestras nuevas clases derivadas rectangulo y triangulo

```php
<?php

class GeometricShape {

    public function area(){
        echo "area de la figura";
    }

    public function perimetro(){
        echo "perimetro de la figura";
    }
}

class TrianguloEquilatero extends GeometricShape{
    private $sides = 3;
    private $sideLength;
    
    public function __construct($sideLength) {
        $this->sideLength=$sideLength;
    }
    public function area() {
        return sqrt(3)*pow($this->sideLength,2)/4;
    }

    public function perimetro() {
        return $this->sideLength*3;
    }
}
class Rectangulo extends GeometricShape{

    private $sides = 4;
    private $height;
    private $width;

    public function __construct($height, $width) {
        $this->height = $height;
        $this->width = $width;
    }

    public function area() {
        return $this->width*$this->height;
    }

    public function perimetro() {
        return 2 * $this->height + 2 * $this->width;
    }

}

class AreaOperations {

    public static function sumAreas(array $figuras) {
        $area = 0;
        foreach ($figuras as $fig) {
            $area += $fig->area();
        }
        return $area;
    }

}

class PerimeterOperations {

    public static function sumPerimetros(array $figuras) {
        $perimetro = 0;
        foreach ($figuras as $fig) {
            $perimetro += $fig->perimetro();
        }
        return $perimetro;
    }

}

$figuras = [
    new Rectangulo(10, 5),
    new Rectangulo(4, 6),
    new Rectangulo(5, 1),
    new Rectangulo(8, 9),
    new TrianguloEquilatero(9),
    new TrianguloEquilatero(12)
];

$sumAreas = AreaOperations::sumAreas($figuras);
$sumPerimetros = PerimeterOperations::sumPerimetros($figuras);

echo "el Area es: {$sumAreas} y el perímetro es: {$sumPerimetros}\n";
```

Al tener un contrato llamado **GeometricShape** el método de la clase **AreaOperations** y el método de la clase 
**PerimeterOperations** podrá hacer sumas de perímetros sin necesidad de conocer si son triangulos o perímetros
los ingresados, esto ayudará el día de mañana a si queremos implementar un circulo solo bastará con heredar
de la clase GeometricShape, las desventajas de esta primera forma de atacar este principio son las siguientes: 

* Si recordamos solo podemos heredar de una sola clase por lo que perdemos la capacidad de herencia.

* De esta forma no esta obligada la clase hija a redefinir los métodos del padre por lo que puedo olvidar el implementar
mi propio método para obtener el perímetro y el área de mi figura y se ejecutaría la funcionalidad del padre, lo que
lleva al siguiente punto.

* La más importante es que al implementar esta estrategia violamos otro principio **Principio de substitución de liskov**,
el cuál se verá más adelante que una clase hija deberá comportarse igual al padre, en este caso al redefinir los
métodos de la clase padre estamos violando ese principio.

Por estos puntos es funcional este método pero ineficaz para solventar el principio.

####Ejemplo utilizando Herencia de una clase abstracta

Este método solo difiere en que la clase base es una clase abstracta.

```php
<?php

abstract class GeometricShape {

    public function area(){
        echo "area de la figura";
    }

    abstract public function perimetro();
}

class TrianguloEquilatero extends GeometricShape{
    private $sides = 3;
    private $sideLength;
    
    public function __construct($sideLength) {
        $this->sideLength=$sideLength;
    }
    public function area() {
        return sqrt(3)*pow($this->sideLength,2)/4;
    }

    public function perimetro() {
        return $this->sideLength*3;
    }
}
class Rectangulo extends GeometricShape{

    private $sides = 4;
    private $height;
    private $width;

    public function __construct($height, $width) {
        $this->height = $height;
        $this->width = $width;
    }

    public function area() {
        return $this->width*$this->height;
    }

    public function perimetro() {
        return 2 * $this->height + 2 * $this->width;
    }

}

class AreaOperations {

    public static function sumAreas(array $figuras) {
        $area = 0;
        foreach ($figuras as $fig) {
            $area += $fig->area();
        }
        return $area;
    }

}

class PerimeterOperations {

    public static function sumPerimetros(array $figuras) {
        $perimetro = 0;
        foreach ($figuras as $fig) {
            $perimetro += $fig->perimetro();
        }
        return $perimetro;
    }

}

$figuras = [
    new Rectangulo(10, 5),
    new Rectangulo(4, 6),
    new Rectangulo(5, 1),
    new Rectangulo(8, 9),
    new TrianguloEquilatero(9),
    new TrianguloEquilatero(12)
];

$sumAreas = AreaOperations::sumAreas($figuras);
$sumPerimetros = PerimeterOperations::sumPerimetros($figuras);

echo "el Area es: {$sumAreas} y el perímetro es: {$sumPerimetros}\n";
```

En el caso de esta solución podemos ver que al ser una clase abstracta no todos los métodos tienen una
definición, esta solución es conveniente cuando se tiene definición de algunos métodos o procesos, por ejemplo si
se pudiera definir el perímetro de la misma forma para todas las figuras podríamos sin ningún problema usar este método.


####Ejemplo utilizando Interfaces

```php
<?php

interface IGeometricShape {

    public function area();

    public function perimetro();
}

class TrianguloEquilatero implements IGeometricShape{
    private $sides = 3;
    private $sideLength;
    
    public function __construct($sideLength) {
        $this->sideLength=$sideLength;
    }
    public function area() {
        return sqrt(3)*pow($this->sideLength,2)/4;
    }

    public function perimetro() {
        return $this->sideLength*3;
    }
}
class Rectangulo implements IGeometricShape{

    private $sides = 4;
    private $height;
    private $width;

    public function __construct($height, $width) {
        $this->height = $height;
        $this->width = $width;
    }

    public function area() {
        return $this->width*$this->height;
    }

    public function perimetro() {
        return 2 * $this->height + 2 * $this->width;
    }

}

class AreaOperations {

    public static function sumAreas(array $figuras) {
        $area = 0;
        foreach ($figuras as $fig) {
            $area += $fig->area();
        }
        return $area;
    }

}

class PerimeterOperations {

    public static function sumPerimetros(array $figuras) {
        $perimetro = 0;
        foreach ($figuras as $fig) {
            $perimetro += $fig->perimetro();
        }
        return $perimetro;
    }

}

$figuras = [
    new Rectangulo(10, 5),
    new Rectangulo(4, 6),
    new Rectangulo(5, 1),
    new Rectangulo(8, 9),
    new TrianguloEquilatero(9),
    new TrianguloEquilatero(12)
];

$sumAreas = AreaOperations::sumAreas($figuras);
$sumPerimetros = PerimeterOperations::sumPerimetros($figuras);

echo "el Area es: {$sumAreas} y el perímetro es: {$sumPerimetros}\n";
```

Con esta solución no afectamos funcionalidades previas, ya que la interfaz solo puede definir el contrato más no
la implementación, eso queda a cargo de cada tipo de figura en este ejemplo,podemos aun heredar de
otra clase ya que no hemos agotado esta opción,  tampoco se viola el principio de substitucion
de Liskov, por lo que para este caso de estudio es la **mejor opción**.


### Liskov substitution Principle
principio de sustitución de Liskov.
Fue propuesto por Barvara Liskov y Jeannette Marie Wing en la década de los noventa,
aunque años antes, en el 1987, Liskov había hablado de él en una conferencia.

#### Definición formal
*Let q(x) be a property provable about objects x of type T. Then q(y) should be provable 
for objects y of type S, where S is a subtype of T.*

En programación orientado a objetos esto puede ser interpretado de la siguiente manera:

- Si a un método “q” le podemos pasar objetos “x” de la clase “T”, el método hace correctamente su función.
- Si tenemos una clase “S” que hereda de la clase “T”.Entonces un objeto “y” de la clase “S” debería
ser capaz de pasarse a la función “q” y ésta funcionará igualmente.

```php
<?php

interface IGeometricShape {

    public function area();

    public function perimetro();
}

class TrianguloEquilatero implements IGeometricShape {

    private $sides = 3;
    private $sideLength;

    public function __construct($sideLength) {
        $this->sideLength = $sideLength;
    }

    public function area() {
        return sqrt(3) * pow($this->sideLength, 2) / 4;
    }

    public function perimetro() {
        return $this->sideLength * $this->sides;
    }

}

class Cuadrado implements IGeometricShape {

    private $sides = 4;
    private $sideLength;

    function __construct($sideLength) {
        $this->sideLength = $sideLength;
    }

    public function area() {
        return $this->sideLength * $this->sideLength;
    }

    public function perimetro() {
        return $this->sideLength * $this->sides;
    }

}

class Rectangulo implements IGeometricShape {

    private $sides = 4;
    private $height;
    private $width;

    public function __construct($height, $width) {
        $this->height = $height;
        $this->width = $width;
    }

    public function area() {
        return $this->width * $this->height;
    }

    public function perimetro() {
        return 2 * $this->height + 2 * $this->width;
    }

}

class AreaOperations {

    public static function sumAreas(array $figuras) {
        $area = 0;
        foreach ($figuras as $fig) {
            $area += $fig->area();
        }
        return $area;
    }

}

class PerimeterOperations {

    public static function sumPerimetros(array $figuras) {
        $perimetro = 0;
        foreach ($figuras as $fig) {
            $perimetro += $fig->perimetro();
        }
        return $perimetro;
    }

}

$figuras = [
    new Rectangulo(10, 5),
    new Rectangulo(4, 6),
    new Rectangulo(5, 1),
    new Rectangulo(8, 9),
    new TrianguloEquilatero(9),
    new TrianguloEquilatero(12),
    new Cuadrado(19)
];

$sumAreas = AreaOperations::sumAreas($figuras);
$sumPerimetros = PerimeterOperations::sumPerimetros($figuras);

echo "el Area es: {$sumAreas} y el perímetro es: {$sumPerimetros}\n";
```

En este caso al utilizar interfaces nos ayudó a evitar tener sobreescrituras de código entre nuestra clase base y nuestra
clase hija por lo que cumplimos este principio.

### Interface Segregation Principle

Este principio comenta que los clientes solo deben conocer las interfaces que deben, en nuestro ejemplo anterior
tanto AreaOperations como PerimeterOperations puede cada uno ver los métodos del otro.
Por lo que debemos pensar en estos dos principios fundamentales:

* Muchas interfaces específicas son mejores que una única más general.

* Los clientes no deberían verse forzados a depender de interfaces que no usan.

### Dependency Inversion Principle

Este principio su finalidad es el de desacoplar módulos de software
