###Esenciales

Se dividen en cuatro tipos de problemas.

* Complejidad: cuando un dominio de un problema es complejo en sí mismo. En el caso de adiciones y todas las acciones que conlleven al sistema a ser más complejo.
Manejo del problema de complejidad: No desarrollar: Comprar - OSS.
* Conformidad: en qué contexto se usa el software y cómo debe adecuarse al mismo. Se incluyen todo lo que le compete. Ej: Ambiente, conectividad, impuestos, etc.
Manejo del problema: Prototipado rápido, feedback y ciclos rápidos para soluciones pequeñas.
* Tolerancia al Cambio: Posibilidad del cambio en el mismo y que sea responsivo a diferentes contextos.
Manejo del problema: Desarrollo Evolutivo, desarrollos pequeños. Paso a paso pero de manera firme e ir haciendo crecer el software.
* Invisibilidad: Problemas de tangibilidad nula.
Manejo del problema: Grandes diseñadores, Arquitectos que saben abtraer el problema y que realiza soluciones elegantes, de manera simple, con la mejor calidad posible en los componentes que lo necesitan.

###Accidentales

Está relacionado con la plataforma que vamos a implementar, tecnología, lenguajes, frameworks, integraciones, 
entre otros, que tienen tres Entornos:

* Lenguajes de alto nivel
* Multi-procesamiento
* Entornos de programación
