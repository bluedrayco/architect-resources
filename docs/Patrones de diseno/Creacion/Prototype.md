## Prototype


### Otros nombres

N/A.

### Finalidad

Este patrón tiene como finalidad crear nuevos objetos clonando una onstancia de un objeto creado previamente.

### En donde lo puedo aplicar

Este patrón es util 

### Estructura



### Ventajas

### Desventajas

### Usos conocidos

### Ejemplo



Nombre del patrón: nombre estándar del patrón por el cual será reconocido en la comunidad (normalmente se expresan en inglés).
Clasificación del patrón: creacional, estructural o de comportamiento.
Intención: ¿Qué problema pretende resolver el patrón?
También conocido como: Otros nombres de uso común para el patrón.
Motivación: Escenario de ejemplo para la aplicación del patrón.
Aplicabilidad: Usos comunes y criterios de aplicabilidad del patrón.
Estructura: Diagramas de clases oportunos para describir las clases que intervienen en el patrón.
Participantes: Enumeración y descripción de las entidades abstractas (y sus roles) que participan en el patrón.
Colaboraciones: Explicación de las interrelaciones que se dan entre los participantes.
Consecuencias: Consecuencias positivas y negativas en el diseño derivadas de la aplicación del patrón.
Implementación: Técnicas o comentarios oportunos de cara a la implementación del patrón.
Código de ejemplo: Código fuente ejemplo de implementación del patrón.
Usos conocidos: Ejemplos de sistemas reales que usan el patrón.
Patrones relacionados: Referencias cruzadas con otros patrones.