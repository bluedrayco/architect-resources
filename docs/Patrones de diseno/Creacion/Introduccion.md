## Introduccion

Los patrones de creación se enfocan principalmente a resolver la necesidad de 
tener diferentes mecanismos para la creación de objetos, estos patrones tratan de solucionar el problema 
de no agregar complejidad al diseño de crear una forma básica de creación de objetos.