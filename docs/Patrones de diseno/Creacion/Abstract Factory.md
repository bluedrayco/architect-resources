
## Abstract factory

###Descripción
Para crear series de objetos dependientes o relacionados sin epecificar sus clases concretas. 
###Cuando usarlo
* Para crear familias de objetos relacionados o dependientes entre si.

* Para encapsular dependencias de plataforma y hacer una aplicación portable.

* Para prevenir que el código no utilice el operador 'new'

* Para cambiar la plataforma subyascente con el mínimo de cambios.

### Componentes principales

* Una clase Abstracta **Factory** publica
###Diagrama UML
![Abstract Factory](./abstract_factory.png "Abstract Factory")
![Abstract Factory](./abstract-factory2.jpg "Abstract Factory")

###Ejemplo de código

Parser.php
```php
<?php
namespace DesignPatterns\Creational\AbstractFactory;

interface Parser
{
    public function parse(string $input): array;
}
```

CsvParser.php
```php
<?php

namespace DesignPatterns\Creational\AbstractFactory;

class CsvParser implements Parser
{
    const OPTION_CONTAINS_HEADER = true;
    const OPTION_CONTAINS_NO_HEADER = false;

    /**
     * @var bool
     */
    private $skipHeaderLine;

    public function __construct(bool $skipHeaderLine)
    {
        $this->skipHeaderLine = $skipHeaderLine;
    }

    public function parse(string $input): array
    {
        $headerWasParsed = false;
        $parsedLines = [];

        foreach (explode(PHP_EOL, $input) as $line) {
            if (!$headerWasParsed && $this->skipHeaderLine === self::OPTION_CONTAINS_HEADER) {
                continue;
            }

            $parsedLines[] = str_getcsv($line);
        }

        return $parsedLines;
    }
}
```

JsonParser.php
```php
<?php

namespace DesignPatterns\Creational\AbstractFactory;

class JsonParser implements Parser
{
    public function parse(string $input): array
    {
        return json_decode($input, true);
    }
}
```

ParserFactory.php
```php
<?php

namespace DesignPatterns\Creational\AbstractFactory;

class ParserFactory
{
    public function createCsvParser(bool $skipHeaderLine): CsvParser
    {
        return new CsvParser($skipHeaderLine);
    }

    public function createJsonParser(): JsonParser
    {
        return new JsonParser();
    }
}
```