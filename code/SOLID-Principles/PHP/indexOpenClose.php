<?php

require 'vendor/autoload.php';

use App\OpenClose\Rectangle;
use App\OpenClose\EquilateralTriangle;
use App\OpenClose\Shape;
use App\OpenClose\AreaOperations;
use App\OpenClose\PerimeterOperations;

$shapes = [
    new Rectangle(10, 5),
    new Rectangle(4, 6),
    new Rectangle(5, 1),
    new Rectangle(8, 9),
    new EquilateralTriangle(9),
    new EquilateralTriangle(12)
];

$sumAreas = AreaOperations::sumAreas($shapes);
$sumPerimetros = PerimeterOperations::sumPerimeters($shapes);

echo "el Area es: ".$sumAreas." y el perímetro es: ".$sumPerimetros."\n";
