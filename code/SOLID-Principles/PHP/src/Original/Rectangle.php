<?php
namespace App\Original;

class Rectangle {
    private float $height;
    private float $width;

    public function __construct(float $height, float $width) {
        $this->height = $height;
        $this->width = $width;
    }

    public static function sumAreas(array $rectangulos):float {
        $area = 0;
        foreach($rectangulos as $rectangulo) {
            $area += $rectangulo->getHeight() * $rectangulo->getWidth();
        }
        return $area;
    }

    public static function sumPerimeters(array $rectangulos):float {
        $perimetro = 0;
        foreach($rectangulos as $rectangulo) {
            $perimetro += 2 * $rectangulo->getHeight() + 2 * $rectangulo->getWidth();
        }
        return $perimetro;
    }

    public function getHeight():float {
        return $this->height;
    }

    public function getWidth():float {
        return $this->width;
    }

    public function setHeight(float $height):void{
        $this->height = $height;
    }

    public function setWidth(float $width):void {
        $this->width = $width;
    }
}
