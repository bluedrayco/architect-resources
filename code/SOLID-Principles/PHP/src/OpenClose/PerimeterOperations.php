<?php 
namespace App\OpenClose;

use App\OpenClose\Shape;

class PerimeterOperations{

    public static function sumPerimeters(array $shapes):float{
        $perimeter = 0;
        foreach($shapes as $shape) {
            $perimeter += $shape->perimeter();
        }
        return $perimeter;
    }
}