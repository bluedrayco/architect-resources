<?php
namespace App\SingleResponsability;

use App\SingleResponsability\Rectangle;


class AreaOperations{

    public static function sumAreas(array $rectangulos):float{
        $area = 0;
        foreach($rectangulos as $rectangle) {
            $area += $rectangle->getHeight() * $rectangle->getWidth();
        }
        return $area;
    }
}