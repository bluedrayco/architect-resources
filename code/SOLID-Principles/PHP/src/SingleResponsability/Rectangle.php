<?php
namespace App\SingleResponsability;

class Rectangle {
    public float $height;
    public float $width;

    public function __construct(float $height,float $width){
        $this->height = $height;
        $this->width = $width;
    }

    public function getHeight():float {
        return $this->height;
    }

    public function getWidth():float {
        return $this->width;
    }

    public function setHeight(float $height):void {
        $this->height = $height;
    }

    public function setWidth(float $width):void {
        $this->width = $width;
    }
}
