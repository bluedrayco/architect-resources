<?php
namespace App\LiskovSubstitution;

use App\LiskovSubstitution\IGeometricShape;

class EquilateralTriangle implements IGeometricShape{
    private float $sideLength;
    
    public function __construct(float $sideLength){
        $this->sideLength=$sideLength;
    }
    public function area():float{
        return sqrt(3)*pow($this->sideLength, 2)/4;
    }

    public function perimeter():float{
        return $this->sideLength*3;
    }
}