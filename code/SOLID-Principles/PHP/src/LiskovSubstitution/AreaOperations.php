<?php
namespace App\LiskovSubstitution;

class AreaOperations{

    public static function sumAreas(array $shapes):float{
        $area = 0;
        foreach($shapes as $shape) {
            $area += $shape->area();
        }
        return $area;
    }
}