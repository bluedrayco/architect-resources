<?php
namespace App\LiskovSubstitution;

interface IGeometricShape{
    public function area():float;
    public function perimeter():float;
}