<?php
require 'vendor/autoload.php';

use App\SingleResponsability\Rectangle;
use App\SingleResponsability\AreaOperations;
use App\SingleResponsability\PerimeterOperations;

$rectangulos = [
    new Rectangle(10, 5),
    new Rectangle(4, 6),
    new Rectangle(5, 1),
    new Rectangle(8, 9)
];



$sumAreas = AreaOperations::sumAreas($rectangulos);
$sumPerimetros = PerimeterOperations::sumPerimetros($rectangulos);

echo "el Area es: ".$sumAreas." y el perímetro es: ".$sumPerimetros."\n";
