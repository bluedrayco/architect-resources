<?php

require 'vendor/autoload.php';

use App\Original\Rectangle;


$rectangulos = [
    new Rectangle(10, 5),
    new Rectangle(4, 6),
    new Rectangle(5, 1),
    new Rectangle(8, 9)
];


$sumAreas = Rectangle::sumAreas($rectangulos);
$sumPerimetros = Rectangle::sumPerimeters($rectangulos);

echo "el Area es: ".$sumAreas." y el perímetro es: ".$sumPerimetros."\n";