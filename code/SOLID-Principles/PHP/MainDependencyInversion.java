package com.tecno;

import com.tecno.solid.dependency_inversion.Rectangle;
import com.tecno.solid.dependency_inversion.EquilateralTriangle;
import com.tecno.solid.dependency_inversion.IGeometricShape;
import com.tecno.solid.dependency_inversion.Operate;

import java.util.List;
import java.util.ArrayList;

public class MainDependencyInversion {
    public static void main(String[] args) {
        List<IGeometricShape> shapes = new ArrayList<IGeometricShape>();

        shapes.add(new Rectangle(10, 5));
        shapes.add(new Rectangle(4, 6));
        shapes.add(new Rectangle(5, 1));
        shapes.add(new Rectangle(8, 9));
        shapes.add(new EquilateralTriangle(9));
        shapes.add(new EquilateralTriangle(12));

        Operate operateManager = new Operate(shapes);

        double sumAreas = operateManager.getSumAreas();
        double sumPerimeters = operateManager.getSumPerimeters();

        System.out.println("el Area es: "+sumAreas+" y el perímetro es: "+sumPerimeters);
    }
}