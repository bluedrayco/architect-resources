package com.tecno;

import com.tecno.solid.original.Rectangle;
import java.util.List;
import java.util.ArrayList;

public class MainOriginal {
    public static void main(String[] args) {
        List<Rectangle> rectangulos = new ArrayList<Rectangle>();

        rectangulos.add(new Rectangle(10, 5));
        rectangulos.add(new Rectangle(4, 6));
        rectangulos.add(new Rectangle(5, 1));
        rectangulos.add(new Rectangle(8, 9));

        double sumAreas = Rectangle.sumAreas(rectangulos);
        double sumPerimetros = Rectangle.sumPerimeters(rectangulos);

        System.out.println("el Area es: "+sumAreas+" y el perímetro es: "+sumPerimetros);
    }
}