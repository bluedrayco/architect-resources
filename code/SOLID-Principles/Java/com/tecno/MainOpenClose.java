package com.tecno;

import com.tecno.solid.open_close.Rectangle;
import com.tecno.solid.open_close.EquilateralTriangle;
import com.tecno.solid.open_close.Shape;
import com.tecno.solid.open_close.AreaOperations;
import com.tecno.solid.open_close.PerimeterOperations;

import java.util.List;
import java.util.ArrayList;

public class MainOpenClose {
    public static void main(String[] args) {
        List<Shape> shapes = new ArrayList<Shape>();

        shapes.add(new Rectangle(10, 5));
        shapes.add(new Rectangle(4, 6));
        shapes.add(new Rectangle(5, 1));
        shapes.add(new Rectangle(8, 9));
        shapes.add(new EquilateralTriangle(9));
        shapes.add(new EquilateralTriangle(12));

        double sumAreas = AreaOperations.sumAreas(shapes);
        double sumPerimetros = PerimeterOperations.sumPerimeters(shapes);

        System.out.println("el Area es: "+sumAreas+" y el perímetro es: "+sumPerimetros);
    }
}