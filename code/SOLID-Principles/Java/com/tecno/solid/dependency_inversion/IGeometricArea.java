package com.tecno.solid.dependency_inversion;

public interface IGeometricArea{

    public double area();

}