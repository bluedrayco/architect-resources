package com.tecno.solid.dependency_inversion;

import com.tecno.solid.dependency_inversion.IGeometricShape;
import com.tecno.solid.dependency_inversion.IGeometricArea;
import com.tecno.solid.dependency_inversion.IGeometricPerimeter;
import java.util.List;
import java.util.ArrayList;

public class Operate{

    private List<IGeometricShape> shapes;
    
    public Operate(List<IGeometricShape> shapes){
        this.shapes = shapes;
    }

    public double getSumAreas(){
        List<IGeometricArea> shapesArea = new ArrayList<>(this.shapes);
        return AreaOperations.sumAreas(shapesArea);
    }

    public double getSumPerimeters(){
        List<IGeometricPerimeter> shapesPerimeter = new ArrayList<>(this.shapes);
        return PerimeterOperations.sumPerimeters(shapesPerimeter);
    }
}