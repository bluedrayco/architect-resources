package com.tecno.solid.liskov_substitution;

import com.tecno.solid.liskov_substitution.IGeometricShape;
import java.util.List;

public class AreaOperations{

    public static double sumAreas(List<IGeometricShape> shapes){
        double area = 0;
        for(IGeometricShape shape : shapes) {
            area += shape.area();
        }
        return area;
    }
}