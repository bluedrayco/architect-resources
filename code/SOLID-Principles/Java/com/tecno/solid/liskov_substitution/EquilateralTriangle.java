package com.tecno.solid.liskov_substitution;

import com.tecno.solid.liskov_substitution.IGeometricShape;

public class EquilateralTriangle implements IGeometricShape{
    private double sideLength;
    
    public EquilateralTriangle(double sideLength){
        this.sideLength=sideLength;
    }
    public double area(){
        return Math.sqrt(3)*Math.pow(this.sideLength, 2)/4;
    }

    public double perimeter(){
        return this.sideLength*3;
    }
}