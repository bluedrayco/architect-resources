package com.tecno.solid.interface_segregation;

import com.tecno.solid.interface_segregation.IGeometricArea;
import com.tecno.solid.interface_segregation.IGeometricPerimeter;

public interface IGeometricShape extends IGeometricPerimeter,IGeometricArea{

}