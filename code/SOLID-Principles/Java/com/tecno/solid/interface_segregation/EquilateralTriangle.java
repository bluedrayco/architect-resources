package com.tecno.solid.interface_segregation;

import com.tecno.solid.interface_segregation.IGeometricShape;

public class EquilateralTriangle implements IGeometricShape{
    private double sideLength;
    
    public EquilateralTriangle(double sideLength){
        this.sideLength=sideLength;
    }
    public double area(){
        return Math.sqrt(3)*Math.pow(this.sideLength, 2)/4;
    }

    public double perimeter(){
        return this.sideLength*3;
    }
}