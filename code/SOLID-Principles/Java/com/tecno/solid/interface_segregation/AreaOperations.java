package com.tecno.solid.interface_segregation;

import com.tecno.solid.interface_segregation.IGeometricArea;
import java.util.List;

public class AreaOperations{

    public static double sumAreas(List<IGeometricArea> shapes){
        double area = 0;
        for(IGeometricArea shape : shapes) {
            area += shape.area();
        }
        return area;
    }
}