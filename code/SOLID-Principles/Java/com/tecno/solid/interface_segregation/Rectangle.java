package com.tecno.solid.interface_segregation;

import com.tecno.solid.interface_segregation.IGeometricShape;

public class Rectangle implements IGeometricShape
{
    private double height;
    private double width;

    public  Rectangle(double height, double width){
        this.height = height;
        this.width = width;
    }

    public double area(){
        return this.width*this.height;
    }

    public double perimeter(){
        return 2 * this.height + 2 * this.width;
    }
}