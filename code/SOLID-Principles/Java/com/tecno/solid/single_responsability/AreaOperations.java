package com.tecno.solid.single_responsability;

import com.tecno.solid.single_responsability.Rectangle;
import java.util.List;

public class AreaOperations{

    public static double sumAreas(List<Rectangle> rectangulos){
        double area = 0;
        for (Rectangle rect : rectangulos) {
            area += rect.getHeight() * rect.getWidth();
        }
        return area;
    }
}