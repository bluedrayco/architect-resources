package com.tecno.solid.single_responsability;

import java.util.List;

public class PerimeterOperations{

    public static double sumPerimetros(List<Rectangle> rectangulos){
        double perimetro = 0;
        for (Rectangle rect : rectangulos) {
            perimetro += 2 * rect.getHeight() + 2 * rect.getWidth();
        }
        return perimetro;
    }
}