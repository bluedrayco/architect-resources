package com.tecno.solid.single_responsability;

public class Rectangle {
    public double height;
    public double width;

    public Rectangle(double height,double width){
        this.height = height;
        this.width = width;
    }

    public double getHeight() {
        return this.height;
    }

    public double getWidth() {
        return this.width;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public void setWidth(double width) {
        this.width = width;
    }
}
