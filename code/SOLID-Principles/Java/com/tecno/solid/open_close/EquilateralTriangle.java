package com.tecno.solid.open_close;

import com.tecno.solid.open_close.Shape;

public class EquilateralTriangle extends Shape{
    private double sideLength;
    
    public EquilateralTriangle(double sideLength){
        this.sideLength=sideLength;
    }
    public double area(){
        return Math.sqrt(3)*Math.pow(this.sideLength, 2)/4;
    }

    public double perimeter(){
        return this.sideLength*3;
    }
}