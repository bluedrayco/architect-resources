package com.tecno.solid.open_close;

import com.tecno.solid.open_close.Shape;

public class Rectangle extends Shape
{
    private double height;
    private double width;

    public  Rectangle(double height, double width){
        this.height = height;
        this.width = width;
    }

    public double area(){
        return this.width*this.height;
    }

    public double perimeter(){
        return 2 * this.height + 2 * this.width;
    }
}