package com.tecno;

import com.tecno.solid.liskov_substitution.Rectangle;
import com.tecno.solid.liskov_substitution.EquilateralTriangle;
import com.tecno.solid.liskov_substitution.IGeometricShape;
import com.tecno.solid.liskov_substitution.AreaOperations;
import com.tecno.solid.liskov_substitution.PerimeterOperations;

import java.util.List;
import java.util.ArrayList;

public class MainLiskovSubstitution {
    public static void main(String[] args) {
        List<IGeometricShape> shapes = new ArrayList<IGeometricShape>();

        shapes.add(new Rectangle(10, 5));
        shapes.add(new Rectangle(4, 6));
        shapes.add(new Rectangle(5, 1));
        shapes.add(new Rectangle(8, 9));
        shapes.add(new EquilateralTriangle(9));
        shapes.add(new EquilateralTriangle(12));

        double sumAreas = AreaOperations.sumAreas(shapes);
        double sumPerimetros = PerimeterOperations.sumPerimeters(shapes);

        System.out.println("el Area es: "+sumAreas+" y el perímetro es: "+sumPerimetros);
    }
}