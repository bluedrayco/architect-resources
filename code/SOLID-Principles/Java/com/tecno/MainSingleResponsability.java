package com.tecno;

import com.tecno.solid.single_responsability.Rectangle;
import com.tecno.solid.single_responsability.AreaOperations;
import com.tecno.solid.single_responsability.PerimeterOperations;
import java.util.List;
import java.util.ArrayList;

public class MainSingleResponsability {
    public static void main(String[] args) {
        List<Rectangle> rectangulos = new ArrayList<Rectangle>();

        rectangulos.add(new Rectangle(10, 5));
        rectangulos.add(new Rectangle(4, 6));
        rectangulos.add(new Rectangle(5, 1));
        rectangulos.add(new Rectangle(8, 9));

        double sumAreas = AreaOperations.sumAreas(rectangulos);
        double sumPerimetros = PerimeterOperations.sumPerimetros(rectangulos);

        System.out.println("el Area es: "+sumAreas+" y el perímetro es: "+sumPerimetros);
    }
}