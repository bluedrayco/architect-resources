<?php
namespace DesignPatterns\Structure\Registry\Solution\Controller;

use DesignPatterns\Structure\Registry\Solution\Registry;
use Slim\Slim;

class PokemonController
{

    private $app;
    public function __construct(Slim $app)
    {
        $this->app=$app;
    }

    public function getPokemonFromAPI($number)
    {
        $httpClient=Registry::get(Registry::HTTP_CLIENT);
        $responsePokemon = $httpClient->request("GET", "https://pokeapi.co/api/v2/pokemon/{$number}");
        $logger = Registry::get(Registry::LOGGER);
        $logger->info("finalizamos exitosamente el request con number: {$number}");
        $this->app->response()->headers->set("Content-Type", 'application/json');
        $this->app->response()->write($responsePokemon->getBody());
        $this->app->response()->setStatus(200);
    }
}
