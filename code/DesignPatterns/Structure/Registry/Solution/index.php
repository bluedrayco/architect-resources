<?php
require "../../../../../vendor/autoload.php";


use Slim\Slim;
use DesignPatterns\Structure\Registry\Solution\Controller\PokemonController;

$app = new Slim();

require "./dependenciesLoader.php";

$app->get('/pokemon/:number', function ($number) use ($app, $guzzleClient, $logger) {
    $controller = new PokemonController($app);
    $controller->getPokemonFromAPI($number);
});


$app->run();
