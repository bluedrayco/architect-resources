<?php
namespace DesignPatterns\Structure\Registry\Solution\Service;

use Monolog\Logger;
use Monolog\Handler\StreamHandler;

class AppLogger extends Logger implements Service
{

    public function __construct(string $appName)
    {
        parent::__construct($appName);
        $this->pushHandler(new StreamHandler('../app.log', Logger::INFO));
    }
}
