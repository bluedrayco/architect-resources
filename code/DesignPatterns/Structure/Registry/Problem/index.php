<?php
require "../../../../../vendor/autoload.php";

use Slim\Slim;
use GuzzleHttp\Client;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;

$app = new Slim();

$guzzleClient = new Client();
$logger = new Logger('app');
$logger->pushHandler(new StreamHandler('./app.log', Logger::INFO));

$app->get('/pokemon/:number', function ($number) use ($app, $guzzleClient, $logger) {
    $responsePokemon = $guzzleClient->request("GET", "https://pokeapi.co/api/v2/pokemon/{$number}");
    $logger->info("finalizamos exitosamente el request con number: {$number}");
    $app->response->headers->set("Content-Type", 'application/json');
    $app->response->write($responsePokemon->getBody());
    $app->response->setStatus(200);
});

$app->run();
