from converter import Converter
import os

class VideoProperties:
    def __init__(self, audioCodec, duration):
        self.audioCodec = audioCodec
        self.duration = duration


class VideoConverter:
    conv = None
    supportedTypes = [
        'mp4'
    ]

    def __init__(self, file: str):
        self.conv = Converter()
        self.videoFilename = file

    def getInfo(self) -> VideoProperties:
        properties = self.conv.probe(self.videoFilename)
        return VideoProperties(audioCodec=properties.audio.codec, duration='%.2f' % (properties.format.duration/60))

    def convertTo(self, target:str,outputFilename:str='output.mp4')->object:
        if target not in self.supportedTypes:
            raise Exception("The mediatype is not supported")

        convert = self.conv.convert(self.videoFilename, outputFilename, {
            'format': target,
            'audio': {
                'codec': 'aac',
                'samplerate': 11025,
                'channels': 2
            },
            'video': {
                'codec': 'hevc',
                'width': 720,
                'height': 400,
                'fps': 25
            }})
        return convert


class AudioConverter:
    supportedTypes = [
        'mp3',
        'wav',
        'flac'
    ]
    def __init__(self,audioFile):
        filename = audioFile.split('.')
        self.audioFile=filename[0]
        self.extension = filename[1]

    def convertTo(self,target:str,outputFilename:str='output_audio'):
        if target not in self.supportedTypes:
            raise Exception("The mediatype is not supported")
        os.system(f"rm -rf input;rm -rf output; mkdir input; cp {self.audioFile}.{self.extension} input/{self.audioFile}.{self.extension};mkdir output")
        command = f"audioconvert convert input output --output-format='.{target}'"
        os.system(command)
        if outputFilename=='output_audio':
            outputFilename= outputFilename + '.'+target
        os.system(f"cp output/{self.audioFile}.{target} .;mv {self.audioFile}.{target} {outputFilename};rm -rf input;rm -rf output")


class MediaConverter:

    def __init__(self):
        pass

    def convertVideoFile(self,source:str,destination:str,formatFile:str):
        videoConverter = VideoConverter(source)
        convert = videoConverter.convertTo(formatFile,destination)
        for timecode in convert:
            print(f'\rConverting ({timecode:.2f}) ...')
    
    def convertAudioFile(self,source:str,destination:str,formatFile:str):
        audioConverter = AudioConverter(source)
        audioConverter.convertTo(formatFile,destination)


media = MediaConverter()
media.convertVideoFile('tecno.avi','test1.mp4','mp4')
media.convertAudioFile('pokemon.mp3','pokemon.wav','wav')

