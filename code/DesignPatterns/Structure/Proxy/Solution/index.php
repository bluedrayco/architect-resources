<?php
require "../../../../../vendor/autoload.php";

use DesignPatterns\Structure\Proxy\Solution\CachingDownloader;
use DesignPatterns\Structure\Proxy\Solution\SimpleDownloader;
use DesignPatterns\Structure\Proxy\Solution\Downloader;


function clientCode(Downloader $subject)
{
    $initial = microtime(true);
    $result = $subject->download("http://www.google.com.mx");
    $final = microtime(true);
    $time = $final-$initial;
    printf ("---First Download  -> %.6f \n",$time);
    $initial = microtime(true);
    $result = $subject->download("http://www.google.com.mx");
    $final = microtime(true);
    $time = $final-$initial;
    printf ("---Second Download -> %.6f \n",$time);
}

shell_exec('reset');
echo "Executing client code with real subject:\n";
$downloader = new SimpleDownloader();
clientCode($downloader);

echo "\n";

echo "Executing the same client code with a proxy:\n";
$proxyDownloader = new CachingDownloader($downloader);
clientCode($proxyDownloader);