<?php
require "../../../../../vendor/autoload.php";
use DesignPatterns\Structure\Proxy\Problem\Downloader;
use DesignPatterns\Structure\Proxy\Problem\CacheDownloader;


function client_code(Downloader $downloader){
    $downloader->download("http://www.google.com");
}

$downloader = new Downloader();
client_code($downloader);

$downloaderWithCache = new CacheDownloader();
client_code($downloaderWithCache);
client_code($downloaderWithCache);
