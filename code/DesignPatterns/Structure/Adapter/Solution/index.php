<?php

require "../../../../../vendor/autoload.php";

use DesignPatterns\Structure\Adapter\Solution\EmailNotification;
use DesignPatterns\Structure\Adapter\Solution\SlackNotification;
use DesignPatterns\Structure\Adapter\Solution\NotificationService;
use DesignPatterns\Structure\Adapter\Solution\SlackApi;

$emailNotification = new EmailNotification("tecno", 'admin123', ["email1@gmail.com","email2@hotmail.com"]);
$notifierService = new NotificationService($emailNotification);

$notifierService->sendNotification("Titulo 1", "Hola mundo!!!");

echo "\n===================\n";

$slackApi = new SlackApi('tecno', "wefse324243fw23rew");
$emailNotification = new SlackNotification($slackApi, "proyecto x");
$notifierService = new NotificationService($emailNotification);

$notifierService->sendNotification("Titulo 1", "Saludos a todos!!!");
