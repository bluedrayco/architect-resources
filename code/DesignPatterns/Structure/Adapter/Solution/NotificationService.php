<?php

namespace DesignPatterns\Structure\Adapter\Solution;

class NotificationService
{
    private $notifier=null;

    public function __construct(Notification $notifier)
    {
        $this->notifier=$notifier;
    }

    public function sendNotification(string $title, string $message)
    {
        $this->notifier->send($title, $message);
    }
}
