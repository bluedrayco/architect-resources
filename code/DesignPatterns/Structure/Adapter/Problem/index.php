<?php
require "../../../../../vendor/autoload.php";

use DesignPatterns\Structure\Adapter\Problem\NotificationService;
use DesignPatterns\Structure\Adapter\Problem\NotificationServiceV2;

$notificationService = new NotificationService("tecno", 'admin123', ["email1@gmail.com","email2@hotmail.com"]);

$notificationService->send("Saludo", "Hola mundo");


echo "\n\n=====================\n\n";

$config = [
    "login"=>'tecno',
    "apiKey"=>"wefse324243fw23rew"
];
$notificationServiceV2 = new NotificationServiceV2("Slack", $config);

$config2 =[
    "chatId"=>"proyecto x"
];
$notificationServiceV2->send("Saludo", "Hola mundo", $config2);

echo "\n";

$config = [
    "username"=>'bluedrayco',
    "password"=>"master123"
];
$notificationServiceV2 = new NotificationServiceV2("Email", $config);

$config2 =[
    "recipients"=>["email1@gmail.com","email2@hotmail.com"]
];
$notificationServiceV2->send("Saludo", "Hola mundo", $config2);
