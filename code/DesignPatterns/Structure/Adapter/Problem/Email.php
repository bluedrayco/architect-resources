<?php

namespace DesignPatterns\Structure\Adapter\Problem;

class Email
{
    private $username;
    private $password;
    private $recipents;
    
    public function __construct(string $username, string $password)
    {
        $this->username = $username;
        $this->password = $password;
    }

    public function setRecipients(array $recipients):void
    {
        $this->recipents=$recipients;
    }

    public function connect():void
    {
        echo "connecting with the user {$this->username} and password {$this->password}\n";
    }

    public function close():void
    {
        echo "\nThe connection was closed\n";
    }

    public function sendMail(string $title, string $message): void
    {
        $emails = implode(',', $this->recipents);
        echo "Sent email with title '$title' to '{$emails}' that says '{$message}'.";
    }
}
