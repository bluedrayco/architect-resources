<?php
namespace DesignPatterns\Structure\Adapter\Problem;

use DesignPatterns\Structure\Adapter\Problem\Email;

class NotificationService
{

    public function __construct(string $username, string $password, array $recipients)
    {
        $this->email = new Email($username, $password);
        $this->email->setRecipients($recipients);
    }

    public function send(string $subject, string $message):void
    {
        $this->email->connect();
        $this->email->sendMail($subject, $message);
        $this->email->close();
    }
}
