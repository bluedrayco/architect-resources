<?php
namespace DesignPatterns\Structure\Adapter\Problem;

class NotificationServiceV2
{

    private $api = null;

    public function __construct(string $apiName, array $config)
    {
        $this->apiName = $apiName;
        $this->api = $this->getApi($apiName, $config);
    }

    private function getApi($apiName, $config):object
    {
        switch ($this->apiName) {
            case "Slack":
                $api = new SlackApi($config['login'], $config['apiKey']);
                break;
            case "Email":
                $api = new Email($config['username'], $config['password']);
                break;
        }
        return $api;
    }

    public function send(string $title, string $message, array $extra = null)
    {
        switch ($this->apiName) {
            case "Slack":
                $this->api->logIn();
                $slackMessage = "#" . $title . "# " . strip_tags($message);
                $this->api->sendMessage($extra['chatId'], $slackMessage);
                break;
            case "Email":
                $this->api->setRecipients($extra['recipients']);
                $this->api->connect();
                $this->api->sendMail($title, $message);
                $this->api->close();
                break;
        }
    }
}
