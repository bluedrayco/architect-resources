<?php
namespace DesignPatterns\Structure\Flyweight\Problem;

class TemplateManager
{
    private $templates=[];

    public function addTemplate($type, $title, $body)
    {
        $this->templates[]= TemplateFactory::getTemplateType($type, $title, $body);
    }

    public function draw()
    {
        foreach ($this->templates as $templates) {
            $templates->draw();
        }
    }
}
