<?php

namespace DesignPatterns\Structure\Flyweight\Problem;

class JSONTemplate
{
    private $title;
    private $body;
    private $template;
    private $file;

    public function __construct($title, $body)
    {
        $this->title=$title;
        $this->body = $body;
        $this->template = "Rendering template: JSON\n";
        $this->template.="{
            title: '{$this->title}',
            body: '{$this->body}'
        }";
        $this->file = file_get_contents(dirname(__FILE__)."/agregar.pdf");
    }

    public function draw()
    {
        echo $this->template;
    }
}
