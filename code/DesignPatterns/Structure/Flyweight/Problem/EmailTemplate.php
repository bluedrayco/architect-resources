<?php
namespace DesignPatterns\Structure\Flyweight\Problem;

class EmailTemplate
{
    private $title;
    private $body;

    public function __construct($title, $body)
    {
        $this->title=$title;
        $this->body = $body;
        $this->file = file_get_contents(dirname(__FILE__)."/agregar.pdf");
    }

    public function draw()
    {
        echo "Rendering template: Email\n";
        echo "<html>
            <a> bienvenido '{$this->title}'</a>
            <h1> te hemos enviado a tu correo tu user y password amigo '{$this->body}'</h1>
        </html>";
    }
}
