<?php
namespace DesignPatterns\Structure\Flyweight\Problem;

class TemplateFactory
{
    public const EMAIL_TEMPLATE="EmailTemplate";
    public const JSON_TEMPLATE="JSONTemplate";

    public static function getTemplateType($type, $title, $body)
    {
            switch ($type) {
                case self::EMAIL_TEMPLATE:
                    $templateType = new EmailTemplate($title, $body);
                break;
                case self::JSON_TEMPLATE:
                    $templateType = new JSONTemplate($title, $body);
                break;
            }
        return $templateType;
    }
}
