<?php
namespace DesignPatterns\Structure\Flyweight\Solution;

class TemplateFactory
{
    private static $templateTypes = [];
    public const EMAIL_TEMPLATE="EmailTemplate";
    public const JSON_TEMPLATE="JSONTemplate";

    public static function getTemplateType($type)
    {
        if (!array_key_exists($type, self::$templateTypes)) {
            $templateType = null;
            switch ($type) {
                case self::EMAIL_TEMPLATE:
                    $templateType = self::createEmailTemplate();
                break;
                case self::JSON_TEMPLATE:
                    $templateType = self::createJSONTemplate();
                break;
            }
            self::$templateTypes[$type]=$templateType;
        }
        return self::$templateTypes[$type];
    }

    private static function createEmailTemplate()
    {
        return new TemplateType(
            "Email",
            "<html>&&</html>",
            "<a> bienvenido '##'</a>",
            "<h1> te hemos enviado a tu correo tu user y password amigo '%%'</h1>"
        );
    }
    private static function createJSONTemplate()
    {
        return new TemplateType(
            "JSON",
            "{\n
                &&
            }",
            "title:'##',",
            "body: '%%'"
        );
    }
}
