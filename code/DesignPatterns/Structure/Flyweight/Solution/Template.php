<?php
namespace DesignPatterns\Structure\Flyweight\Solution;

class Template
{
    private $title;
    private $body;
    private $type;

    public function __construct($title, $body, TemplateType $type)
    {
        $this->title=$title;
        $this->body=$body;
        $this->type = $type;
    }

    public function draw()
    {
        $this->type->draw($this->title, $this->body);
    }
}
