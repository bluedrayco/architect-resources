<?php

namespace DesignPatterns\Structure\Bridge\Problem;

interface Pages
{

    public function getSimplePage(string $title, string $content):string;
    public function getProductPage(int $id, string $title, string $description, string $image, float $price):string;
}
