<?php
namespace DesignPatterns\Structure\Bridge\Problem;

class RenderHTML implements Pages
{

    public function getSimplePage(string $title, string $content):string
    {
        return "
        <html>
            <body>
                <h1>{$title}</h1>
            <div class='text'{$content}</div>
            </body>
        </html>
        ";
    }

    public function getProductPage(int $id, string $title, string $description, string $image, float $price):string
    {
        return "
        <html>
            <body>
                <h1>{$title}</h1>
                <div class='text'>{$description}</div>
                <img src='{$image}'>
                <a href='/cart/add/{$id}'>Add to cart (\${$price})</a>
            </body>
        </html>       
        ";
    }
}
