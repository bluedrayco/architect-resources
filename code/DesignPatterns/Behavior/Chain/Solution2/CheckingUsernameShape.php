<?php
namespace DesignPatterns\Behavior\Chain\Solution2;

class CheckingUsernameShape extends Middleware{
    
    public function check(User $user):void{
        $username = $user->getUsername();
        if(!ctype_upper($username[0])){
            throw new ValidatorException("The username must begins in uppercase");
        }
        if(!ctype_alnum($username)){
            throw new ValidatorException("The username must be created with alpanum");
        }
        parent::check($user);
    }
}