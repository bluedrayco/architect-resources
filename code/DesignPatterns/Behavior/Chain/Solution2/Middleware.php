<?php
namespace DesignPatterns\Behavior\Chain\Solution2;

abstract class Middleware
{

    private $next;

    public function addMiddleware(Middleware $next): Middleware
    {
        $this->next = $next;

        return $next;
    }

    public function check(User $user): void
    {
        if (!$this->next) {
            return;
        }

        $this->next->check($user);
    }
}