<?php

require "../../../../../vendor/autoload.php";

use DesignPatterns\Behavior\Chain\Solution2\CheckingUsernameShape;
use DesignPatterns\Behavior\Chain\Solution2\User;
use DesignPatterns\Behavior\Chain\Solution2\UsernameExists;
try{
    $user = new User("usuario@gmail.com","username5","password2");

    $validator = new UsernameExists();
    $validator->addMiddleware(new CheckingUsernameShape());

    $user->setMiddleware($validator);

    $user->validate();
    echo "pasamos las validaciones...\n";
    
}catch(Exception $ex){
    echo $ex->getMessage();
}

