<?php
namespace DesignPatterns\Behavior\Chain\Solution2;

class UserRepository{
    private $users=[];

    public function __construct(){
        $this->users=[
            "Username1"=>new User("email1@gmail.com", "Username1", "password1"),
            "Username2"=>new User("email2@gmail.com", "Username2", "password2"),
            "Username3"=>new User("email3@gmail.com", "Username3", "password3")
            ];
    }

    public function getUserByUsername(string $username ):?User{
        if(!array_key_exists($username,$this->users)){
            return null;
        }
        return $this->users[$username];
    }   
}