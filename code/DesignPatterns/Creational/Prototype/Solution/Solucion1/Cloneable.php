<?php
namespace DesignPatterns\Creational\Prototype\Solution\Solucion1;

interface Cloneable
{

    public function clone():object;
}
