<?php
namespace DesignPatterns\Creational\Prototype\Solution\Solucion1;

class Book implements Cloneable
{
    private $name='';
    private $author='';

    public function __construct(string $name, string $author)
    {
        $this->name=$name;
        $this->author=$author;
    }

    public function clone():object
    {
        $this->name .=" cloned";
        $this->author.=" cloned";
        return $this;
    }
}
