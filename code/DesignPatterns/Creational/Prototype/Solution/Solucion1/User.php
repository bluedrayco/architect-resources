<?php

namespace DesignPatterns\Creational\Prototype\Solution\Solucion1;

use DesignPatterns\Creational\Prototype\Solution\Solucion1\Book;
use DesignPatterns\Creational\Prototype\Solution\Solucion1\Cloneable;

class User implements Cloneable
{

    private $name=null;
    private $age=0;
    private $lastName=null;
    private $books=[];
    private $booksRead=0;


    public function __construct(string $name, string $lastName, string $age)
    {
        $this->name=$name;
        $this->lastName=$lastName;
        $this->age=$age;
    }

    public function addBook(Book $book)
    {
        $this->books[]=$book;
        $this->booksRead+=1;
    }

    public function clone():object
    {
        $this->books= $this->cloneBooks();
        $this->name .= " cloned";
        $this->lastName.=" cloned";
        return $this;
    }

    private function cloneBooks()
    {
        $books=[];
        foreach ($this->books as $book) {
            $books[]=$book->clone();
        }
        return $books;
    }
}
