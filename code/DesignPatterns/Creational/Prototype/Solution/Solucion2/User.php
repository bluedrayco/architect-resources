<?php

namespace DesignPatterns\Creational\Prototype\Solution\Solucion2;

use DesignPatterns\Creational\Prototype\Solution\Solucion2\Book;

class User
{

    private $name=null;
    private $age=0;
    private $lastName=null;
    private $books=[];
    private $booksRead=0;


    public function __construct(string $name, string $lastName, string $age)
    {
        $this->name=$name;
        $this->lastName=$lastName;
        $this->age=$age;
    }

    public function addBook(Book $book)
    {
        $this->books[]=$book;
        $this->booksRead+=1;
    }

    public function __clone()
    {
        $booksTmp=$this->books;
        $this->books=[];
        foreach ($booksTmp as $book) {
            $this->books[]= clone $book;
        }
        $this->name .= " cloned";
        $this->lastName.=" cloned";
    }
}
