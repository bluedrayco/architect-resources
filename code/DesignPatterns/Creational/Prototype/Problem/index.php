<?php

require "../../../../../vendor/autoload.php";

use DesignPatterns\Creational\Prototype\Problem\UserRepository;

$userRepository = new UserRepository();

$user1 = $userRepository->getUserWithBooks(1);

// print_r($user1);

$user2 = $user1;

$user2->name=$user1->name." clone";

$bookTmp=$user2->books;
$user2->books=[];
foreach ($bookTmp as $book) {
    $book->name.=" 2";
    $user2->books[]=$book;
}

print_r($user2);
