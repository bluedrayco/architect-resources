<?php

namespace DesignPatterns\Creational\Prototype\Problem;

use DesignPatterns\Creational\Prototype\Problem\Book;

class User
{

    public $name=null;
    public $age=0;
    public $lastName=null;
    public $books=[];
    public $booksRead=0;


    public function __construct(string $name, string $lastName, string $age)
    {
        $this->name=$name;
        $this->lastName=$lastName;
        $this->age=$age;
    }

    public function addBook(Book $book)
    {
        $this->books[]=$book;
        $this->booksRead+=1;
    }
}
