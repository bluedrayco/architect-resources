<?php
namespace DesignPatterns\Creational\Prototype\Problem;

class Book
{
    public $name='';
    public $author='';

    public function __construct(string $name, string $author)
    {
        $this->name=$name;
        $this->author=$author;
    }
}
