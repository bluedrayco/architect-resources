<?php
namespace DesignPatterns\Creational\Prototype\Problem;

use DesignPatterns\Creational\Prototype\Problem\Book;

class UserRepository
{
    
    public function getUserWithBooks(int $id)
    {
        $user = new User('Roberto Leroy', 'Monroy Ruiz', 30);
        $user->addBook(new Book('World War Z', "Max Brooks"));
        $user->addBook(new Book('Resident Evil', "S.D. Perry"));
        return $user;
    }
}
