<?php
namespace DesignPatterns\Creational\FactoryMethod\Problem;

use PHPMailer\PHPMailer\PHPMailer;

class Hotmail
{
    private $user=null;
    private $password=null;

    public function __construct(string $user, string $password, string $aliasAccount = null)
    {
        $this->user=$user;
        $this->password=$password;
        $this->aliasAccount = $aliasAccount==null?$this->user:$aliasAccount;
    }

    public function sendToHotmail(array $to, string $subject, string $body)
    {
        $mailer = $this->createMailer();
        foreach ($to as $email) {
            $mailer->addAddress($email);
        }
        $mailer->Subject = $subject;
        $mailer->Body    = $body;
        $mailer->send();
    }


    private function createMailer()
    {
        $mailer = new PHPMailer();
        $mailer->isSMTP();
        $mailer->SMTPDebug = 4;
        $mailer->SMTPSecure = 'tls';
        $mailer->SMTPAuth = true;
        $mailer->Host = 'smtp.live.com';
        $mailer->Port = 587;
        $mailer->IsHTML(true);
        $mailer->Username = $this->user;
        $mailer->Password = $this->password;
        $mailer->setFrom($this->user, $this->aliasAccount);
        return $mailer;
    }
}
