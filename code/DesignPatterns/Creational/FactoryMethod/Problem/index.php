<?php

require "../../../../../vendor/autoload.php";

use DesignPatterns\Creational\FactoryMethod\Problem\Gmail;
use DesignPatterns\Creational\FactoryMethod\Problem\Hotmail;

$gmail = new Gmail("bluedrayco@gmail.com", "asbbfckwdwhsyyxd", "tecno_system");

$gmail->send(["bluedrayco@gmail.com"], "saludo", "hola como estas?");

$hotmail = new Hotmail("bluedrayco@hotmail.com", "3n0v4!!!");
$hotmail->sendToHotmail(["bluedrayco@gmail.com"], "saludo", "hola como estas?");
