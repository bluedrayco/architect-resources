<?php
namespace DesignPatterns\Creational\FactoryMethod\Problem;

use PHPMailer\PHPMailer\PHPMailer;

class Gmail
{
    private $user=null;
    private $password=null;
    private $aliasAccount=null;

    public function __construct(string $user, string $password, string $aliasAccount = null)
    {
        $this->user=$user;
        $this->password=$password;
        $this->aliasAccount = $aliasAccount==null?$this->user:$aliasAccount;
    }

    public function send(array $to, string $subject, string $body)
    {
        $mailer = $this->createMailer();
        foreach ($to as $email) {
            $mailer->addAddress($email);
        }
        $mailer->Subject = $subject;
        $mailer->Body    = $body;
        $mailer->send();
    }


    private function createMailer()
    {
        $mailer = new PHPMailer();
        $mailer->isSMTP();
        $mailer->SMTPDebug = 4;
        $mailer->SMTPSecure = 'ssl';
        $mailer->SMTPAuth = true;
        $mailer->Host = 'smtp.gmail.com';
        $mailer->Port = 465;
        $mailer->IsHTML(true);
        $mailer->Username = $this->user;
        $mailer->Password = $this->password;
        $mailer->setFrom($this->user, $this->aliasAccount);
        return $mailer;
    }
}
