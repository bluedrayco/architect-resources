<?php
namespace DesignPatterns\Creational\FactoryMethod\Solution;

use DesignPatterns\Creational\FactoryMethod\Solution\EmailConnector;
use PHPMailer\PHPMailer\PHPMailer;

class Gmail implements EmailConnector
{
    private $mailer =null;
    private $user=null;
    private $password=null;
    private $aliasAccount = null;

    public function __construct(string $user, string $password, string $aliasAccount = null)
    {
        $this->user=$user;
        $this->password=$password;
        $this->aliasAccount = $aliasAccount==null?$this->user:$aliasAccount;
    }

    public function configure(): void
    {
        $this->mailer = new PHPMailer();
        $this->mailer->isSMTP();
        $this->mailer->SMTPDebug = 4;
        $this->mailer->SMTPSecure = 'ssl';
        $this->mailer->SMTPAuth = true;
        $this->mailer->Host = 'smtp.gmail.com';
        $this->mailer->Port = 465;
        $this->mailer->IsHTML(true);
        $this->mailer->Username = $this->user;
        $this->mailer->Password = $this->password;
        $this->mailer->setFrom($this->user, $this->aliasAccount);
    }

    public function addRecepients(array $to): void
    {
        foreach ($to as $email) {
            $this->mailer->addAddress($email);
        }
    }

    public function send(string $subject, string $body): void
    {
        $this->mailer->Subject = $subject;
        $this->mailer->Body    = $body;
        $this->mailer->send();
    }

    public function close(): void
    {
    }
}
