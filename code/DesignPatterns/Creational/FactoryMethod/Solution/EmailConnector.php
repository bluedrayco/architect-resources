<?php
namespace DesignPatterns\Creational\FactoryMethod\Solution;

interface EmailConnector
{
    public function configure(): void;

    public function send(string $subject, string $body): void;

    public function addRecepients(array $to): void;

    public function close(): void;
}
