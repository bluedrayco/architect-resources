<?php
namespace DesignPatterns\Creational\Singleton\Solution;

use DesignPatterns\Creational\Singleton\Solution\Configuration;

class Service
{

    public function getUsers(): array
    {
        $config = Configuration::getInstance('configuration.json');
        $configDatabase = $config->getConfigurationDatabase();
        $data =json_decode(file_get_contents("users.{$configDatabase['host']}.{$configDatabase['username']}.{$configDatabase['password']}.json"), true);
        return $data==null?[]:$data;
    }

    public function sendUsersToWS(array $users)
    {
        $config = Configuration::getInstance('configuration.json');
        $configWS = $config->getConfigurationWS();
        file_put_contents("{$configWS['url']}.{$configWS['user']}.{$configWS['password']}_request.json", json_encode($users));
        return [
            "message"=>"sucess"
        ];
    }
}
