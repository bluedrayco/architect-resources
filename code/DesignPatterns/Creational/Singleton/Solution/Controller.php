<?php

namespace DesignPatterns\Creational\Singleton\Solution;

class Controller
{

    public function sendUsersToWS()
    {
        $service = new Service();
        $users = $service->getUsers();
        return $service->sendUsersToWS($users);
    }
}
