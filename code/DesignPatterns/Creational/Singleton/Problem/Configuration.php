<?php
namespace DesignPatterns\Creational\Singleton\Problem;

class Configuration
{
    private $filename;

    public function __construct(string $filename)
    {
        $this->filename=$filename;
    }

    public function getConfigurationDatabase()
    {
        $config = json_decode(file_get_contents($this->filename), true);
        return $config['database'];
    }

    public function getConfigurationWS()
    {
        $config = json_decode(file_get_contents($this->filename), true);
        return $config['webservice'];
    }
}
