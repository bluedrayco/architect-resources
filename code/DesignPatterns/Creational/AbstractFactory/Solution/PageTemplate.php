<?php
namespace DesignPatterns\Creational\AbstractFactory\Solution;

interface PageTemplate
{
    public function getTemplateString(): string;
}