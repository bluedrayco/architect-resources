<?php
namespace DesignPatterns\Creational\AbstractFactory\Problem;

use Twig\Loader\ArrayLoader;
use Twig\Environment;

class TwigTemplate implements TemplateRenderer
{
    public function render(string $title, string $content):string
    {
        $template = <<<HTML
        <div class="page">
            {{ title }}
            <article class="content">{{ content }}</article>
        </div>
        HTML;
        $loader = new ArrayLoader([
            'index.html' => $template,
        ]);
        $twig = new Environment($loader);
        return $twig->render('index.html', [
            "title"=>$title,
            "content"=>$content
        ]);
    }
}
