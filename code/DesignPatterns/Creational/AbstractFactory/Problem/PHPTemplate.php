<?php
namespace DesignPatterns\Creational\AbstractFactory\Problem;

class PHPTemplate implements TemplateRenderer{
    
    public function render(string $title,string $content):string{
        $template =<<<HTML
        <div class="page">
            $title
            <article class="content">{$content}</article>
        </div>
        HTML;

        return $template;
    }
}