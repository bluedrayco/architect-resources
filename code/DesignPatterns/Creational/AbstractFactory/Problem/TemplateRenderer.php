<?php
namespace DesignPatterns\Creational\AbstractFactory\Problem;


interface TemplateRenderer{

    public function render(string $title, string $content):string;
    
}