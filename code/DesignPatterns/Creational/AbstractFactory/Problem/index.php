<?php

require "../../../../../vendor/autoload.php";

use DesignPatterns\Creational\AbstractFactory\Problem\PHPTemplate;
use DesignPatterns\Creational\AbstractFactory\Problem\TwigTemplate;
use DesignPatterns\Creational\AbstractFactory\Problem\TemplateRenderer;

function renderTemplate(TemplateRenderer $template,string $title,string $content){
    echo $template->render($title,$content)."\n\n";
}

$phpTemplate = new PHPTemplate();
$twigTemplate = new TwigTemplate();

renderTemplate($phpTemplate,"Hola","Mundo");
renderTemplate($twigTemplate,"Hola","Mundo");
