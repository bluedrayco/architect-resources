<?php

require "../../../../../vendor/autoload.php";

use DesignPatterns\Creational\Builder\Solution\MysqlQueryBuilder;
use DesignPatterns\Creational\Builder\Solution\PostgresQueryBuilder;
use DesignPatterns\Creational\Builder\Solution\SQLQueryBuilder;

function clientCode(SQLQueryBuilder $queryBuilder):string
{
    $query = $queryBuilder->select("users", ["name", "email", "password"])
        ->where("age", 18, ">")
        ->where("age", 30, "<")
        ->limit(10, 20)
        ->getSQL();

    return  $query;
}


switch ($argv[1]) {
    case 'Mysql':
        $query =clientCode(new MysqlQueryBuilder());
        break;

    case 'Postgres':
        $query = clientCode(new PostgresQueryBuilder());
        break;
}


echo "query->{$query}";
