<?php
namespace DesignPatterns\Creational\Builder\Problem;

class Mysql
{

    public function select(array $select, array $where = null, array $limit = null)
    {
        $query = "";
        if (count($select)>1) {
            $query.="SELECT {$select[0]} FROM {$select[1]} ";
        } else {
            $query.="SELECT * FROM {$select[0]} ";
        }
        if ($where) {
            $query.="WHERE {$where[0]}{$where[1]}{$where[2]} ";
        }
        if ($limit) {
            $query.="LIMIT  {$limit[0]} , {$limit[1]};";
        } else {
            $query.=";";
        }
        return $query;
    }
}
