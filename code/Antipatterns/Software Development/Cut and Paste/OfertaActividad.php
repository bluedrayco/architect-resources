<?php

/***
 * Clase Business para  operaciones relacionadas a las actividades de la Oferta Educativa
 * @author silvio.bravo@enova.mx
 * @date Dic 09 2014
 *
 */

namespace ControlEscolar\CalendarioBundle\Business\Entity;

use Doctrine\ORM\EntityManager;
use ControlEscolar\CalendarioBundle\Entity\OfertaActividad                   as EOfertaActividad;
use ControlEscolar\CalendarioBundle\Business\Entity\EventoActividadOferta    as BEventoActividadOferta;
use ControlEscolar\CalendarioBundle\Business\Entity\DiaFeriado               as BDiaFeriado;
use ControlEscolar\CalendarioBundle\Business\Entity\OfertaActividadHorario   as BOfertaActividadHorario;

use ControlEscolar\CalendarioBundle\Business\Entity\Escenario                as BEscenario;

use Core\UserBundle\Entity\Usuario;
use Core\CoreBundle\Business\Entity\BusinessEntity;
use Doctrine\DBAL\DBALException;

class OfertaActividad extends BusinessEntity {

    public function __construct(EntityManager $em,EntityManager $emMako=null) {
        $this->construct($em,$emMako);
    }

    /**
     * Obtenemos la estructura de las actividades a partir de un id de oferta educativa y de un escenario id
     * @param  [int] $ofertaEducativaId [oferta educativa]
     * @param  [int] $escenarioId       [escenario]
     * @param  [date] $fechaInicio      [Fecha inicio]
     * @param  [date] $fechaFinal       [Fecha Final]
     * @return [array]                  [estructura de datos de las actividades]
     */
    public function obtenDatos($ofertaEducativaId, $escenarioId, $fechaInicio, $fechaFinal){

        $arregloEscenariosIds   = [];

        if($escenarioId !="todos"){

            $escenario = $this->getRepository(
                "ControlEscolarCalendarioBundle:Escenario",
                array('escenario_id' => $escenarioId ),
                null,
                404,
                null,
                'find',
                null
            );
            if (!$escenario) {
                return null;
            }



            $arregloEscenariosIds   = $this->obtenerIdsEscenariosPorTipoEscenario(
                $escenario,
                $ofertaEducativaId
            );

        }
        else{  //Buscamos los escenarios que coincidan con esta oferta educativa.


            $ofertaEducativa          = $this->getRepository(
                "ControlEscolarCalendarioBundle:OfertaEducativa",
                array('oferta_educativa_id' => $ofertaEducativaId ),
                null,
                404,
                null,
                'find',
                null
            );

            if (!$ofertaEducativa) {
                return null;
            }


            $tmpescenario           = $this->getRepository(
                "ControlEscolarCalendarioBundle:EscenarioOferta",
                array('OfertaEducativa' => $ofertaEducativa),
                null,
                404,
                null,
                'findBy',
                null
            );


            foreach ($tmpescenario as $key => $escenario) {

                $arregloEscenariosIds = array_merge(
                                                    $arregloEscenariosIds,
                                                    $arregloEscenariosIds = $this->obtenerIdsEscenariosPorTipoEscenario(
                                                        $escenario->getEscenario(),
                                                        $ofertaEducativaId
                                                    )
                );

            }

        }




        $OfertaEscenarioEventos = $this->getRepository(
            "ControlEscolarCalendarioBundle:EventoActividadOferta",
            array(
                'oferta_educativa_id' => $ofertaEducativaId,
                "escenario_ids"=>$arregloEscenariosIds,
                "fecha_inicio"=>$fechaInicio,
                "fecha_final"=>$fechaFinal
            ),
            null,
            200,
            null,
            'findEventosForCalendar',
            null
        );

       if (!$OfertaEscenarioEventos) {

            $this->respuesta["data"]["eventos"] = array();
           return $this->respuesta;
       }

       return $this->buildRespuesta(null, array('eventos' => $OfertaEscenarioEventos));
    }

    /**
     * Metodo para obtener un arreglo de ids de escenario basandose en las siguientes premisas:
     * Tipo "todos" se obtendrá el id del escenario Todos
     * Tipo "centro" se obtendrá el id del escenario Todos, el id individual y los ids de los grupos en los que se encuentra el centro agregado
     * Tipo "grupo" se obtendrá el id del escenario Todos además del id representativo del grupo
     * @param mixed $escenario objeto escenario con el que se determinará el tipo de escenario
     * @param integer $ofertaEducativaId oferta educativa a la que esta vinculada el escenario
     * @return array arreglo de identificadores que representan a los escenarios.
     */
    public function obtenerIdsEscenariosPorTipoEscenario($escenario,$ofertaEducativaId){
        $arregloEscenarios=array();
        switch($escenario->getTipoEscenario()->getClave()){

            case 'todos':
                $arregloEscenarios[]=$escenario->getEscenarioId();
            break;

            case 'centro':
                $arregloEscenarios[]=$escenario->getEscenarioId();
                $escenarioBusiness = new BEscenario($this->entityManager,$this->entityManagerMako);

                $idEscenarioTodos = $escenarioBusiness->obtenerIdEscenarioTodos();
                $arregloEscenarios[]=$idEscenarioTodos;

                $idsEscenarioGrupos=$escenarioBusiness->obtenerIdsEscenarioGruposByIdCentro($escenario->getCentro()->getCentroId(),$ofertaEducativaId);
                if(count($idsEscenarioGrupos)>0){
                    $arregloEscenarios=  array_merge($idsEscenarioGrupos,$arregloEscenarios);
                }
            break;

            case 'grupo':
                $arregloEscenarios[]=$escenario->getEscenarioId();
                $escenarioBusiness = new BEscenario($this->entityManager,$this->entityManagerMako);
                $idEscenarioTodos = $escenarioBusiness->obtenerIdEscenarioTodos();
                $arregloEscenarios[]=$idEscenarioTodos;
            break;
        }
        return $arregloEscenarios;
    }


    /**
     * registraActividad description
     * @param  date   $fechaInicio            Fecha en que debe iniciar la programación de los eventos para esta actividad
     * @param  int    $escenarioOfertaId      Id de escenario Oferta
     * @param  int    $actividadAcademicaId   Id de Actividad Academica, puede venir null si no es una actividad academica
     * @param  int    $actividadId            Id de Actividad no Academica, puede venir null si no es una actividad no academica
     * @param  array  $horario                array que contiene el horario de la actividad
     * @param  int    $esquemaId              Id del Esquema en el que se realizará la programación del evento
     * @param  date   $fechaFinal             Fecha en que debe finalizar la programación de los eventos (siempre y cuando sea una actividad No academica)
     * @param  int    $usuarioId              Id de Usuario de la Sesión
     * @return array                          revoldemos un objeto envuelto en un array
     */
    public function registraActividad($fechaInicio,
                                      $escenarioOfertaId,
                                      $actividadAcademicaId,
                                      $actividadId,
                                      $horario,
                                      $esquemaId,
                                      $obligatorio,
                                      $fechaFinal,
                                      $usuarioId,
                                      $controlAcceso){


        //Obtenemos Objetos que serán ocupados en el proceso de registrar la oferta actividad.
        $vtObjetos      = $this->obtenerObjetosParaOfertaActividad($escenarioOfertaId, $actividadAcademicaId, $actividadId, $esquemaId);
        if(!$vtObjetos){
            $this->respuesta["data"]["ofertaactividad"] = array();
           return $this->respuesta;
        }

        //Obtenemos el Usuario que representa la sesión
        $usuario         = $this->getUsuarioById($usuarioId);
        if(!$usuario){
            return $this->respuesta;
        }

        $ofertaActividad        = new EOfertaActividad();
        $BEventoActividadOferta = new BEventoActividadOferta($this->entityManager,$this->entityManagerMako);
        $BDiasFeriados          = new BDiaFeriado($this->entityManager,$this->entityManagerMako);
        $BHorario               = new BOfertaActividadHorario($this->entityManager,$this->entityManagerMako);
        $escenarioOferta        = $vtObjetos["escenarioOferta"];
        $actividad              = $vtObjetos["actividad"];
        $actividadAcademica     = $vtObjetos["actividadAcademica"];
        $esquema                = $vtObjetos["esquema"];
        $totalHoras             = ($vtObjetos["actividadAcademica"]!=null)?$actividadAcademica->getDuracionHorasPresenciales():0;
        $diasFeriados           = $BDiasFeriados->obtencionDiasFeriados();

        /** #########  Iniciamos La Transacción para toda la Operación  ######### */
        $this->entityManager->beginTransaction();

        try{


            $fechas         = $BEventoActividadOferta->generaFechasEventos($horario, $totalHoras,$fechaInicio, $diasFeriados,$fechaFinal);

            if(count($fechas["fechas"]) <=0){
                $this->entityManager->rollback();
                $this->buildRespuesta('No se ha podido realizar la operación solicitada ya que no se encontro fecha para el horario seleccionado',array('ofertaactividad' =>  false),false,404);
                return $this->respuesta;
            }

            // Generamos la oferta Actividad
            $ofertaActividad->setUsuario($usuario);
            $ofertaActividad->setEscenarioOferta($escenarioOferta);
            $ofertaActividad->setActividad($actividad);
            $ofertaActividad->setActividadAcademica($actividadAcademica);
            $ofertaActividad->setFechaInicio(new \DateTime($fechas["fechaInicio"]));
            $ofertaActividad->setFechaFin(new \DateTime($fechas["fechaFinal"]));
            $ofertaActividad->setFechaAlta(new \DateTime());
            $ofertaActividad->setActivo(true);
            $ofertaActividad->setExtemporanea($escenarioOferta->getOfertaEducativa()->getSincronizado());
            $ofertaActividad->setPuedePublicar(false);
            $ofertaActividad->setSincronizado(false);
            $ofertaActividad->setActividadAcademicaEsquema($esquema);
            //Si el control de acceso es nulo se queda igual
            if($controlAcceso != null){
                $ofertaActividad->setControlAcceso($controlAcceso);
            }

            $ofertaActividad->setObligatoria($obligatorio);
            $this->entityManager->persist($ofertaActividad);
            //Fin de generar la oferta Actividad
            $BHorario->generaHorario($ofertaActividad,$horario,$usuario);                            //Generamos el horario.
            $BEventoActividadOferta->generaEventos($ofertaActividad, $fechas["fechas"],$usuario);   //Generamos los eventos.
            $this->entityManager->flush();
            $this->entityManager->commit();
            $this->buildRespuesta('post',array('ofertaactividad' => $ofertaActividad));
        }
        catch (DBALException $ex) {
            $this->entityManager->rollback();
            $this->buildRespuesta('No se ha podido realizar la operación solicitada',array('ofertaactividad' =>  false),false,404);
        }
        return $this->respuesta;
    }




    /**
     * Obtenemos los objetos de actividad no academica o actividad academica y su esquema si es el caso
     * @param  int $actividadAcademicaId actividad academica id
     * @param  int $actividadId          actividad no academica id
     * @param  int $esquemaId            id de tra_actividad_academica_esquema
     * @return array|boolean             false si ocurrio un error
     */
    private function obtenerObjetosParaOfertaActividad($escenarioOfertaId,$actividadAcademicaId, $actividadId, $esquemaId){


        $vtObjetos       = array("escenarioOferta"=>null, "actividadAcademica"=>null, "actividad"=>null, "esquema"=>null);

        // Obtenemos el escenario Oferta

        $escenarioOferta = $this->getRepository(
                             "ControlEscolarCalendarioBundle:EscenarioOferta",
                             array('escenario_oferta_id' => $escenarioOfertaId ),
                             null,
                             404,
                             null,
                             'find',
                             null
                         );

        if(!$escenarioOferta){
            $this->buildRespuesta(
                      "No se encontro el Escenario Oferta con el id {$escenarioOfertaId}",
                      array('ofertaactividad' =>  array()),
                        false,
                        404
                   );
            $this->log->error("No se encontro el Escenario Oferta con el id {$escenarioOfertaId}");
            return $vtObjetos;
        }

        $vtObjetos["escenarioOferta"] = $escenarioOferta;

        if($actividadId != null){                                           //Es una Actividad No Academica


            //Obtenemos el Objeto de la Actividad No Academica
            $actividad              = $this->getRepository(
                                               "ControlEscolarPlanAnualBundle:Actividad",
                                               array("actividad_id"=>$actividadId),
                                               null,
                                               404,
                                               null,
                                               'find',
                                               null
                                            );
            if(!$actividad){
                $this->buildRespuesta(
                          "No se encontro la actividad  No academica con el id {$actividadId}",
                          array('ofertaactividad' =>  array()),
                            false,
                            404
                        );
                $this->log->error("No se encontro la actividad  No academica con el id {$actividadId}");
                return $vtObjetos;

            }
            $vtObjetos["actividad"] = $actividad;

        }
        else{                                                           //Se trata de una actividad Academica :D


            // Obtenemos el objeto de la actividad Academica.

            $actividadAcademica =  $this->getRepository(
                                              "CoreCoreBundle:ActividadAcademica",
                                              array("actividad_academica_id"=>$actividadAcademicaId),
                                              null,
                                              404,
                                              null,
                                              'find',
                                              null
                                            );
            if(!$actividadAcademica){

                $this->buildRespuesta(
                            'No se encontro la actividad academica con el id ' .$actividadAcademicaId,
                            array('ofertaactividad' =>  array()),
                            false,
                            404
                        );
                $this->log->error('No se encontro la actividad academica con el id ' .$actividadAcademicaId);
                return $vtObjetos;

            }
            $vtObjetos["actividadAcademica"] = $actividadAcademica;

            $esquema            = $this->getRepository(
                                            "CoreCoreBundle:ActividadAcademicaEsquema",
                                            array("actividad_academica_esquema_id"=>$esquemaId),
                                            null,
                                            404,
                                            null,
                                            'find',
                                            null
                                        );
            if(!$esquema){
                $this->buildRespuesta(
                            'No se encontro la actividad academica esquema con el id '. $esquemaId,
                            array('ofertaactividad' =>  array()),
                            false,
                            404
                        );
                $this->log->error('No se encontro la actividad academica esquema con el id '. $esquemaId);
                return $vtObjetos;
            }
            $vtObjetos["esquema"] = $esquema;
        }

        return $vtObjetos;

    }

/**
 * eliminación de una ofertaActividad en base a su identificador, eliminando eventos y horarios generados
 * @param integer $ofertaActividadId identificador de la oferta actividad a eliminar
 * @return mixed objeto con la respuesta positiva o negativa del borrado de la oferta actividad
 */

    public function eliminaOfertaActividad($ofertaActividadId){
        $ofertaActividad = $this->getRepository(
                             "ControlEscolarCalendarioBundle:OfertaActividad",
                             array('oferta_actividad_id' => $ofertaActividadId ),
                             null,
                             404,
                             null,
                             'find',
                             null
                         );
        if(!$ofertaActividad){
            $this->buildRespuesta('No se ha encontrado una Oferta Actividad con el Id proporcionado',array('ofertaactividad' =>  false),false,404);
            return $this->respuesta;
        }


        $BEventoActividadOferta = new BEventoActividadOferta($this->entityManager,$this->entityManagerMako);
        $BHorario               = new BOfertaActividadHorario($this->entityManager,$this->entityManagerMako);

        $this->entityManager->beginTransaction();

        try{
             $BEventoActividadOferta->eliminaEventosDesdeOfertaActividad($ofertaActividad);
             $BHorario->eliminaHorarioDesdeOfertaActividad($ofertaActividad);
             $this->entityManager->remove($ofertaActividad);
             $this->entityManager->flush();
             $this->entityManager->commit();
             $this->buildRespuesta('delete',array('ofertaactividad' =>  true));
        }
        catch (DBALException $ex) {
            $this->entityManager->rollback();
            $this->buildRespuesta('Ocurrio un error al momento de eliminar la Oferta Actividad',array('ofertaactividad' =>  false),false,404);
        }

        return $this->respuesta;
    }

     /**
     * Actualizacion de una Oferta Actividad contemplando los eventos y horarios
     * @param  date   $obj[fecha_inicio]             Fecha en que debe iniciar la programación de los eventos para esta actividad
     * @param  int    $obj[escenario_oferta_id]      Id de escenario Oferta
     * @param  int    $obj[actividad_academica_id]   Id de Actividad Academica, puede venir null si no es una actividad academica
     * @param  int    $obj[actividad_id]             Id de Actividad no Academica, puede venir null si no es una actividad no academica
     * @param  array  $obj[horario]                  array que contiene el horario de la actividad
     * @param  int    $obj[esquema_id]               Id del Esquema en el que se realizará la programación del evento
     * @param  date   $obj[fecha_final]              Fecha en que debe finalizar la programación de los eventos (siempre y cuando sea una actividad No academica)
     * @param  int    $obj[usuario_id]               Id de Usuario de la Sesión
     * @return array                                 devoldemos un objeto envuelto en un array
     */
    public function actualizar($obj){
        //Obtenemos el Usuario que representa la sesión
        $usuario         = $this->getUsuarioById($obj['usuario_id']);
        if(!$usuario){
            return $this->respuesta;
        }
        $escenarioOfertaId    = array_key_exists('escenario_oferta_id',$obj)?$obj['escenario_oferta_id']:null;
        $actividadAcademicaId = array_key_exists('actividad_academica_id', $obj)?$obj['actividad_academica_id']:null;
        $actividadId          = array_key_exists('actividad_id',$obj)?$obj['actividad_id']:null;
        $esquemaId            = array_key_exists('esquema_id', $obj)?$obj['esquema_id']:null;
        //Obtenemos Objetos que serán ocupados en el proceso de registrar la oferta actividad.
        $vtObjetos      = $this->obtenerObjetosParaOfertaActividad($escenarioOfertaId, $actividadAcademicaId, $actividadId, $esquemaId);
        if(!$vtObjetos){
            $this->respuesta["data"]["ofertaactividad"] = false;
           return $this->respuesta;
        }
        //definición de Business que se usarán.
        $BEventoActividadOferta = new BEventoActividadOferta($this->entityManager,$this->entityManagerMako);
        $BDiasFeriados          = new BDiaFeriado($this->entityManager,$this->entityManagerMako);
        $BHorario               = new BOfertaActividadHorario($this->entityManager,$this->entityManagerMako);

        $escenarioOferta        = $vtObjetos["escenarioOferta"];
        $actividad              = $vtObjetos["actividad"];
        $actividadAcademica     = $vtObjetos["actividadAcademica"];
        $esquema                = $vtObjetos["esquema"];
        $totalHoras             = ($vtObjetos["actividadAcademica"]!=null)?$actividadAcademica->getDuracionHorasPresenciales():0;
        $diasFeriados           = $BDiasFeriados->obtencionDiasFeriados();
        $fechaFinal             = $obj['fecha_final'];

        //obtenermos la OfertaActividad a modificar
        $ofertaActividad = $this->getRepository(
                             "ControlEscolarCalendarioBundle:OfertaActividad",
                             array('oferta_actividad_id' => $obj['oferta_actividad_id'] ),
                             null,
                             404,
                             null,
                             'find',
                             null
                         );

        if(!$ofertaActividad){
            $this->buildRespuesta('No se ha encontrado una Oferta Actividad con el Id proporcionado',array('ofertaactividad' =>  false),false,404);
            return $this->respuesta;
        }
        $this->entityManager->beginTransaction();

        try{
            $fechas         = $BEventoActividadOferta->generaFechasEventos($obj['horario'], $totalHoras,$ofertaActividad->getFechaInicio()->format('Y-m-d'), $diasFeriados,$fechaFinal);


            if(count($fechas["fechas"]) <=0){
                $this->entityManager->rollback();
                $this->buildRespuesta('No se ha podido realizar la operación solicitada ya que no se encontro fecha para el horario seleccionado',array('ofertaactividad' =>  false),false,404);
                return $this->respuesta;
            }


            //modificación elementos propios de OfertaActividad
            if (array_key_exists('escenario_oferta_id',$obj)) {
                $ofertaActividad->setEscenarioOferta($escenarioOferta);
            }

                $ofertaActividad->setActividad($actividad);
                $ofertaActividad->setActividadAcademica($actividadAcademica);
                //modificación elementos propios de OfertaActividad
            if (array_key_exists('esquema_id',$obj)) {
                $ofertaActividad->setActividadAcademicaEsquema($esquema);
            }

            if (array_key_exists('fechaInicio', $fechas)){
                $ofertaActividad->setFechaInicio(new \DateTime($fechas["fechaInicio"]));
            }

            if (array_key_exists('fechaFinal', $fechas)){
                $ofertaActividad->setFechaFin(new \DateTime($fechas["fechaFinal"]));
            }

            if (array_key_exists('obligatorio', $obj)){
                $ofertaActividad->setObligatoria($obj['obligatorio']);
            }

            //Si el control de acceso es nulo se queda igual
            if(array_key_exists('control_acceso', $obj)){
                $ofertaActividad->setControlAcceso($obj['control_acceso']);
            }

            $ofertaActividad->setExtemporanea($escenarioOferta->getOfertaEducativa()->getSincronizado());
            $ofertaActividad->setSincronizado(false);

            $this->entityManager->flush();
            //eliminar relaciones Eventos y Horarios
             $BEventoActividadOferta->eliminaEventosDesdeOfertaActividad($ofertaActividad);
             $BHorario->eliminaHorarioDesdeOfertaActividad($ofertaActividad);

             //creacion de los Eventos y Horarios
             $BHorario->generaHorario($ofertaActividad,$obj['horario'],$usuario);                  //Generamos el horario.
             $BEventoActividadOferta->generaEventos($ofertaActividad, $fechas["fechas"],$usuario); //Generamos los eventos.
             $this->entityManager->flush();
             $this->entityManager->commit();
             $this->buildRespuesta('put',array('ofertaactividad' =>  true));
        }
        catch (DBALException $ex) {
            $this->entityManager->rollback();
            $this->buildRespuesta('Ocurrio un error al momento de eliminar la Oferta Actividad',array('ofertaactividad' =>  false),false,404);
        }
        return $this->respuesta;
    }
}
?>
