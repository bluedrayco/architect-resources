Symptoms And Consequences
- Redundant navigation paths.
- Transient associations.
- Stateless classes.
- Temporary, short-duration objects and classes.
- Single-operation classes that exist only to "seed" or "invoke" other classes through temporary 
associations.
- Classes with "control-like" operation names such as start_process_alpha.

Typical Causes
- Lack of object-oriented architecture. "The designers don't know object orientation."
- Incorrect tool for the job. Contrary to popular opinion, the object-oriented approach 
isn't necessarily the right solution for every job. As a poster once read, "There is no 
right way to do the wrong thing." That is to say, if object orientation isn't the right tool, 
there's no right way to implement it.
- Specified disaster. As in the Blob, management sometimes makes architectural committments 
during requirements analysis. This is inappropriate, and often leads to problems like this AntiPattern.