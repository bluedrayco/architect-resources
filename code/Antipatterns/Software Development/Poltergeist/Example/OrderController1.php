<?php

class OrderController
{
    public function payOrder($orderId)
    {
        if(isset($_POST['Order'])) {
            $order = Order::findOrFail($orderId);
            $invoice = new Invoice();
            $invoice->setOrder($order);
            $invoice->setUser(Auth::user());

            if($invoice->save()) {
                $email = new Email();
                $email->setSubject('Invoice for order#' . $order->id);

                // ...

                if($email->send()) {
                    $this->redirectTo('orders');
                } else {
                    // ... fail
                }
            } else {
                // ... fail
            }
        }
    }
}
