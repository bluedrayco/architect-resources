<?php

class InvoicePaymentHandler
{
    public function make(array $data)
    {
        $order = Order::findOrFail($orderId);
        $invoice = new Invoice();
        $invoice->setOrder($order);
        $invoice->setUser(Auth::user());

        if($invoice->save()) {
            $email = new Email();
            $email->setSubject('Invoice for order#' . $order->id);
            ...

            return $email->send();
        } 

        return false;
    }
}