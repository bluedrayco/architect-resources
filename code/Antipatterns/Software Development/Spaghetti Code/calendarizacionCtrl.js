/*
 * @author Jaime Hernández Cruz
 * @description Controlador principal del calendario de calendarizacion en centro
 *
 * @param {angular} $scope
 * @param {angular} $compile
 * @param {api} uiCalendarConfig
 * @param {factory} ofertaEducativaFactory    -- Factoria que contiene las funciones para la comunicacion con otros controladores
 * @param {factory} eventoFactory             -- Factoria que contiene las funciones de evento para la comunicacion con otros controladores
 * @param {service} ofertaEducativaService    -- Servicio que gestiona las ofertas educativas con el server
 * @returns {undefined}
 */
function calendarizacionCtrl(
    $scope
    ,$rootScope
    ,$compile
    ,uiCalendarConfig
    ,ofertaEducativaFactory
    ,filtrosFactory
    ,eventoFactory
    ,ofertaEducativaCentroService
    ,ofertaCalendarioCentroService
    ,estadisticaFactory
    ,centroFactory
    ,centroParametrosService
    ,loaderFactory
    ) {


  var date = new Date();
var d    = date.getDate();
var m    = date.getMonth();
var y    = date.getFullYear();
$scope.events                   = [];
ofertaEducativaFactory.eventosMako        = [];
$scope.eventSources             = [];
$scope.ofertaActividad          = {};
$scope.ofertaFechaFinal         = null;
$scope.centroFactory            = {}; //Para ocultar el calendario
$scope.variableEstatus          = {
                                    primerGetActividadesOfertaEscenario : true
                                  };
/**
 * Nombre del tipo de vista Mensual del Calendario. Por defecto 'month'
 * @type String
 * @constant
 */
var VIEW_MONTH = 'month';
/**
 * Nombre del tipo de vista Semanal del Calendario. Por defecto 'agendaWeek'
 * @type String
 * @constant
 */
var VIEW_WEEK = 'agendaWeek';
/**
 * Nombre del tipo de vista por Día del Calendario. Por defecto 'agendaDay'
 * @type String
 * @constant
 */
var VIEW_DAY = 'agendaDay';


/*
 * Agregando las funciones para que cargue el inicio
 **/
$scope.init = function(fechaSistema,fechaHoraSistema,timezone,ambiente){
    //Se carga la funcion en la factoria para ocuparla en el controladore de calendarizacionCentral tmb
    centroFactory.cargaInicialCalendarizacion = cargaInicialCalendarizacion;
    $scope.centroFactory = centroFactory;

    centroFactory.fechaSistema          = fechaSistema;
    centroFactory.fechaHoraSistema      = new Date(fechaHoraSistema);
    centroFactory.eligiendoCentroFuente = $scope.eligiendoCentroFuente;
    $scope.timezoneSistema              = timezone;

    if(ambiente == 'local'){
        centroFactory.cargaInicialCalendarizacion();
        centroFactory.mostrarCalendario = true;
    }

}
/*
 * Función que sirve en calendarizacion desde central y ejecuta las accioens cuando es llamada
 * @returns {undefined}
 */
$scope.eligiendoCentroFuente = function(){
    $scope.centroFactory.mostrarCalendario = true;
    centroFactory.cargaInicialFiltros();
    centroFactory.cargaInicialEvento('calendarizacion','calendarizacionCentral');
    centroFactory.cargaInicialCalendarizacion();
}

function cargaInicialCalendarizacion(){
    ofertaEducativaFactory.renderActividadesEnCalendario = $scope.renderActividadesEnCalendario;   // Cargando la funcion en la variable por
                                                                                                   // referencia contenida en la factoria
                                                                                                   // esto para usarla en otros controladores
                                                                                                   // referencia contenida en la factoria
    ofertaEducativaFactory.irAFechaCalendario           = $scope.irAFechaCalendario;
    ofertaEducativaFactory.eventosMako = [];

    //Se obtienen los dias laborales
    ofertaEducativaCentroService.restResource.getDiasFeriados(
        {
            hostCentro: limpiarUri(centroFactory.centroActual.ip)
        },
        {},
        function(data){
            ofertaEducativaFactory.diasFeriados = data;
            if(!ofertaEducativaFactory.ofertaEducativa){
                toastr.success("No hay días no laborables");
            }else{
                toastr.success("Días no laborables cargados");
            }

        }
    );

      //  Se agrega al array de eventos de mako los eventos
      //  para bindear en el calendario
      obtenerEventosMako();

      //Inicializacion de parametros
      centroParametrosService.restResource.getParametros(
                                      {hostCentro: limpiarUri(centroFactory.centroActual.ip)}
                                      ,{}, function(data){
                                          if(data.length > 0){
                                            centroFactory.cargaInicialParametros(data);
                                            $scope.configurarCalendario();
                                          }else{
                                              loaderFactory.loader.remove();
                                              loaderFactory.loader = null;
                                          }
                                          bindeoCalendario();
                                      });
}


/*
* Obtiene los eventos calendarizados en Mako
* @returns {undefined}
*/
function obtenerEventosMako(){
 //Se obtienen los eventos de Mako por medio de un HARDCODE
    ofertaEducativaCentroService.restResource.getIpMako(
        {
            hostCentro: limpiarUri(centroFactory.centroActual.ip)
        },
        {},function(data){
            //Evalua si encontro la Ip de Mako
            if(data){
                //Se realiza la petición a Mako
                ofertaEducativaCentroService.restResource.getEventosMako(
                {
                    hostMako: limpiarUri(data.ip),
                    fecha: '2016-02-18'
                },
                {},
                function(data){
                    ofertaEducativaFactory.eventosMako = data;
                    if(!ofertaEducativaFactory.ofertaEducativa){
                        toastr.success("No hay eventos de mako");
                    }else{
                        toastr.success("Eventos de mako cargados");
                    }

                });

            }else{
                ofertaEducativaFactory.eventosMako = [];
                toastr.error("Fallo la comunicación con Mako 1.4")
            }
        }
       );
}
//Bindeo inicial del calendario una vez se obtienen los parametros iniciales
function bindeoCalendario(){
    if(loaderFactory.loader == null) loaderFactory.loader = loader.modal();
    ofertaEducativaCentroService.restResource.getOfertaEducativaVigente(
        {hostCentro: limpiarUri(centroFactory.centroActual.ip)},
        {},
        function(data){
            if(data.ofertaseducativascentros.length == 0){
                toastr.warning("No existe programación CE activa");
                ofertaEducativaFactory.ofertaEducativa = {};
                if(loaderFactory.loader != null){
                  loaderFactory.loader.remove();
                  loaderFactory.loader = null;
                }
            }else{
                ofertaEducativaFactory.ofertaEducativa  =  data.ofertaseducativascentros[0];
                ofertaEducativaFactory.ofertaEducativa.fecha_inicio = getSoloFecha(new Date(data.ofertaseducativascentros[0].fecha_inicio));
                ofertaEducativaFactory.ofertaEducativa.fecha_fin    = getSoloFecha(new Date(data.ofertaseducativascentros[0].fecha_fin));

                filtrosFactory.cargaFiltroInicialCalendario(function(){
                    $scope.configurarCalendario();                          //Configuración inicial del calendario
                });
            }
        }
    );
}

$scope.getSoloFecha = function(valor){
  var mesFinal              =  ((valor.getMonth()  + 1) <10)?("0"+(valor.getMonth()  + 1)):(valor.getMonth()  + 1);
  var diaFinal              =  ( valor.getDate() <10 )?("0" + valor.getDate()):valor.getDate();
  var fin                   =  valor.getFullYear()  + "-" + mesFinal  + "-" + diaFinal;
  return fin;
}



/* event source that calls a function on every view switch */
$scope.eventsF = function (start, end, timezone, callback) {
  var s = new Date(start).getTime() / 1000;
  var e = new Date(end).getTime() / 1000;
  var m = new Date(start).getMonth();
  var events = [{title: 'Feed Me ' + m,start: s + (50000),end: s + (100000),allDay: false, className: ['customFeed']}];
  callback(events);
};

$scope.calEventsExt = {
   color: '#f00',
   textColor: 'yellow',
   events: [
      //{type:'party',title: 'Click for Google',start: new Date(y, m, 28),end: new Date(y, m, 29),url: 'http://google.com/'}
    ]
};

/* alert on eventClick */
$scope.eventEdition = function( date, jsEvent, view){
    if(date.esFeriado != undefined){
        toastr.warning(ofertaEducativaFactory.mensajes.noEventoDiaLaboral);
    }
    else if(date.className == 'categoria-eventoMako'){
        toastr.warning(ofertaEducativaFactory.mensajes.noEventoMako);
    }
    else if(date.externo == true && date.nombreOfertaExterna != 'false' ){
        toastr.warning("La actividad pertenece a la programación CE: <strong>" + date.nombreOfertaExterna + "</strong>");
    }
    else{
      eventoFactory.editarEvento(date);
        $("#idModalEventos").modal("show");
    }
};

/* alert on Drop */
 $scope.alertOnDrop = function(event, delta, revertFunc, jsEvent, ui, view){


             var data = {
                 hora_inicio:   event.start.format("HH:mm:ss"),
                 hora_fin:      event.end.format("HH:mm:ss"),
                 fecha_evento:  event.end.format("YYYY-MM-DD")
             }

             var fechaFin        = new Date(data.fecha_evento+" " + data.hora_fin);
             var fechaInicio     = new Date(data.fecha_evento+" " + data.hora_inicio);
             var error = ofertaEducativaFactory.verificarDiaFeriado(event.start);

             //si es de tipo evento la modificacion verifica que no sea mayor el evento

            eventoFactory.eventoModificable(
                true,
                event.start._d,
                centroFactory.fechaHoraSistema,
                centroFactory.HorasMinimasActualizarSession
                );

             /*if(!eventoFactory.fechaEventoModificable){
                toastr.warning(eventoFactory.mensajes.fechaLimite());
                error = true;
             }*/
              if(event.externo == true && event.nombreOfertaExterna != 'false' ){
                toastr.warning("La actividad pertenece a la programación CE: <strong>" + event.nombreOfertaExterna + "</strong>");
                error = true;
             }
             else if(fechaFin < fechaInicio){
                toastr.error("Hora no válida");
                error = true;
              }
             else if(fechaInicio < new Date(ofertaEducativaFactory.ofertaEducativa.fecha_inicio)){
                toastr.warning("Evento fuera de la oferta educativa");
                error = true;
             }
             else if(parseInt(event.start.format("mm")) != 0 && parseInt(event.start.format("mm")) != 30){
                toastr.warning("Hora no válida, no es posible calendarizar en este horario");
                error = true;
             }
             else if(event.start.hour() < centroFactory.horarioCentro.apertura || event.end.hour() > centroFactory.horarioCentro.cierre){
                toastr.warning("Hora no válida, evento fuera horario del permitido");
                error = true;
             }
             //Si la fecha inicial del evento que se quiere mover es la fecha
             //inicio de la actividad y la fecha inicio es fija
             //y también la fecha nueva es distinta de la fecha vieja; entonces,
             //no se permite mover a ningun lado
            else if(
                    event.start._i.substring(0,10) == getSoloFecha(new Date(event.fecha_inicio))
                    && event.start._i.substring(0,10) != getSoloFecha(new Date(event.start._d))
                    && (event.hasOwnProperty('control_acceso') 
                        && event.control_acceso.hasOwnProperty('obligatorio')
                        && event.control_acceso.obligatorio.indexOf('fecha_inicio') > -1
                        )
                    ){
                toastr.warning("No es posible mover el evento, la fecha inicio es fija");
                error = true;
            }

            if(!error){
                var evento_id  = event.evento_id;
                eventoFactory.editarSoloPorEvento(data,evento_id,function(){revertFunc()});
             }
            else{
                revertFunc();
            }

};

/* alert on Resize */
$scope.alertOnResize = function(event, delta, revertFunc, jsEvent, ui, view ){
   $scope.alertMessage = ('Event Resized to make dayDelta ' + delta);
};

/* add and removes an event source of choice */
$scope.addRemoveEventSource = function(sources,source) {
  var canAdd = 0;
  angular.forEach(sources,function(value, key){
    if(sources[key] === source){
      sources.splice(key,1);
      canAdd = 1;
    }
  });
  if(canAdd === 0){
    sources.push(source);
  }
};

/* add custom event*/
$scope.addEvent = function(titulo,fechaInicio,fechaFin,clase) {
  $scope.events.push({
    title           : titulo,
    start           : fechaInicio,
    end             : fechaFin,
    className       : clase,
    durationEditable: false,
  });
};

/*
 * Evento que se detona al dar clic sobre el dia
 *
 * @param {Object} contiene la fecha y hora respecto al timezone
 * @returns {undefined}
 */
$scope.agregarEventoXDia = function(date,jsEvent,view) {
    if(view.name == "month")
        date = date.zone(new Date().getTimezoneOffset()/-60);
    ofertaEducativaFactory.verificarFechaCreacion(
            date
            ,eventoFactory
            ,$scope.timezoneSistema
            ,centroFactory.fechaHoraSistema);
}




$scope.evaluaDiaFeriado  = function(date){
  var inicio = null;
  var fin    = null;
  var fecha  = new Date(date);
  for(var n =0; n < ofertaEducativaFactory.diasFeriados.length; n++){
    inicio = new Date(ofertaEducativaFactory.diasFeriados[n].start);
    fin    = new Date(ofertaEducativaFactory.diasFeriados[n].end);

    if(fecha >= inicio && date <= fin){
      return true;
    }
  }
  return false;

}

/* remove event */
$scope.remove = function(index) {
  $scope.events.splice(index,1);
};

/* Change View */
$scope.changeView = function(view,calendar) {
  //uiCalendarConfig.calendars[calendar].fullCalendar('changeView',view);
};

/* Change View */
$scope.renderCalender = function(calendar) {
  if(uiCalendarConfig.calendars[calendar]){
    uiCalendarConfig.calendars[calendar].fullCalendar('render');
  }
};

 /**
  * @ng-doc method
  * @name controlEscolar.calendarizacionCtrl#eventRender
  * @methodOf controlEscolar.calendarizacionCtrl
  * @param {Object} event Objeto  de tipo evento contiene las propiedades a renderear
  * @param {Object} element Elemento html que correspondera el rendereo.
  * @param {Object} view Vista propiedades visuales del evento.
  * @description
  * Render Tooltip. Renderea la vista para cada evento en el calendario de acuerdo al tipo,
  * Obligatoriedad,si es extemporaneo, tipo de actividad,etc.
  */
$scope.eventRender = function( event, element, view ) {
    if(event.hasOwnProperty("tipo")){
        if(event.tipo != "centro"){
            element.addClass("eventoOfertaEducativa");
        }
    }
    //Evalua si el evento es externo, es decir que pertenezca a otra ofertaeducativa
    if(event.hasOwnProperty("externo")){
        if(event.externo == true){
            element.addClass("eventoExterno");
            element.find('.fc-title').before(
                jQuery('<i class="fa fa-external-link  icono-evento" />&nbsp;').text("")
            );
        }
    }
    //Se agrega el icono y el nombre de evento Mako
    if(event.className == "categoria-eventoMako"){
                element.find('.fc-title').before(
                jQuery('<div  />&nbsp;').text("Evento Mako")
                );
                element.find('.fc-title').before(
                jQuery('<div class="fa fa-cloud  icono-evento"  />&nbsp;').text("")
                );
    }

    if(event.esFeriado != undefined){
       return false;
    }

    // evalua si tiene la propiedad de actividad academica y si es así evalua su estado booleano
    // de ser verdadero agrega el icono de actividad academica, despues evalua si es obligatoria
    // y agrea el icono fa-lock para identificar eventos obligatorios. Los iconos son con Boostrap3.*
    if(event.hasOwnProperty("es_academica")){
        var iconoActividad = event.es_academica? "fa-graduation-cap":"fa fa-cog";
        element.find('.fc-title').before(
            jQuery('<i class="fa '+iconoActividad+' icono-evento" />').text("")
              );
        if(event.obligatorio){
           element.find('.fc-title').before(
            jQuery('<i class="fa fa-lock icono-evento" />').text("")
              );
        }
    }
    //evalua si tiene la bandera extemporanea y evalua su estado booleano 
    //  agrega el icono fa-plus-square para identificar programación extemporanea
    if(event.hasOwnProperty("extemporanea")){
      if(event.extemporanea){
        element.find('.fc-title').before(
          jQuery('<i class="fa fa-plus-square icono-evento" />').text(""));
      }
    }


    if(event.hasOwnProperty("aula_nombre_actividad")){
        /*
         * Tooltip
         *
         * Se crea el Tooltip manejado desde jquery para poder cachar los eventos de manera directa.
         * Le enviamos la posicón a la cual se acomodará la posición del Tooltip y el mensaje del Tooltip.
         *
         * @author Bet Gader Porcayo Juárez <bet.porcayo@enova.mx>
         * @since Jun 30 2015
         * @version 1.0
         *
         * @link http://www.tutorialrepublic.com/twitter-bootstrap-tutorial/bootstrap-tooltips.php
         */
        var TOOLTIP_EVENT_SHOW = 'show.bs.tooltip';
        var TOOLTIP_EVENT_HIDE = 'hide.bs.tooltip';
        element.tooltip(
            {
                placement: $scope._getTooltipPosition(event, view),
                title: $scope._getTooltipMessage(event)
            })
            .on(TOOLTIP_EVENT_SHOW, function () {
                // Para la vista Mensual se añade z-index a los renglones (por semana) para
                // que el tooltip se vea sin complicaciones
                if (view.name === VIEW_MONTH) {
                    $scope._changeZIndexWeekToMonth(element, "show");
                }
            })
            .on(TOOLTIP_EVENT_HIDE, function () {
                // Regresamos a su estado original el z-index del renglon modificado de la vista mensual
                if (view.name === VIEW_MONTH) {
                    $scope._changeZIndexWeekToMonth(element, "hide");
                }
            });
        /*
         * Tooltip - end
         */
    }


    element.find('.fc-title').after(
        jQuery('<div class="event-nombre-aula"/>').text("")
      );
    var tipoIcono ="";
    switch(event.tipo_escenario){
      case "todos":
        tipoIcono = "fa-home";
      break;
      case "grupo":
        tipoIcono = "fa-group";
      break;
      case "centro":
        tipoIcono = "fa-male";
      break;
    }
    element.find('.fc-title').before(
        jQuery('<i class="fa '+tipoIcono+' icono-evento" />').text("")
    );


    $compile(element)($scope);
};



/**
 * Event After All Events
 * Ejecutamos la acción para despues de cargar todos las actividades
 *
 * @author Bet Gader Porcayo Juárez <bet.porcayo@enova.mx>
 * @since Jun 30 2015
 * @version 1.0
 *
 * @link http://fullcalendar.io/docs/
 * @link http://fullcalendar.io/docs/event_rendering/eventAfterAllRender/
 **/
$scope.eventAfterAllRender = function(view) {

    // Ejecutamos la acción para calcular el alto del calendario
    $scope._heightCalendar(view);
};

/*
 * Obtiene los eventos correspondientes al calendario y los renderea en el mismo
 * @returns {undefined}
 */
$scope.renderActividadesEnCalendario = function(){
    $scope.calendarioActual = uiCalendarConfig.calendars["ofertaEducativaCalendar"];
    $scope.calendarioActual.fullCalendar('refetchEvents');

}

/*
 * Setea ek calendario en una nueva fecha
 * @param {type} nuevaFecha nueva fecha a donde se colocara el calendario
 * @returns {undefined}
 */
$scope.irAFechaCalendario = function(nuevaFecha){
    $scope.calendarioActual = uiCalendarConfig.calendars["ofertaEducativaCalendar"];
    $scope.calendarioActual.fullCalendar( 'gotoDate', nuevaFecha );
    $scope.evaluaVista($scope.calendarioActual.fullCalendar( 'getView' ));
}


/*
 * Genera la configuración del calendario
 * */
$scope.configurarCalendario = function(){
    $scope.uiConfig = {                                                                                 // configurando el calendario
      calendar:{

        editable         : true,
        lang              :'es',
        header:{
          left: 'prev,next today',
          center: 'title',
          //right: '',
          right: 'month,agendaWeek,agendaDay'
        },
        defaultView      : 'agendaWeek',
        monthNames       : ['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'],
        monthNamesShort  : ['Ene','Feb','Mar','Abr','May','Jun','Jul','Ago','Sep','Oct','Nov','Dic'],
        dayNames         : ['Domingo','Lunes','Martes','Miércoles','Jueves','Viernes','Sábado'],
        dayNamesShort    : ['Dom','Lun','Mar','Mié','Jue','Vie','Sáb'],
        buttonText       : {
                            today: 'hoy',
                            month: 'mes',
                            week: 'semana',
                            day: 'día'
                         },
        eventClick       : $scope.eventEdition,
        eventDrop        : $scope.alertOnDrop,
        eventResize      : $scope.alertOnResize,
        eventRender      : $scope.eventRender,
        dayClick         : $scope.agregarEventoXDia,
        timezone         : 'local',
        weekends         : true,
        agenda           : "",
        slotDuration     : '00:15:00',
        minTime          : centroFactory.horarioCentro.apertura+':00:00',
        maxTime          : centroFactory.horarioCentro.cierre+':00:00',

        viewRender       : $scope.evaluaVista,
        events           : $scope.getActividadesOfertaEscenario,
        allDaySlot       : false,
        allDayDefault    : false,
        hiddenDays       : [ 0 ],
        defaultDate      : ofertaEducativaFactory.ofertaEducativa.fecha_inicio,
        eventAfterAllRender :  $scope.eventAfterAllRender // Evento al finalizar la carga de todas las actividades
      }
    };
}


/*
 * Obtiene las actividades oferta por escenario dado
 * @returns events {Obtiene los eventos}
 */
$scope.getActividadesOfertaEscenario = function(fechaInicio, fechaFin, timezone, callback){
    var fechaInicio     = new Date(fechaInicio);
    var fechaFin        = new Date(fechaFin);
    fechaInicio         = $scope.getSoloFecha(fechaInicio);
    fechaFin            = $scope.getSoloFecha(fechaFin);
    if(filtrosFactory.filtroInicial!=""){
        var data = {
                    "oferta_educativa_id"   : ofertaEducativaFactory.ofertaEducativa.oferta_educativa_centro_id,
                    "fecha_inicio"          : fechaInicio,
                    "tipo"                  : filtrosFactory.filtroInicial,
                    "fecha_final"           : fechaFin,
                    hostCentro              : limpiarUri(centroFactory.centroActual.ip)
                    };
          // Cargando de filtros factory el valor del aula_id
        var aula_id_filtro          = (filtrosFactory.aula                                     != 0
                                       && filtrosFactory.aula                                  != null
                                       && filtrosFactory.aula.hasOwnProperty("aula_id") != false
                                       && filtrosFactory.aula                                  != "todos")? filtrosFactory.aula.aula_id : 0;// Si no existe valor cargar por default el 0
                               var algo = typeof(filtrosFactory.aula);
        var facilitador_id_filtro   = (filtrosFactory.facilitador                                       != 0
                                       && filtrosFactory.facilitador                                    != null
                                        && filtrosFactory.facilitador.hasOwnProperty("facilitador_id") != false
                                       && filtrosFactory.facilitador != 'todos') ? filtrosFactory.facilitador.facilitador_id : 0;// Si no existe valor cargar por default el 0
         data.aula_id           = aula_id_filtro;
         data.facilitador_id    = facilitador_id_filtro;


        ofertaCalendarioCentroService.restResource.getActividadesOfertaCalendario(
                data,
                function(data){
                    var  allEvents     = data.concat(ofertaEducativaFactory.diasFeriados);  //concatenamos los dias feriados.
                    allEvents          = allEvents.concat(ofertaEducativaFactory.eventosMako);  //concatenamos los dias feriados.
                    centroFactory.allEvents = allEvents;
                    centroFactory.renderCalendar = callback;
                    callback(allEvents);

                    var dataEstadistica = {
                        "oferta_educativa_id"   : ofertaEducativaFactory.ofertaEducativa.oferta_educativa_centro_id,
                        "aula_id"               : aula_id_filtro,
                        "facilitador_id"        : facilitador_id_filtro,
                        hostCentro              : limpiarUri(centroFactory.centroActual.ip)
                    };
                    ofertaCalendarioCentroService.restResource.getEstadistica(
                        dataEstadistica,
                        function(data){
                            estadisticaFactory.actividades_obligatorias_faltantes   = data.actividades_obligatorias_faltantes;
                            estadisticaFactory.total_actividades_calendarizadas     = data.total_actividades_calendarizadas;
                            estadisticaFactory.total_actividades_oferta_educativa     = data.total_actividades_oferta_educativa;
                            estadisticaFactory.total_actividades_academicas_calendarizadas = data.total_actividades_academicas_calendarizadas;
                            estadisticaFactory.total_alertas_cancelacion_desactivadas = data.total_alertas_cancelacion_desactivadas;
                            estadisticaFactory.total_alertas_eliminacion_desactivadas = data.total_alertas_eliminacion_desactivadas;
                            $rootScope.$broadcast( 'eventosDeCalendario');
                            if(loaderFactory.loader != null){
                                  loaderFactory.loader.remove();
                                  loaderFactory.loader = null;
                                }

                                estadisticaFactory.cargandoVariables();

                            },
                            function(){
                                toastr.error(ocurrioError+": traer estadística");
                                m.remove();
                            }
                    );
                },
                function(){
                    toastr.error(ocurrioError+": traer sesiones.");
                    m.remove();
                }
        );
    }else{
        var  allEvents          = ofertaEducativaFactory.diasFeriados;
        callback(allEvents);
    }

}

/**
 * Evaluamos la vista seleccionada, aqui validaremos las flechas de sig y atras para ver si se muestran dependiendo
 * de   las fechas de inicio y fin de la oferta.
 * @param  object view Objeto de tipo fecha.
 * @return void
 */
$scope.evaluaVista =  function(view){
    ofertaEducativaFactory.evaluaVista(view);
}

/**
 * Devuelve el mensaje para el tooltip de cada actividad en el calendario. Actualmente éste se compone del nombre
 * del aula, el nombre del facilitador y la clave del grupo
 *
 * @argument Event event Actividad en el Calendario
 * @return String Mensaje para el Tooltip
 *
 * @return String Mensaje para el Tooltip
 *
 *
 * @author Bet Gader Porcayo Juárez <bet.porcayo@enova.mx>
 * @since Jun 30 2015
 * @version 1.0
 *
 * @link http://fullcalendar.io/docs/
 * @link http://fullcalendar.io/docs/display/contentHeight/
 **/
$scope._getTooltipMessage = function(event){
    var tooltip_mensaje = "Aula: " + event.aula_nombre_actividad + "\n";
    if (event.hasOwnProperty("facilitador_nombre_evento") || event.hasOwnProperty("facilitador_apellido_paterno_evento") || event.hasOwnProperty("facilitador_apellido_paterno_evento")) {
        tooltip_mensaje += "Facilitador: \n";
        tooltip_mensaje += (event.hasOwnProperty("facilitador_nombre_evento"))           ? event.facilitador_nombre_evento + "\n"           : "";
        tooltip_mensaje += (event.hasOwnProperty("facilitador_apellido_paterno_evento")) ? event.facilitador_apellido_paterno_evento + " "  : "";
        tooltip_mensaje += (event.hasOwnProperty("facilitador_apellido_materno_evento")) ? event.facilitador_apellido_materno_evento        : "";
        tooltip_mensaje += "\n";
    }
    tooltip_mensaje += (event.hasOwnProperty("clave_grupo")) ? "Grupo: \n" + event.clave_grupo : "";
    tooltip_mensaje += "\n";
    tooltip_mensaje += ( event.hasOwnProperty("numero_movimientos") && event.hasOwnProperty("numero_movimientos_maximo") ) ? "Movimientos realizados: " + event.numero_movimientos + ' de ' + event.numero_movimientos_maximo : "";
    return tooltip_mensaje;
};

/**
 * Devuelve la posición para el tooltip de cada actividad en el calendario.
 * Esta se calcula dependiendo del tipo de vista (Mensual, Semanal, Diaria) y de la posición en la vista.
 *
 * @argument Event event Actividad en el Calendario
 * @argument Calendar view Vista del Calendario
 *
 * @return String Mensaje para el Tooltip
 *
 *
 * @author Bet Gader Porcayo Juárez <bet.porcayo@enova.mx>
 * @since Jun 30 2015
 * @version 1.0
 *
 * @link http://getbootstrap.com/javascript/#tooltips
 * @link http://www.tutorialrepublic.com/twitter-bootstrap-tutorial/bootstrap-tooltips.php
 **/
$scope._getTooltipPosition = function(event, view){
    // Define global variables
    /**
     * Holgura para el tooltip en vista superior. Por defecto 1 hora
     * @constant Number
     */
    var HOLGURA_ESPACIO_TOOLTIP_HORAS = 1;
    /**
     * Holgura para el tooltip en vista superior. Por defecto 2 semana
     * @constant Number
     */
    var HOLGURA_ESPACIO_TOOLTIP_SEMANAS = 2;
    /**
     * Numero de referencia para el día para Lunes. Por defecto 1
     * @constant Number
     */
    var LUNES_DIA_CALENDARIO = 1;
    /**
     * Numero de referencia para el día para Sabado. Por defecto 6
     * @constant Number
     */
    var SABADO_DIA_CALENDARIO = 6;
    /**
     * Referencia a la Posición para el tooltip Superior. Por defecto 'top'
     * @constant String
     */
    var TOP = "top";
    /**
     * Referencia a la Posición para el tooltip Inferior. Por defecto 'bottom'
     * @constant String
     */
    var BOTTOM = "bottom";
    /**
     * Referencia a la Posición para el tooltip Izquierdo. Por defecto 'left'
     * @constant String
     */
    var LEFT = "left";
    /**
     * Referencia a la Posición para el tooltip Derecho. Por defecto 'right'
     * @constant String
     */
    var RIGHT = "right";

    // Variable interna para almacenar la posición para el Tooltip
    var tooltip_posicion = TOP;
    switch (view.name) {
        case VIEW_MONTH: // Cuando la vista que se muestra es mensual
            // Posiciones
            // Abajo (Bottom):
            //      Los eventos que se encuentren la primer o segunda semana, es decir, en el primer o hasta
            //      segundo renglón del calendario, mendiante 2 casos,
            //          1: Que es del mes anterior al que se está mostrando
            //          2: Que es del mes corriente y que es de la semana 0 o 1 del mes
            var formato_fecha_view = view.calendar.getDate().format().split('-');
            var calendario_fecha_visible = new Date(formato_fecha_view[0], parseInt(formato_fecha_view[1])-1, formato_fecha_view[2]); // Obtenemos la fecha visible

            if (parseInt(event._start._d.getMonth()) < (parseInt(calendario_fecha_visible.getMonth()))) {
                // Caso 1
                tooltip_posicion = BOTTOM;
            } else if ((parseInt(event._start._d.getMonth()) === parseInt(calendario_fecha_visible.getMonth())) && ($scope.getWeekOfMonth(new Date(event._start._d)) < HOLGURA_ESPACIO_TOOLTIP_SEMANAS)) {
                // Caso 2
                tooltip_posicion = BOTTOM;
            }

            if (parseInt(event._start._d.getDay()) === LUNES_DIA_CALENDARIO) {
                // Derecha
                //      La actividad se encuentra en los días lunes
                tooltip_posicion = RIGHT;
            } else if (parseInt(event._start._d.getDay()) === SABADO_DIA_CALENDARIO) {
                // Izquierda
                //      La actividad se encuentra en los días sábados
                tooltip_posicion = LEFT;
            }
            break;
        case VIEW_WEEK:  // Cuando la vista que se muestra es Semanal
            // Se muestra abajo si la actividad se encuentra despues de la hora de apertura mas 1 hora
            // Se muestra abajo en cualquier otro caso
            tooltip_posicion = (event.start._d.getHours() <= (centroFactory.horarioCentro.apertura + HOLGURA_ESPACIO_TOOLTIP_HORAS)) ? BOTTOM : TOP;

            if (parseInt(event._start._d.getDay()) === LUNES_DIA_CALENDARIO) {
                // Derecha
                //      La actividad se encuentra en los días lunes
                tooltip_posicion = RIGHT;
            } else if (parseInt(event._start._d.getDay()) === SABADO_DIA_CALENDARIO) {
                // Izquierda
                //      La actividad se encuentra en los días sábados
                tooltip_posicion = LEFT;
            }
            break;
        case VIEW_DAY:   // Cuando la vista que se muestra es Semanal
            // Se muestra abajo si la actividad se encuentra despues de la hora de apertura mas 1 hora
            // Se muestra abajo en cualquier otro caso
            tooltip_posicion = (event.start._d.getHours() <= (centroFactory.horarioCentro.apertura + HOLGURA_ESPACIO_TOOLTIP_HORAS)) ? BOTTOM : TOP;
            break;
    }
    return tooltip_posicion;
};

/**
 * Calcular alto del calendario
 *
 * Calcula el alto del calendario, debido al tooltip que se muestra cortado
 *
 * @argument Calendar view Vista del Calendario
 *
 * @author Bet Gader Porcayo Juárez <bet.porcayo@enova.mx>
 * @since Jun 30 2015
 * @version 1.0
 *
 * @link http://fullcalendar.io/docs/
 * @link http://fullcalendar.io/docs/display/contentHeight/
 **/
$scope._heightCalendar = function (view) {
    var ALTO_CALENDARIO_DEFAULT = 600;
    var calendario_oe = $('#ofertaEducativaCalendar');
    var calendario_bloque_visible = calendario_oe.find('div.fc-view-container').find('div.fc-view').find('table').find('tbody').find('tr').find('td.fc-widget-content').find('div');
    var calendario_alto = 0;
    var numero_maximo_actividades_por_renglones = 0;
    switch (view.name) {
        case VIEW_MONTH: // Cuando la vista que se muestra es mensual

            // Obtenemos el numero de semanas por mes
            var renglones_por_mes = calendario_bloque_visible.find('div.fc-day-grid').find('div.fc-week');

            // calculamos el numero máximo de actividades por día, esto mediante el numero de rows por semana,
            renglones_por_mes.each(function () {
                var numero_maximo_actividades_por_renglon = $(this).find('div.fc-content-skeleton').find('table').find('tbody').find('tr').length;
                numero_maximo_actividades_por_renglones = (numero_maximo_actividades_por_renglon > numero_maximo_actividades_por_renglones) ? numero_maximo_actividades_por_renglon : numero_maximo_actividades_por_renglones;
            });

            // Si hay menos de 3 actividades colocamos un alto fijo
            if (numero_maximo_actividades_por_renglones < 3) {
                view.setHeight(ALTO_CALENDARIO_DEFAULT);
            } else {
                // Si hay 3 o más actividades por renglon se hace el calculo del alto
                //
                // Alto del calendario estará definido por las siguientes reglas:
                // Alto del Header
                // Mas alto de los numeros de los días
                //      (número de semanas) * 26
                // Más 10 (por el primer renglón para la visivilidad del tooltip
                // Mas alto del espacio ocupado por las actividades
                //      (número de semanas) * (número de renglones totales) * 28
                calendario_alto = 30 + (renglones_por_mes.length * 26) + 10 + (renglones_por_mes.length * numero_maximo_actividades_por_renglones * 28);
                // El resultado del alto del calendario es redondeado a multiplos de 100
                calendario_alto = Math.ceil(calendario_alto / 100) * 100;

                // Se aplica el alto obtenido al calendario
                view.setHeight(calendario_alto);
            }
            break;
        case VIEW_WEEK:  // Cuando la vista que se muestra es Semanal
        case VIEW_DAY:   // Cuando la vista que se muestra es Diaria
            // Variables de apoyo
            var ALTO_DIVISION = 21;
            var ALTO_HEADER = 70;
            // Obtenemos las divisiones totales que hay
            var divisiones_totales = calendario_oe.find('div.fc-time-grid').find('div.fc-slats').find('table').find('tbody').find('tr').length;

            // El alto estará definido por el numero total de divisiones, por el alto de la division,
            // y el resultado es redondeado a multiplos de 10
            calendario_alto = (Math.ceil((divisiones_totales * ALTO_DIVISION + ALTO_HEADER) / 10)) * 10;

            // Se aplica el alto obtenido al calendario
            view.setHeight(calendario_alto);
            break;
    }
};

/**
 * Cambia el Z-Index de un renglon (Semana) en la vista Mensual para la visibilidad del tooltip
 *
 * @author Bet Gader Porcayo Juárez <bet.porcayo@enova.mx>
 * @since Jun 30 2015
 * @version 1.0
 **/
$scope._changeZIndexWeekToMonth = function(element, type){
    var nodo_padre_renglon = $(element).parent().parent().parent().parent().parent().parent();
    if(String(type) === String("show")){
        nodo_padre_renglon.css('z-index',100);
    }else{
        nodo_padre_renglon.css('z-index',"");
    }
};
/**
 * Devuelve el numero de Semana en un Mes en especifico
 *
 * @argument Date date Fecha a la cual se obtendrá el número de Semana
 *
 * @return Number Número de Semana en la que se encuentra la fecha ingresada
 *
 * @author Bet Gader Porcayo Juárez <bet.porcayo@enova.mx>
 * @since Jun 30 2015
 * @version 1.0
 *
 * @link http://stackoverflow.com/questions/5974798/how-to-find-week-of-month-for-calendar-which-starts-from-monday
 **/
$scope.getWeekOfMonth = function(date) {
    var day = date.getDate();
    day -= (parseInt(date.getDay()) === 0 ? 6 : date.getDay() - 1); //get monday of this week

    //special case handling for 0 (sunday)
    day += 7;

    //for the first non full week the value was negative
    var prefixes = ['0', '1', '2', '3', '4', '5'];
    return prefixes[0 | (day) / 7];
};
}






/* EOF */
