class Computer:
    brand: str = None
    capacity: int = 0
    ram: int = 0

    def __init__(self, brand: str, capacity: int, ram: int):
        print(f"accedimos al constructor de la pc: {brand}")
        self.brand = brand
        self.capacity = capacity
        self.ram = ram

    def imprimirInfoPC(self) -> None:
        print(
            f"Soy una computadora marca: {self.brand} con almacenamiento de {self.capacity}GB y memoria de {self.ram}GB")

    def __del__(self):
        print(f"Eliminamos a la pc: {self.brand}")


class Person:
    firstName: str = None
    lastName: str = None
    age: int = 0
    address: str = None
    computer: Computer = None

    def __init__(self, firstName: str, lastName: str, age: int, address: str, brand: str, capacity: int, ram: int):
        print(f"----accedimos al constructor de la persona: {firstName}")
        self.firstName=firstName
        self.lastName=lastName
        self.age=age
        self.address=address
        self.computer = Computer(brand,capacity,ram)

    def imprimirInfo(self)->None:
        print(f"Mi nombre es: {self.firstName} {self.lastName}, mi edad es de {self.age} años y mi dirección es: {self.address}")
        self.computer.imprimirInfoPC()

    def __del__(self):
        print(f"eliminamos el objeto...{self.firstName}")


persona = Person('Roberto Leroy','Monroy Ruiz',30,'Dirección...','Dell',256,4)

print('\n')
persona.imprimirInfo()
print('\n')

persona2 = Person('Roberto Leroy2','Monroy Ruiz2',31,'Dirección2...','HP',512,8)

print('\n')
persona2.imprimirInfo()
print('\n')


print('****seguimos aqui...')

