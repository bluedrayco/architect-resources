class Computer {
    public String brand = null;
    public int capacity = 0;
    public int ram = 0;

    public Computer(String brand, int capacity, int ram) {
        System.out.println("accedimos al constructor de la pc:" + brand + "\n");
        this.brand = brand;
        this.capacity = capacity;
        this.ram = ram;
    }

    public void imprimirInfoPC() {
        System.out.println("Soy una computadora marca: " + this.brand + " con almacenamiento de " + this.capacity
                + "GB y memoria de " + this.ram + "GB\n");
    }
}

class Person {
    public String firstName = null;
    public String lastName = null;
    public int age = 0;
    public String address = null;
    public Computer computer = null;

    public Person(String firstName, String lastName, int age, String address, String brand, int capacity, int ram) {
        System.out.println("----accedimos al constructor de la persona: " + firstName + "\n");
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
        this.address = address;
        this.computer = new Computer(brand, capacity, ram);
    }

    public void imprimirInfo() {
        System.out.println("----Mi nombre es: " + this.firstName + " " + this.lastName + ", mi edad es de " + this.age
                + " anios y mi direccion es: " + this.address + "\n");
        this.computer.imprimirInfoPC();
    }
}

class Main {

    public static void main(String[] args) {
        Person persona = new Person("Roberto Leroy", "Monroy Ruiz", 30, "Direccion...", "Dell", 256, 4);

        System.out.println("\n");
        persona.imprimirInfo();
        System.out.println("\n");

        Person persona2 = new Person("Roberto Leroy", "Monroy Ruiz", 30, "Direccion...", "HP", 512, 8);

        System.out.println("\n");
        persona2.imprimirInfo();
        System.out.println("\n");

        System.out.println("****seguimos aqui....");
    }
}