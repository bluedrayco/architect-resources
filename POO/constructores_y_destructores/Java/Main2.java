import java.io.IOException;

class Computer {
    public String brand = null;
    public int capacity = 0;
    public int ram = 0;

    public Computer(String brand, int capacity, int ram) {
        // System.out.println("accedimos al constructor de la pc:" + brand + "\n");
        this.brand = brand;
        this.capacity = capacity;
        this.ram = ram;
    }

    public void imprimirInfoPC() {
        System.out.println("Soy una computadora marca: " + this.brand + " con almacenamiento de " + this.capacity
                + "GB y memoria de " + this.ram + "GB\n");
    }

    protected void finalize() throws Throwable {
        System.out.println("eliminamos a la pc: " + this.brand + "\n");
        super.finalize();
    }
}

class Person {
    public String firstName = null;
    public String lastName = null;
    public int age = 0;
    public String address = null;
    public Computer computer = null;

    public Person(String firstName, String lastName, int age, String address, String brand, int capacity, int ram) {
        // System.out.println("----accedimos al constructor de la persona: " + firstName
        // + "\n");
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
        this.address = address;
        this.computer = new Computer(brand, capacity, ram);
    }

    public void imprimirInfo() {
        System.out.println("----Mi nombre es: " + this.firstName + " " + this.lastName + ", mi edad es de " + this.age
                + " anios y mi direccion es: " + this.address + "\n");
        this.computer.imprimirInfoPC();
    }

    protected void finalize() throws Throwable {
        System.out.println("----eliminamos el objeto..." + this.firstName + "\n");
        super.finalize();
    }
}

class Main2 {

    public static void main(String[] args) {
        try {
            try {
                for (int x = 0; x < 100000; x++) {

                    Person persona3 = new Person("Roberto Leroy"+x, "Monroy Ruiz", 30, "Direccion...", "HP", 512, 8);
                    Runtime.getRuntime().exec("clear");
                    System.out.println(Runtime.getRuntime().freeMemory());
                    Thread.sleep(1);

                }
            } catch (InterruptedException e) {
                System.out.println(e.getMessage());
            } catch (IOException e) {
                System.out.println(e.getMessage());
            }

            System.out.println("****seguimos aqui....");

        } catch (RuntimeException e) {
            System.out.println(e.getMessage());
        }
    }
}

