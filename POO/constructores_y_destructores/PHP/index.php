<?php

class Computer
{
    public $brand=null;
    public $capacity = 0;
    public $ram=0;

    public function __construct(string $brand, int $capacity, int $ram)
    {
        echo "accedimos al constructor de la pc: {$brand}\n";
        $this->brand=$brand;
        $this->capacity=$capacity;
        $this->ram=$ram;
    }

    public function imprimirInfoPC()
    {
        echo "Soy una computadora marca: {$this->brand} con almacenamiento de {$this->capacity}GB y memoria de {$this->ram}GB\n";
    }
    public function __destruct()
    {
        echo "eliminamos a la pc: {$this->brand}\n";
    }
}
class Person
{
    public $firstName = null;
    public $lastName=null;
    public $age=0;
    public $address=null;
    public $computer=null;

    public function __construct(string $firstName, string $lastName, int $age, string $address, string $brand, int $capacity, int $ram)
    {
        echo "----accedimos al constructor de la persona: {$firstName}\n";
        $this->firstName=$firstName;
        $this->lastName=$lastName;
        $this->age=$age;
        $this->address=$address;
        $this->computer = new Computer($brand, $capacity, $ram);
    }

    public function imprimirInfo()
    {
        echo "----Mi nombre es: {$this->firstName} {$this->lastName}, mi edad es de {$this->age} años y mi dirección es: {$this->address}\n";
        $this->computer->imprimirInfoPC();
    }

    public function __destruct()
    {
        echo "----eliminamos el objeto...{$this->firstName}\n";
    }
}

$persona = new Person('Roberto Leroy', 'Monroy Ruiz', 30, 'Dirección...', 'Dell', 256, 4);

echo "\n\n";
$persona->imprimirInfo();
echo "\n\n";

$persona2 = new Person('Roberto Leroy2', 'Monroy Ruiz2', 31, 'Dirección2...', 'HP', 512, 8);

echo "\n\n";
$persona2->imprimirInfo();
echo "\n\n";

echo "****seguimos aqui....";
