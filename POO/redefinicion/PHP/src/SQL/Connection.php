<?php

namespace Project\SQL;

class Connection
{
    private $host;
    private $username;
    private $password;

    public function __construct(string $host, string $username, string $password)
    {
        $this->host = $host;
        $this->username=$username;
        $this->password=$password;
    }

    public function connect():void
    {
        echo "conectando a: {$this->host} con user: {$this->username} y password: {$this->password}\n";
    }

    public function close():void
    {
        echo "conexion terminada...\n";
    }
}
