<?php

namespace Project\SQL;

class MySQLQueryBuilder
{
    protected $connection;
    protected $select;
    protected $where;
    protected $limit;

    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
        $this->select='';
        $this->where='';
        $this->limit='';
    }

    public function select(string $table, array $fields): MySQLQueryBuilder
    {
        $this->select = "SELECT " . implode(", ", $fields) . " FROM " . $table;
        return $this;
    }

    public function where(string $field, string $value, string $operator = '='): MySQLQueryBuilder
    {
        if ($this->select == '') {
            throw new \Exception("WHERE can only be added after SELECT");
        }
        $operator = strtoupper($operator);
        $value = $operator == 'LIKE' || $operator == 'ILIKE'? "'{$value}'" : $value;
        $this->where = " WHERE {$field} {$operator} {$value}";
        return $this;
    }

    public function limit(int $start, int $offset): MySQLQueryBuilder
    {
        if ($this->select == '') {
            throw new \Exception("LIMIT can only be added after SELECT");
        }
        $this->limit = " LIMIT " . $start . ", " . $offset;

        return $this;
    }


    protected function getSQL(): string
    {
        $sql = $this->select;
        if ($this->where != '') {
            $sql .= $this->where;
        }
        if ($this->limit != '') {
            $sql .= $this->limit;
        }
        $sql .= ";";
        return $sql;
    }

    public function execute():void
    {
        $this->connection->connect();
        echo "---->".$this->getSQL()."\n";
        $this->connection->close();
    }
}
