<?php

namespace Project\SQL;

use Project\SQL\MySQLQueryBuilder;

class PostgreSQLQueryBuilder extends MySQLQueryBuilder
{
    protected function getSQL(): string
    {
        echo "Enviamos un mensaje extra...\n";
        return parent::getSQL();
    }
    
    
    #    En version php 7.3.23 No es posible redefinir el tipo de retorno, puede colocarse la clase o parent
    #    En version php 7.4.14 Si es posible redefinir el tipo de retorno.
    
    public function limit(int $start, int $offset): MySQLQueryBuilder
    {
        if ($this->select == '') {
            throw new \Exception("LIMIT can only be added after SELECT");
        }
        $this->limit = " LIMIT " . $start . " OFFSET " . $offset;

        return $this;
    }
}
