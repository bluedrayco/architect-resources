package com.tecno.project.sql;

public class Connection {
    private String host;
    private String username;
    private String password;

    public Connection(String host, String username, String password) {
        this.host = host;
        this.username = username;
        this.password = password;
    }

    public void connect() {
        System.out.println(
                "conectando a: " + this.host + " con user: " + this.username + " y password: " + this.password);
    }

    public void close() {
        System.out.println("conexion terminada...");
    }
}
