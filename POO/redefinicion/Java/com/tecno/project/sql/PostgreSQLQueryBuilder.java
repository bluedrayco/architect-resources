package com.tecno.project.sql;

import com.tecno.project.sql.MySQLQueryBuilder;

public class PostgreSQLQueryBuilder extends MySQLQueryBuilder {
    public PostgreSQLQueryBuilder(Connection connection) {
        super(connection);
    }

    protected String getSQL() {
        System.out.println("Enviamos un mensaje extra...");
        return super.getSQL();
    }

    public PostgreSQLQueryBuilder limit(int start, int offset) throws Exception {
        if (this.select == "") {
            throw new Exception("LIMIT can only be added after SELECT");
        }
        this.limit = " LIMIT " + start + " OFFSET " + offset;

        return this;
    }
}
