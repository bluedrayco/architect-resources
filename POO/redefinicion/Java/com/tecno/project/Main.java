package com.tecno.project;

import com.tecno.project.sql.Connection;
import com.tecno.project.sql.MySQLQueryBuilder;
import com.tecno.project.sql.PostgreSQLQueryBuilder;

public class Main {
    public static void main(String[] args) {
        try {
            Connection connection = new Connection("localhost", "user", "password");
            MySQLQueryBuilder query = new MySQLQueryBuilder(connection);
            String[] selectFields = { "name", "email", "password" };
            query.select("users", selectFields).where("age", "30", "<").limit(10, 20).execute();

            System.out.println();

            PostgreSQLQueryBuilder query2 = new PostgreSQLQueryBuilder(connection);
            query2.select("users", selectFields).where("age", "30", "<").limit(10, 20).execute();
        } catch (Exception ex) {
            System.out.println("Error: " + ex.getMessage());
        }
    }
}