from app.sql.connection import Connection
from app.sql.mysql import MysqlQueryBuilder
from app.sql.postgresql import PostgreSQLQueryBuilder

try:
    connection = Connection('localhost','user','password')
    query = MysqlQueryBuilder(connection)
    query.select("users", ["name", "email", "password"]).where("age", 30, "<").limit(10, 20).execute()

    print()
    
    query = PostgreSQLQueryBuilder(connection)
    query.select("users", ["name", "email", "password"]).where("age", 30, "<").limit(10, 20).execute()
except Exception as err:
    print(f"Error: {str(err)}")