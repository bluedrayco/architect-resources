
class Connection:

    __host:str=None
    __username:str=None
    __password:str=None

    def __init__(self,host:str, username:str, password:str):
        self.__host = host
        self.__username=username
        self.__password=password

    def connect(self)->None:
        print(f"conectando a: {self.__host} con user: {self.__username} y password: {self.__password}")

    def close(self)->None:
        print("conexion terminada...")
