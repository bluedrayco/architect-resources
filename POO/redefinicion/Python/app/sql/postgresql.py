from app.sql.mysql import MysqlQueryBuilder

class PostgreSQLQueryBuilder(MysqlQueryBuilder):

    def _getSQL(self):
        print(f"Enviamos un mensaje extra...")
        return MysqlQueryBuilder._getSQL(self)

    def limit(self,start:int, offset:int)->'PostgreSQLQueryBuilder':
        if self._select == '':
            raise Exception("LIMIT can only be added after SELECT")

        self._limit = " LIMIT " + str(start) + " OFFSET " + str(offset)

        return self