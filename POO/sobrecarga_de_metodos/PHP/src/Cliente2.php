<?php
namespace Project;

class Cliente2
{
    public $nombre;
    public $apellidoPaterno;
    public $apellidoMaterno;
    public $edad;
    public $saldo;

    public function __construct(string $nombre=null,string $apellidoPaterno=null,string $apellidoMaterno=null,int $edad=null, float $saldo=null)
    {
        if(is_null($nombre) && is_null($apellidoPaterno) && is_null($apellidoMaterno) && is_null($edad) && is_null($saldo)){
            $this->nombre = "N/A";
            $this->apellidoPaterno = "N/A";
            $this->apellidoMaterno = "N/A";
            $this->edad = 18;
            $this->saldo = 0.0;
        } elseif(!is_null($nombre) && !is_null($apellidoPaterno) && !is_null($apellidoMaterno) && is_null($edad) && is_null($saldo)){
            $this->nombre = $nombre;
            $this->apellidoPaterno = $apellidoPaterno;
            $this->apellidoMaterno = $apellidoMaterno;
            $this->edad = 18;
            $this->saldo = 0.0;
        }elseif(!is_null($nombre) && !is_null($apellidoPaterno) && !is_null($apellidoMaterno) && !is_null($edad) && !is_null($saldo)){
            $this->nombre = $nombre;
            $this->apellidoPaterno = $apellidoPaterno;
            $this->apellidoMaterno = $apellidoMaterno;
            $this->edad = $edad;
            $this->saldo = $saldo;
        }elseif(!is_null($nombre) && !is_null($apellidoPaterno) && is_null($apellidoMaterno) && is_null($edad) && !is_null($saldo)){
            $this->nombre = $nombre;
            $this->apellidoPaterno = $apellidoPaterno;
            $this->apellidoMaterno = "N/A";
            $this->edad = 18;
            $this->saldo = $saldo;
        }elseif(!is_null($nombre) && !is_null($apellidoPaterno) && is_null($apellidoMaterno) && !is_null($edad) && is_null($saldo)){
            $this->nombre = $nombre;
            $this->apellidoPaterno = $apellidoPaterno;
            $this->apellidoMaterno = "N/A";
            $this->edad = $edad;
            $this->saldo = 0;
        }else{
            $this->nombre = "N/A";
            $this->apellidoPaterno = "N/A";
            $this->apellidoMaterno = "N/A";
            $this->edad = 0;
            $this->saldo = $saldo;
        }
    }

    public function __toString()
    {
        return "(".$this->nombre.") (".$this->apellidoPaterno.") (".$this->apellidoMaterno . ") (". $this->edad . ") (". $this->saldo.")\n";
    }
}
