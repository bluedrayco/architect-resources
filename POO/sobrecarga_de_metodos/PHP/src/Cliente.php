<?php
namespace Project;

class Cliente
{
    public $nombre;
    public $apellidoPaterno;
    public $apellidoMaterno;
    public $edad;
    public $saldo;

    public function __construct($nombre="N/A",$apellidoPaterno="N/A",$apellidoMaterno="N/A",$edad=18,$saldo=0.0)
    {
        $this->nombre = $nombre;
        $this->apellidoPaterno = $apellidoPaterno;
        $this->apellidoMaterno = $apellidoMaterno;
        $this->edad = $edad;
        $this->saldo = $saldo;
    }

    public function __toString()
    {
        return "(".$this->nombre.") (".$this->apellidoPaterno.") (".$this->apellidoMaterno . ") (". $this->edad . ") (". $this->saldo.")\n";
    }
}
