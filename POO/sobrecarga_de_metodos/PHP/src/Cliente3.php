<?php
namespace Project;

class Cliente3{
    public $nombre;
    public $apellidoPaterno;
    public $apellidoMaterno;
    public $edad;
    public $saldo;

    public static function __callStatic($name, $arguments)
    {
        echo "accedi al metodo estatico: {$name}, y mis argumentos son: ".implode(',',$arguments)."\n";
    }

    public function __call($name, $arguments){
        echo "accedi al metodo: {$name}, y mis argumentos son: ".implode(',',$arguments)."\n";
    }

    public function __toString()
    {
        return "(".$this->nombre.") (".$this->apellidoPaterno.") (".$this->apellidoMaterno . ") (". $this->edad . ") (". $this->saldo.")\n";
    }
}