<?php

require "vendor/autoload.php";

use Project\Cliente;


$cliente1 = new Cliente();
$cliente2 = new Cliente("Jose","Aldama","Alvarez");
$cliente3 = new Cliente("Jose","Aldama","Alvarez",20,450.00);
$cliente4 = new Cliente("Jose","Aldama",null,300.00);
$cliente5 = new Cliente("Jose","Aldama",null,18);
$cliente6 = new Cliente(null,null,null,null,500);

echo $cliente1;
echo $cliente2;
echo $cliente3;
echo $cliente4;
echo $cliente5;
echo $cliente6;