from app.project.cliente2 import Cliente2

#keyword arguments / named arguments
cliente1 = Cliente2()
cliente2 = Cliente2(nombre="Aldama",apellidoPaterno="Alvarez")
cliente3 = Cliente2(nombre="Jose",apellidoPaterno="Aldama",apellidoMaterno="Alvarez",edad=20,saldo=450.00)
cliente4 = Cliente2(nombre="Jose",apellidoPaterno="Aldama",saldo=300.00)
cliente5 = Cliente2(nombre="Jose",apellidoPaterno="Aldama",edad=18)
cliente6 = Cliente2(saldo=500)

print(cliente1)
print(cliente2)
print(cliente3)
print(cliente4)
print(cliente5)
print(cliente6)