# REQUIREMENTS

- virtualbox latest version
- vagrant latest version

# INSTALATION

For create the virtual machine type:

```ssh
vagrant up
```

When the virtual machine is created for enter in the virtual machine and access to code type:

```ssh
vagrant ssh
cd code
```

And finally for execute the different codes type:

```ssh
python3 app.py
or
python3 app2.py
or
python3 app3.py
or
python3 app4.py
```

For exit to the virtual machine type **exit** command in the terminal

