
class Cliente2:
    nombre=None
    apellidoPaterno=None
    apellidoMaterno=None
    edad=0
    saldo=0.0

    def __init__(self,nombre:str='N/A',apellidoPaterno:str='N/A',apellidoMaterno:str='N/A',edad:int=0,saldo:float=0.0):
        self.nombre=nombre
        self.apellidoPaterno=apellidoPaterno
        self.apellidoMaterno=apellidoMaterno
        self.edad=edad
        self.saldo=saldo
    
    def __str__(self):
        return "({0}) ({1}) ({2}) ({3}) ({4})".format(self.nombre,self.apellidoPaterno,self.apellidoMaterno,self.edad,self.saldo)