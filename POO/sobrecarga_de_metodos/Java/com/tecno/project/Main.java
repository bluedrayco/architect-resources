package com.tecno.project;

import com.tecno.project.Cliente;

public class Main{
    public static void main(String[] args) {
        Cliente cliente1 = new Cliente();
        Cliente cliente2 = new Cliente("Jose","Aldama","Alvarez");
        Cliente cliente3 = new Cliente("Jose","Aldama","Alvarez",20,450.00);
        Cliente cliente4 = new Cliente("Jose","Aldama",300.00);
        Cliente cliente5 = new Cliente("Jose","Aldama",18);
        Cliente cliente6 = new Cliente(500);

        System.out.println(cliente1);
        System.out.println(cliente2);
        System.out.println(cliente3);
        System.out.println(cliente4);
        System.out.println(cliente5);
        System.out.println(cliente5);
    }
}