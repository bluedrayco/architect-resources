package com.tecno.project;

public class Cliente {
    public String nombre;
    public String apellidoPaterno;
    public String apellidoMaterno;
    public int edad;
    public double saldo;

    public Cliente(double saldo){
        this.nombre = "N/A";
        this.apellidoPaterno = "N/A";
        this.apellidoMaterno = "N/A";
        this.edad = 0;
        this.saldo = saldo;
    }

    public Cliente(){
        this.nombre = "N/A";
        this.apellidoPaterno = "N/A";
        this.apellidoMaterno = "N/A";
        this.edad = 0;
        this.saldo = 0.0;
    }

    public Cliente(String nombre,String apellidoPaterno,String apellidoMaterno,int edad, double saldo){
        this.nombre = nombre;
        this.apellidoPaterno = apellidoPaterno;
        this.apellidoMaterno = apellidoMaterno;
        this.edad = edad;
        this.saldo = saldo;
    }

    public Cliente(String nombre,String apellidoPaterno,String apellidoMaterno){
        this.nombre = nombre;
        this.apellidoPaterno = apellidoPaterno;
        this.apellidoMaterno = apellidoMaterno;
        this.edad = 18;
        this.saldo = 0.0;
    }

    public Cliente(String nombre,String apellidoPaterno,int edad){
        this.nombre = nombre;
        this.apellidoPaterno = apellidoPaterno;
        this.apellidoMaterno = "";
        this.edad  = edad;
        this.saldo = 0;
    }

    public Cliente(String nombre,String apellidoPaterno,double saldo){
        this.nombre = nombre;
        this.apellidoPaterno = apellidoPaterno;
        this.apellidoMaterno = "";
        this.edad  = 18;
        this.saldo = saldo;
    }

    public String toString(){
        return "("+this.nombre+") ("+this.apellidoPaterno+") ("+this.apellidoMaterno + ") ("+ this.edad + ") ("+ this.saldo+")";
    }
}
