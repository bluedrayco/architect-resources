package com.tecno.project.email;

import com.tecno.project.email.Email;
import java.util.Base64;

public class Hotmail extends Email{

    public Hotmail(String host, String username,String password){
        super(host,username,password);
    }

    protected String parseContent(String subject,String body){
        return "{\n"
            +"    \"subject\":\""+subject+"\",\n"
            +"    \"body\":\""+body+"\"\n"
        +"}";
    }

    private String encodeConnection(String connection){
        return Base64.getEncoder().encodeToString(connection.getBytes());
    }

    protected void connect(){
        String connectionEncoded = this.encodeConnection(this.host+"/"+this.username+":"+this.password);
        System.out.println("connecting to hotmail -->"+connectionEncoded);
    }

    protected void close(){
        System.out.println("closing connection to hotmail");
    }

}