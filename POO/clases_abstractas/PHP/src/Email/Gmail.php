<?php
namespace Project\Email;

use Project\Email\Email;

class Gmail extends Email{

    public function __construct(string $host, string $username,string $password){
        parent::__construct($host,$username,$password);
    }

    protected function parseContent(string $subject,string $body):string{
        return "<html>\n"
            ."<head>\n"
                ."    <title>".$subject."</title>\n"
            ."</head>\n"
            ."<body>\n    "
                .$body."\n"
            ."</body>\n"
            ."</html>";
    }

    protected function connect():void{
        echo "connecting to google --> ".$this->host."/".$this->username.":".$this->password."\n";
    }

    protected function close():void{
        echo "closing connection to gmail\n";
    }

}