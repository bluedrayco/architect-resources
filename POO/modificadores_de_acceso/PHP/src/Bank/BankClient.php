<?php

namespace Project\Bank;
use Exception;

class BankClient {
    private $name = null;
    private $lastName = null;
    private $age = 0;
    private $accountBalance = 0.0;

    public function __construct(string $name, string $lastName, int $age, float $accountBalance=0.0) {
        $this->validateAge($age);
        $this->validateAmount($accountBalance);
        $this->name = $name;
        $this->lastName = $lastName;
        $this->age = $age;
        $this->accountBalance = $accountBalance;
    }

    public function getFullName():string {
        return $this->name ." " . $this->lastName;
    }

    private function sendEmail(string $title, string $body):void {
        echo "send email: " . $title . " with text:" . $body;
    }

    public function saveMoney(float $amount):void {
        $this->accountBalance = $this->accountBalance + $amount;
        $this->sendEmail("---saving money...", "it was saved " . $amount);
        $this->sendAccountBalanceToBank($amount);
    }

    public function extractMoney(float $amount):void{
        $finalAmount = $this->accountBalance - $amount;
        $this->validateAmount($finalAmount);
        $this->accountBalance = $finalAmount;
        $this->sendEmail("---extracting money...", "it was extracted " . $amount);
        $this->sendAccountBalanceToBank($amount);
    }

    private function sendAccountBalanceToBank(float $amount):void{
        echo "Sending amount: ".$amount." to the bank...\n";
    }

    private function validateAmount(float $accountBalance):void {
        if ($accountBalance < 0) {
            throw new Exception("The account balance cannot be less than 0");
        }
    }

    private function validateAge(int $age):void {
        if ($age < 18) {
            throw new Exception("The age is less than 18");
        }
    }

    public function printInfo():string {
        return "Name: " . $this->getFullName() . ", Age: " . $this->age . ", Balance: " . $this->accountBalance."\n";
    }
}