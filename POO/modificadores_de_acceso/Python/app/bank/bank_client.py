class BankClient:
    __name = None
    __lastName = None
    __age = 0
    __accountBalance = 0.0

    def __init__(self,name:str,lastName:str,age:int,accountBalance:float=0.0):
        self.__validateAge(age)
        self.__validateAmount(accountBalance)
        self.__name = name
        self.__lastName = lastName
        self.__age = age
        self.__accountBalance = accountBalance
    
    def getFullName(self)->str:
        return self.__name +" "+self.__lastName

    def __sendEmail(self,title:str,body:str)->None:
        print ("send email: " + title + " with text: " + body)

    def saveMoney(self,amount:float)->None:
        self.__accountBalance = self.__accountBalance + amount
        self.__sendEmail("---saving money...", "it was saved " +str(amount))
        self.__sendAccountBalanceToBank(amount)

    def extractMoney(self,amount:float)->None:
        finalAmount = self.__accountBalance - amount
        self.__validateAmount(finalAmount)
        self.__accountBalance = finalAmount
        self.__sendEmail("---extracting money...", "it was extracted " + str(amount))
        self.__sendAccountBalanceToBank(amount)

    def __sendAccountBalanceToBank(self,amount:float)->None:
        print("Sending amount: "+str(amount)+" to the bank...")
    

    def __validateAmount(self,accountBalance:float)->None: 
        if accountBalance < 0 :
            raise Exception("The account balance cannot be less than 0")
        

    def __validateAge(self,age:int)->None: 
        if age < 18 :
            raise Exception("The age is less than 18")

    def printInfo(self)->str: 
        return "Name: " +self.getFullName() + ", Age: " +str(self.__age)+ ", Balance: " + str(self.__accountBalance)
