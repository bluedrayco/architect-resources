<?php
namespace Project\People;

class Professor {
    private $name;
    private $age;
    protected $degree;

    public function __construct(string $name,int $age,Degree $degree){
        $this->name = $name;
        $this->age = $age;
        $this->degree = $degree;
    }

    public function getInfo():string{
        return "Name: {$this->name}, Age: {$this->age}, Degree: {$this->degree->getDegree()}";
    }

    protected function getName():string{
        return $this->name;
    }

    protected function getAge():int{
        return $this->age;
    }
}
