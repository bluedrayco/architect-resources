<?php
namespace Project\People;

class ResearcherProfessor extends Professor{
    private $topic;
    private $salary;

    public function __construct(string $name,int $age,Degree $degree, String $topic, float $salary){
        parent::__construct($name,$age,$degree);
        $this->topic = $topic;
        $this->salary = $salary;
    }

    public function setDegree(Degree $degree):void{
        $this->degree = $degree;
    }

    public function setSalary(float $salary):void{
        $this->salary = $salary;
    }

    public function getAllInfo():string{
        return $this->getInfo(). ", Topic: {$this->topic}, Salary: {$this->salary}";
    }
}
