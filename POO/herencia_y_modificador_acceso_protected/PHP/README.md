# REQUIREMENTS

- virtualbox latest version
- vagrant latest version

# INSTALATION

For create the virtual machine type:

```ssh
vagrant up
```

When the virtual machine is created for enter in the virtual machine and access to code type:

```ssh
vagrant ssh
cd code
```

And finally for execute the different files type:

```ssh
php index.php
or
php index2.php
or
php index3.php
or
php index4.php
```

For exit to the virtual machine type **exit** command in the terminal

