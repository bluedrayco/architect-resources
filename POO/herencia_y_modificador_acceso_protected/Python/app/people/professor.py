from app.people.degree import Degree

class Professor:
    __name: str
    __age: str
    _degree: Degree

    def __init__(self,name:str,age:int,degree:Degree):
        self.__name = name
        self.__age = age
        self._degree = degree

    def getInfo(self)->str:
        return f"Name: {self.__name}, Age: {self.__age}, Degree: {self._degree.getDegree()}"

    def _getName(self)->str:
        return self.__name

    def _getAge(self)->int:
        return self.__age
