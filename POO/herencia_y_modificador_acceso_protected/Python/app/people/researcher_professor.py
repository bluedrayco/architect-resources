from app.people.professor import Professor
from app.people.degree import Degree

class ResearcherProfessor(Professor):
    __topic:str
    __salary:float

    def __init__(self,name:str,age:str,degree:Degree,topic:str,salary:float):
        Professor.__init__(self,name,age,degree)
        self.__topic = topic
        self.__salary = salary
    

    def setDegree(self,degree:Degree)->None:
        self.__degree = degree
    

    def setSalary(self,salary:float)->None:
        self.__salary = salary

    def getAllInfo(self)->str:
        return f"{self.getInfo()} , Topic: {self.__topic}, Salary: {self.__salary}"
    