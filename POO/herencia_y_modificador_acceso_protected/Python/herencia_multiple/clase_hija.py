from herencia_multiple.clase1 import Clase1
from herencia_multiple.clase2 import Clase2
from herencia_multiple.clase3 import Clase3

class ClaseHija(Clase1,Clase2,Clase3):
    valor2:str = ""

    def __init__(self):
        Clase1.__init__(self)
        Clase2.__init__(self)
        Clase3.__init__(self)
        self.valor2="texto 2"

    def metodo2(self):
        print("estamos en la Clase Hija")


