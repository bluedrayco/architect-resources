package com.tecno.school.people;

public class ResearcherProfessor extends Professor{
    private String topic;
    private float salary;

    public ResearcherProfessor(String name,int age,Degree degree, String topic, float salary){
        super(name,age,degree);
        this.topic = topic;
        this.salary = salary;
    }

    public void setDegree(Degree degree){
        this.degree = degree;
    }

    public void setSalary(float salary){
        this.salary = salary;
    }

    public String getAllInfo(){
        return this.getInfo()+ ", Topic: "+ this.topic+", Salary: "+this.salary;
    }
}
