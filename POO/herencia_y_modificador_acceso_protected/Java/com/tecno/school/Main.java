package com.tecno.school;

import com.tecno.school.people.Degree;
import com.tecno.school.people.ResearcherProfessor;

public class Main{
    public static void main(String[] args) {
        Degree degree = new Degree("Computer systems engineer");
        ResearcherProfessor researcher = new ResearcherProfessor("Jose Jorge Velazquez Vega", 21, degree, "Hibrid Computers", 20000);
        System.out.println(researcher.getAllInfo());
    }
}