package com.tecno.project;

import com.tecno.project.app.connection.DatabaseConnection;

public class MainSingleton {

    public static void main(String[] args){
        DatabaseConnection connection1 = DatabaseConnection.getInstance();
        connection1.execute("SELECT * FROM USER");

        DatabaseConnection connection2 = DatabaseConnection.getInstance();
        connection2.execute("SELECT * FROM ROLES");
    }
}
