package com.tecno.project;

import com.tecno.project.app.example1.Person;

public class Main {
    public static void main(String[] args) {
        Person person1 = new Person("Roberto","Ramirez",20);
        Person person2 = new Person("Elvia","Rodriguez",33);

        person1.printInfo();
        person2.printInfo();

        System.out.println();
        person1.name = "Guadalupe";

        person1.printInfo();
        person2.printInfo();

        System.out.println();
        person2.age = 12;

        person1.printInfo();
        person2.printInfo();
    }
}