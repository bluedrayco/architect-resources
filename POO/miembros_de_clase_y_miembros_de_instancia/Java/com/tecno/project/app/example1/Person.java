package com.tecno.project.app.example1;

public class Person {
    public String name = "";
    public String lastName = "";
    public int age = 0;

    public Person(String name, String lastName,int age) {
        this.name = name;
        this.lastName = lastName;
        this.age = age;
    }

    public void printInfo(){
        System.out.println("Name: "+name+", Last name: "+lastName+", Age: "+age);
    }
}
