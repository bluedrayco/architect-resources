package com.tecno.project.app.example2;

public class Person {
    public static String name;
    public static String lastName;
    public static int age;

    public static void setValues(String name, String lastName,int age) {
        Person.name = name;
        Person.lastName = lastName;
        Person.age = age;
    }

    public static void printInfo(){
        System.out.println("Name: "+Person.name+", Last name: "+Person.lastName+", Age: "+Person.age);
    }
}
