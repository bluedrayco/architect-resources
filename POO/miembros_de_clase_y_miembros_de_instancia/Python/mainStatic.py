from app.example2.person import Person

Person.setValues("Roberto","Ramirez",20)
Person.printInfo()
Person.setValues("Elvia","Rodriguez",33)
Person.printInfo()

Person.name="Guadalupe"

Person.printInfo()

print()
Person.age = 12

Person.printInfo()