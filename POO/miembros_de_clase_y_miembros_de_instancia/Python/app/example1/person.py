class Person:
    name = ""
    lastName = ""
    age = 0

    def __init__(self,name:str, lastName:str,age:int):
        self.name = name
        self.lastName = lastName
        self.age = age

    def printInfo(self)->None:
        print(f"Name: {self.name}, Last name: {self.lastName}, Age: {self.age}")