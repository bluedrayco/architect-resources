class Person:
    name:str
    lastName:str
    age:str

    @classmethod
    def setValues(cls,name:str,lastName:str,age:int)->None:
        cls.name = name
        cls.lastName = lastName
        cls.age = age
    
    @staticmethod
    def printInfo()->None:
        print(f"Name: {Person.name}, Last name: {Person.lastName}, Age: {Person.age}")
