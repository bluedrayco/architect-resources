class DatabaseConnection:
    __instance: 'DatabaseConnection'=None

    __host: str = ""
    __username: str = ""
    __password: str = ""

    def __init__(self, host: str, username: str, password: str):
        self.host = host
        self.username = username
        self.password = password

    @classmethod
    def getInstance(cls)->'DatabaseConnection':
        if cls.__instance is None:
            cls.__instance = DatabaseConnection("localhost", "admin", "123456")
        return cls.__instance

    def __connect(self)->None:
        print(f"--->Contectando con host:{self.host}, username:{self.username}, password:{self.password}")

    def execute(self,sql:str)->None:
        self.__connect()
        print(f"executando: {sql}")
        self.__close()

    def __close(self)->None:
        print("--->Cerrando conexion...")

