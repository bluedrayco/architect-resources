
from app.example1.person import Person

person1 = Person("Roberto","Ramirez",20)
person2 = Person("Elvia","Rodriguez",33)

person1.printInfo()
person2.printInfo()

print()
person1.name = "Guadalupe"

person1.printInfo()
person2.printInfo()

print()
person2.age = 12

person1.printInfo()
person2.printInfo()
