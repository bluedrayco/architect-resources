<?php

require "./vendor/autoload.php";

use Project\App\Connection\DatabaseConnection;

$connection1 = DatabaseConnection::getInstance();
$connection1->execute("SELECT * FROM USER");

$connection2 = DatabaseConnection::getInstance();
$connection2->execute("SELECT * FROM ROLES");