<?php

namespace Project\App\Example2;

class Person {
    public static $name;
    public static $lastName;
    public static $age;

    public static function setValues(string $name, string $lastName,int $age):void {
        self::$name = $name;
        self::$lastName = $lastName;
        self::$age = $age;
    }

    public static function printInfo():void{
        echo "Name: ".self::$name.", Last name: ".self::$lastName.", Age: ".self::$age."\n";
    }
}
