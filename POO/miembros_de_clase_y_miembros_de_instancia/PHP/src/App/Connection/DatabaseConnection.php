<?php
namespace Project\App\Connection;

class DatabaseConnection {

    private static $instance;

    private $host="";
    private $username="";
    private $password="";

    private function __construct(string $host, string $username, string $password){
        $this->host=$host;
        $this->username=$username;
        $this->password=$password;
    } 

    public static function  getInstance():DatabaseConnection {
        if (self::$instance == null) {
            self::$instance = new DatabaseConnection("localhost", "admin", "123456");
        }
        return DatabaseConnection::$instance;
    }


    private function connect():void{
        echo "--->Contectando con host:".$this->host.", username:" .$this->username.", password:".$this->password."\n";
    }

    public function execute(string $sql):void{
        $this->connect();
        echo "executando: ".$sql."\n";
        $this->close();
    }

    private function close():void{
        echo "--->Cerrando conexion...\n";
    }
}
