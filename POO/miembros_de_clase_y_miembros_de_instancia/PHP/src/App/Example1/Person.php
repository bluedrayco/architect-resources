<?php

namespace Project\App\Example1;

class Person {
    public $name = "";
    public $lastName = "";
    public $age = 0;

    public function __construct(string $name, string $lastName,int $age) {
        $this->name = $name;
        $this->lastName = $lastName;
        $this->age = $age;
    }

    public function printInfo():void{
        echo "Name: ".$this->name.", Last name: ".$this->lastName.", Age: ".$this->age."\n";
    }
}
