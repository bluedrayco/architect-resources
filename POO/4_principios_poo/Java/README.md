# REQUIREMENTS

- virtualbox latest version
- vagrant latest version

# INSTALATION

For create the virtual machine type:

```ssh
vagrant up
```

When the virtual machine is created for enter in the virtual machine and access to code type:

```ssh
vagrant ssh
cd code
```

First we need to compile the file named HelloWorld.java

```ssh
javac HelloWorld.java
```

And finally for execute the code type:

```ssh
java HelloWorld
```

For exit to the virtual machine type **exit** command in the terminal

