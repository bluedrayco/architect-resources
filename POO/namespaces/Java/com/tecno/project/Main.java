package com.tecno.project;

import com.tecno.project.inventory.Product;
import com.tecno.project.login.User;

public class Main{
    public static void main(String[] args) {
        User user = new User("user1","pass1");

        user.purchasedProduct(new Product("Pelota","pelota de playa",200.50));
        user.purchasedProduct(new Product("Videojuego","Gameboy",150.30));

        System.out.println(user.printInfo());
    }
}