from app.login.user import User
from app.inventory.product import Product

user = User("user1","pass1")

user.purchasedProduct(Product("Pelota","pelota de playa",200.50))
user.purchasedProduct(Product("Videojuego","Gameboy",150.30))

print(user.printInfo())
