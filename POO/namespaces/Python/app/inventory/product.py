class Product:
    title:str
    content:str
    price:float

    def __init__(self, title:str,content:str,price:float):
        self.title=title
        self.content=content
        self.price=price
    
    def printProductInfo(self)->str:
        return f"Title: {self.title}, Content: {self.content}, Price: {self.price}"