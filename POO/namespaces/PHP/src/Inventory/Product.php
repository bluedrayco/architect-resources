<?php

namespace Project\Inventory;

class Product{
    public $title = null;
    public $content = null;
    public $price = 0.0;

    public function __construct(string $title,string $content,float $price){
        $this->title=$title;
        $this->content=$content;
        $this->price = $price;
    }

    public function printProductInfo():string{
        return "Title: {$this->title}, Content: {$this->content}, Price: {$this->price}";
    }
}