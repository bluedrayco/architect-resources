<?php
namespace Project\Login;

class Role{
    public $role="";

    public function __construct(string $role){
        $this->role=$role;
    }

    public function printRole():string{
        return "Rol:{$this->role}";
    }
}
