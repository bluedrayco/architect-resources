<?php

namespace Project\Login;

use Project\Login\Role;
use Project\Inventory\Product;

class User{
    public $purchasedProducts=null;
    public $role=null;
    public $username=null;
    public $password=null;

    public function __construct(string $username,string $password){
        $this->username = $username;
        $this->password=$password;
        $this->purchasedProducts=[];
        $this->role = new Role("client");
    }

    public function purchasedProduct(Product $product):void{
        $this->purchasedProducts[]=$product;
    }

    public function printInfo():string{
        $information ="The user {$this->username} with role ".$this->role->printRole(). " has the following products:\n";
        foreach ($this->purchasedProducts as $product){
            $information.="product: ".$product->printProductInfo()."\n";
        }
        return $information;
    }
}