<?php

require "vendor/autoload.php";

use Project\Login\User;
use Project\Inventory\Product;

$user = new User("user1","pass1");

$user->purchasedProduct(new Product("Pelota","pelota de playa",200.50));
$user->purchasedProduct(new Product("Videojuego","Gameboy",150.30));

echo $user->printInfo();