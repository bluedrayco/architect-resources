<?php

class Email
{
    public $from = [];
    public $subject = null;
    public $body = null;

    public function changeSubject(string $subject):void
    {
        $this->subject = $subject;
    }

    public function changeBody(string $body):void
    {
        $this->body = $body;
    }
}

class Gmail
{
    public $name = "Gmail";
}

class Hotmail
{
    public $name = "Hotmail";
}

class EmailManagment
{
    public $emailProvider = null;

    public function send(array $to, string $text):int
    {
        $emails = implode(",", $to);
        echo "email para: {$emails} con texto: {$text} con el proveedor de email: {$this->emailProvider->name}";
        return 0;
    }

    public function checkInbox():array
    {
        $emails=[];
        // $emails[0]=new Email();
        // $emails[0]->from=["email1@gmail.com","email2@hotmail.com"];
        // $emails[0]->changeSubject("subject 1");
        // $emails[0]->changeBody("Hola como estas?");
        // $emails[1]=new Email();
        // $emails[1]->from=["email1@gmail.com","email2@hotmail.com"];
        // $emails[1]->changeSubject("subject 2");
        // $emails[1]->changeBody("Hola como estas?");
        // $emails[2]=new Email();
        // $emails[2]->from=["email1@gmail.com","email2@hotmail.com"];
        // $emails[2]->changeSubject("subject 3");
        // $emails[2]->changeBody("Hola como estas?");
        
        $data = [
            [
                "emails"=>["email1@gmail.com","email2@hotmail.com"],
                "subject"=>"subject 1",
                "body"=>"Hola como estas?"
            ],
            [
                "emails"=>["email1@gmail.com","email2@hotmail.com"],
                "subject"=>"subject 2",
                "body"=>"Hola como estas?"
            ],
            [
                "emails"=>["email1@gmail.com","email2@hotmail.com"],
                "subject"=>"subject 3",
                "body"=>"Hola como estas?"
            ]
        ];

        // foreach ($data as $element){
        //     $email=new Email();
        //     $email->from=$element['emails'];
        //     $email->changeSubject($element['subject']);
        //     $email->changeBody($element['body']);
        //     $emails[]=$email;
        // }

        for ($x =0; $x<count($data); $x++) {
            $email=new Email();
            $email->from=$data[$x]['emails'];
            $email->changeSubject($data[$x]['subject']);
            $email->changeBody($data[$x]['body']);
            $emails[$x]=$email;
        }
        return $emails;
    }
}


$emailManagment = new EmailManagment();
$emailManagment->emailProvider = new Hotmail();

print_r($emailManagment->checkInbox());

$resultado = $emailManagment->send(["email1@hispavista.com","email2@yahoo.com"], "saludos!!!");

echo "\nresultado: {$resultado}";
