import json 

class Email:
    from2 = []
    subject  = None
    body = None

    def changeSubject(self, subject: str):
        self.subject = subject

    def changeBody(self, body: str):
        self.body = body


class Gmail:
    name = "gmail"


class Hotmail:
    name = "hotmail"


class EmailManagment:
    emailProvider = None

    def send(self, to: list, text: str) -> int:
        emails = ",".join(to)
        print(
            f"email para: {emails} con texto: {text} con el proveedor de email: {self.emailProvider.name}")
        return 0

    def checkInbox(self) -> dict:
        emails = []
        emailTmp = Email()
        emailTmp.from2 = ["email1@gmail.com", "email2@gmail.com"]
        emailTmp.changeSubject("asunto 1")
        emailTmp.changeBody("hola como estas?")
        emails.append(emailTmp)
        emailTmp = Email()
        emailTmp.from2 = ["email1@gmail.com", "email2@gmail.com"]
        emailTmp.changeSubject("asunto 2")
        emailTmp.changeBody("hola como estas?")
        emails.append(emailTmp)
        emailTmp = Email()
        emailTmp.from2 = ["email1@gmail.com",
            "email2@gmail.com", "email3@gmail.com"]
        emailTmp.changeSubject("asunto 3")
        emailTmp.changeBody("hola como estas?")
        emails.append(emailTmp)

        data = [
            {
                "emails" : ["email1@gmail.com", "email2@gmail.com"],
                "subject": "asunto 1",
                "body": "hola como estas?"
            },
            {
                "emails": ["email1@gmail.com", "email2@gmail.com"],
                "subject": "asunto 1",
                "body": "hola como estas?"
            },
            {
                "emails": ["email1@gmail.com", "email2@gmail.com"],
                "subject": "asunto 1",
                "body": "hola como estas?"
            }
        ]

        for element in data:
            email = Email()
            email.from2 = element['emails']
            email.changeSubject(element['subject'])
            email.changeBody(element['body'])
            emails.append(email)

        return emails


emailManagment = EmailManagment()
emailManagment.emailProvider = Hotmail()

emails = emailManagment.checkInbox()
print(json.dumps(emails,default=lambda obj: obj.__dict__,sort_keys=True,indent=4))


resultado = emailManagment.send(
    ["email1@gmail.com", "email2@gmail.com"], "saludos!!!")

print(f"\nresultado: {resultado}")
