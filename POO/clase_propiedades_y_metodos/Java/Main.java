import java.util.Arrays;

class Email{
    public String[] from=null;
    public String subject = null;
    public String body = null;

    public void changeSubject(String subject){
        this.subject = subject;
    }

    public void changeBody (String body){
        this.body = body;
    }
}

class Gmail{
    public String name = "Gmail";
}

class Hotmail{
    public String name = "Hotmail";
}

class EmailManagment{
    public Hotmail emailProvider=null;

    public int send(String[] to, String text){
        String emails = String.join(",",to);
        System.out.println("email para: "+emails+" con texto: "+text+" con el proveedor de email: "+this.emailProvider.name);
        return 0;
    }

    public Email[] checkInbox(){
        // Email[] emails = new Email[3];
        // emails[0]= new Email();
        // emails[0].from = new String[]{"email1@gmail.com","email2@hispavista.com"};
        // emails[0].changeSubject("subject 1");
        // emails[0].changeBody("saludos a todos!!!");
        // emails[1]= new Email();
        // emails[1].from = new String[]{"email1@gmail.com","email2@hispavista.com"};
        // emails[1].changeSubject("subject 2");
        // emails[1].changeBody("saludos a todos!!!");  
        // emails[2]= new Email();
        // emails[2].from = new String[]{"email1@gmail.com","email2@hispavista.com"};
        // emails[2].changeSubject("subject 3");
        // emails[2].changeBody("saludos a todos!!!");   

        String[][] emailTexts = new String[][]{{"email1@gmail.com","email2@gmail.com"},{"email1@gmail.com","email2@gmail.com"},{"email3@gmail.com","email4@gmail.com"}};
        String[]   subjects = new String[]{"asunto 1","asunto 2", "asunto 3"};
        String[]   bodies = new String[]{"hola como estas?","hola como estas?", "hola como estas?"};
        
        Email[] emails=new Email[3];

        for (int x=0; x<emails.length;x++) {
            Email email = new Email();
            email.from=emailTexts[x];
            email.changeSubject(subjects[x]);
            email.changeBody(bodies[x]);
            emails[x]= email;
        }

        return emails;
    }
}

class Main{

    public static void main(String[] args){
        EmailManagment emailManagment = new EmailManagment();
        emailManagment.emailProvider = new Hotmail();

        System.out.println(Arrays.toString(emailManagment.checkInbox()));

        int resultado = emailManagment.send(new String[]{"email1@gmail.com","email2@hotmail.com"}, "saludos!!!");

        System.out.println("resultado: "+resultado);
    }
}