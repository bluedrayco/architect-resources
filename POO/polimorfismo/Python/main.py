
from app.business.store_user import StoreUser
from app.model.user import User
from app.service.store.database import Database
from app.service.store.s3 import S3
from app.service.store.file_system import FileSystem
from app.business.user_process_manager import UserProcessManager
from app.business.send_user_queue import SendUserQueue
from app.service.parser.json import JSON
from app.service.parser.xml import XML
from app.business.user_process import UserProcess
from app.business.sync_user_bank import SyncUserBank



user = User("Roberto","Jimenez",18)


storeUserProcessDatabase = StoreUser(Database("localhost","user","pass"))
storeUserProcessS3 = StoreUser(S3("12312312213","asdasdaqeeq","mybucket"))
storeUserProcessFileSystem = StoreUser(FileSystem("/tmp"))


sendUserQueueWithJSON = SendUserQueue("rabbitmq1.com","rabbit1","pass1",JSON())
sendUserQueueWithXML = SendUserQueue("rabbitmq2.com","rabbit2","pass2",XML())

sendUserToBank = SyncUserBank("banamex.com","2342342423423")

userProcessManager = UserProcessManager(user)
userProcessManager.enqueueProcess(storeUserProcessDatabase)
userProcessManager.enqueueProcess(storeUserProcessS3)
userProcessManager.enqueueProcess(storeUserProcessFileSystem)
userProcessManager.enqueueProcess(sendUserQueueWithJSON)
userProcessManager.enqueueProcess(sendUserQueueWithXML)
userProcessManager.enqueueProcess(sendUserToBank)


userProcessList = userProcessManager.listItems()

for userProcess in userProcessList:
    print(userProcess)


print("\n----Print Items----\n")
userProcessManager.printItems()

print("\n----Executing queue----\n")
userProcessManager.executeList()