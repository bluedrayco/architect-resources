from app.business.user_process import UserProcess
from app.service.store.store_driver import StoreDriver
from app.model.user import User


class SyncUserBank(UserProcess):
    __url: str
    __token: str

    def __init__(self, url: str, token: str):
        self.__url = url
        self.__token = token

    def execute(self, user: User) -> None:
        json = {
            "name": user.getName(),
            "lastName": user.getLastName(),
            "age": user.getAge()
        }
        print(
            f"send information to: {self.__url}/user with token: {self.__token}: {json}")

    def __str__(self) -> str:
        return "SyncUserBank process.\n"
