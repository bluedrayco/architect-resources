
from app.model.user import User
from app.business.user_process import UserProcess

class UserProcessManager:
    __queue:list
    __user:User

    def __init__(self,user:User):
        self.__user = user
        self.__queue = list()

    def enqueueProcess(self,process:UserProcess)->None:
        self.__queue.append(process)
    
    def listItems(self)->list:
        return self.__queue

    def printItems(self)->None:
        for element in self.__queue:
            print(element)

    def executeList(self)->None:
        for element in self.__queue:
            print()
            element.execute(self.__user)
            print()