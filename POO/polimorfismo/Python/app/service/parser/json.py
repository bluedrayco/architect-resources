
from app.service.parser.user_parser import UserParser
from app.model.user import User


class JSON (UserParser):

    def parse(self, user: User) -> str:
        return str({
            "name": user.getName(),
            "lastName": user.getLastName(),
            "age": user.getAge()
        })
