
from app.service.parser.user_parser import UserParser
from app.model.user import User

class XML(UserParser):

    def parse(self,user:User)->str:
        return f"""<root>
                <name>{user.getName()}</name>
                <lastName>{user.getLastName()}</lastName>
                <age>{user.getAge()}</age>
            </root>"""
