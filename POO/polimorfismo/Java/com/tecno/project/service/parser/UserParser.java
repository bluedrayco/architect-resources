package com.tecno.project.service.parser;

import com.tecno.project.model.User;

public interface UserParser {
    public String parse(User user);
}