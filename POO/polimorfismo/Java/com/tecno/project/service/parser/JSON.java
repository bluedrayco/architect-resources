package com.tecno.project.service.parser;

import com.tecno.project.service.parser.UserParser;
import com.tecno.project.model.User;

public class JSON implements UserParser{

    public String parse(User user){
        return "{"
        +"\"name\":\""+user.getName()+"\","
        +"\"lastName\":"+user.getLastName()+"\","
        +"\"age\":\""+user.getAge()+"\"}";
    }
}