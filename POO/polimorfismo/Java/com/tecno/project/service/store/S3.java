package com.tecno.project.service.store;

import com.tecno.project.service.store.StoreDriver;
import com.tecno.project.model.User;

public class S3 implements StoreDriver {
    private String clientId;
    private String secret;
    private String bucket;

    public S3(String clientId, String secret, String bucket) {
        this.clientId = clientId;
        this.secret = secret;
        this.bucket = bucket;
    }

    public void open() {
        System.out.println("abriendo conexion a s3: " + this.clientId + ", " + this.secret);
    }

    public void store(User user){
        String json = "{"
            +"\"name\":\""+user.getName()+"\","
            +"\"lastName\":"+user.getLastName()+"\","
            +"\"age\":\""+user.getAge()+"\"}";
        System.out.println("guardando user en el bucket:"+this.bucket+": "+json);
    }

    public void close(){
        System.out.println("cerrando conexion a s3");
    }
}