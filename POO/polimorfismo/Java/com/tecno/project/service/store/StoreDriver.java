package com.tecno.project.service.store;

import com.tecno.project.model.User;

public interface StoreDriver{
    public void open();
    public void store(User user);
    public void close();
}