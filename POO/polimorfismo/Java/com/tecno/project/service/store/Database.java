package com.tecno.project.service.store;

import com.tecno.project.service.store.StoreDriver;
import com.tecno.project.model.User;

public class Database implements StoreDriver {
    private String host;
    private String username;
    private String password;

    public Database(String host, String username, String password) {
        this.host = host;
        this.username = username;
        this.password = password;
    }

    public void open() {
        System.out.println("abriendo conexion: "+this.host+":"+this.username+"@"+this.password);
    }

    public void store(User user) {
        System.out.println("INSERT INTO USER(" + user.getName() + "," + user.getLastName() + "," + user.getAge() + ")");
    }

    public void close() {
        System.out.println("cerrando conexion de la base de datos");
    }
}