package com.tecno.project.business;

import com.tecno.project.model.User;
import com.tecno.project.business.UserProcess;
import java.util.ArrayList;
import java.util.List;

public class UserProcessManager {
    private List<UserProcess> queue;
    private User user;

    public UserProcessManager(User user) {
        this.user = user;
        this.queue = new ArrayList<UserProcess>();
    }

    public void enqueueProcess(UserProcess process) {
        this.queue.add(process);
    }

    public UserProcess[] listItems() {
        UserProcess[] processes = new UserProcess[this.queue.size()];
        processes = this.queue.toArray(processes);
        return processes;
    }

    public void printItems() {
        UserProcess[] processes = new UserProcess[this.queue.size()];
        processes = this.queue.toArray(processes);
        for (UserProcess element : processes) {
            System.out.println(element);
        }
    }

    public void executeList() {
        UserProcess[] processes = new UserProcess[this.queue.size()];
        processes = this.queue.toArray(processes);
        for (UserProcess element : processes) {
            System.out.println();
            element.execute(this.user);
            System.out.println();
        }
    }

}