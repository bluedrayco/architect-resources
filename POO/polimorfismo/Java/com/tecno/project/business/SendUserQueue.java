package com.tecno.project.business;

import com.tecno.project.business.UserProcess;
import com.tecno.project.service.parser.UserParser;
import com.tecno.project.model.User;

public class SendUserQueue implements UserProcess {
    private String host;
    private String username;
    private String password;
    private UserParser parser;

    public SendUserQueue(String host, String username, String password, UserParser parser) {
        this.host = host;
        this.username = username;
        this.password = password;
        this.parser = parser;
    }

    public void execute(User user) {
        String userParsed = this.parser.parse(user);
        System.out.println("Executing SendUserQueue process...");
        this.open();
        this.send(userParsed);
        this.close();
    }

    public String toString() {
        return "---SendUserQueue process.\n";
    }

    private void open() {
        System.out.println("conectando a RabbitMQ -> " + this.host + ":" + this.username + "@" + this.password);
    }

    private void send(String userParsed) {
        System.out.println("sending data: " + userParsed);
    }

    private void close() {
        System.out.println("cerrando conexion en RabbitMQ");
    }
}