package com.tecno.project.business;

import com.tecno.project.business.UserProcess;
import com.tecno.project.service.store.StoreDriver;
import com.tecno.project.model.User;

public class StoreUser implements UserProcess {
    private StoreDriver driver;

    public StoreUser(StoreDriver driver) {
        this.driver = driver;
    }

    public void execute(User user) {
        System.out.println("Executing StoreUser process...\n");
        this.driver.open();
        this.driver.store(user);
        this.driver.close();
    }

    public String toString() {
        return "+++StoreUser process.\n";
    }
}