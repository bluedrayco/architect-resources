package com.tecno.project.business;

import com.tecno.project.model.User;

public interface UserProcess {
    public void execute(User user);
    public String toString();
}