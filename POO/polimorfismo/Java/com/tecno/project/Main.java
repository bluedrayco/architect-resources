package com.tecno.project;

import com.tecno.project.business.StoreUser;
import com.tecno.project.model.User;
import com.tecno.project.service.store.Database;
import com.tecno.project.service.store.S3;
import com.tecno.project.service.store.FileSystem;
import com.tecno.project.business.UserProcessManager;
import com.tecno.project.business.SendUserQueue;
import com.tecno.project.service.parser.JSON;
import com.tecno.project.service.parser.XML;
import com.tecno.project.business.UserProcess;
import com.tecno.project.business.SyncUserBank;

public class Main {
    public static void main(String[] args) {

        User user = new User("Roberto","Jimenez",18);


        UserProcess storeUserProcessDatabase = new StoreUser(new Database("localhost","user","pass"));
        UserProcess storeUserProcessS3 = new StoreUser(new S3("12312312213","asdasdaqeeq","mybucket"));
        UserProcess storeUserProcessFileSystem = new StoreUser(new FileSystem("/tmp"));
        
        
        UserProcess sendUserQueueWithJSON = new SendUserQueue("rabbitmq1.com","rabbit1","pass1",new JSON());
        UserProcess sendUserQueueWithXML = new SendUserQueue("rabbitmq2.com","rabbit2","pass2",new XML());
        
        UserProcess sendUserToBank = (UserProcess)(new SyncUserBank("banamex.com","2342342423423"));
        
        UserProcessManager userProcessManager = new UserProcessManager(user);
        userProcessManager.enqueueProcess(storeUserProcessDatabase);
        userProcessManager.enqueueProcess(storeUserProcessS3);
        userProcessManager.enqueueProcess(storeUserProcessFileSystem);
        userProcessManager.enqueueProcess(sendUserQueueWithJSON);
        userProcessManager.enqueueProcess(sendUserQueueWithXML);
        userProcessManager.enqueueProcess(sendUserToBank);
        
        
        UserProcess[] userProcessList = userProcessManager.listItems();
        
        for (int i=0;i<userProcessList.length; i++){
            System.out.println(userProcessList[i]);
        }
        
        System.out.println("\n----Print Items----\n");
        userProcessManager.printItems();
        
        System.out.println("\n----Executing queue----\n");
        userProcessManager.executeList();

    }
}











