#!/usr/bin/env bash

apt-get update
apt-get install -y software-properties-common curl
add-apt-repository ppa:ondrej/php
apt-get update
apt-get install -y php7.4 php7.4-xml

curl -sS https://getcomposer.org/installer -o composer-setup.php

php composer-setup.php --install-dir=/usr/local/bin --filename=composer

rm -rf composer-setup.php