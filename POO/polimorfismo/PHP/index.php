<?php

require "./vendor/autoload.php";
use Project\Business\StoreUser;
use Project\Model\User;
use Project\Service\Store\Database;
use Project\Service\Store\S3;
use Project\Service\Store\FileSystem;
use Project\Business\UserProcessManager;
use Project\Business\SendUserQueue;
use Project\Service\Parser\JSON;
use Project\Service\Parser\XML;
use Project\Business\UserProcess;
use Project\Business\SyncUserBank;



$user = new User("Roberto","Jimenez",18);


$storeUserProcessDatabase = new StoreUser(new Database("localhost","user","pass"));
$storeUserProcessS3 = new StoreUser(new S3("12312312213","asdasdaqeeq","mybucket"));
$storeUserProcessFileSystem = new StoreUser(new FileSystem("/tmp"));


$sendUserQueueWithJSON = new SendUserQueue("rabbitmq1.com","rabbit1","pass1",new JSON());
$sendUserQueueWithXML = new SendUserQueue("rabbitmq2.com","rabbit2","pass2",new XML());

$sendUserToBank = new SyncUserBank("banamex.com","2342342423423");

$userProcessManager = new UserProcessManager($user);
$userProcessManager->enqueueProcess($storeUserProcessDatabase);
$userProcessManager->enqueueProcess($storeUserProcessS3);
$userProcessManager->enqueueProcess($storeUserProcessFileSystem);
$userProcessManager->enqueueProcess($sendUserQueueWithJSON);
$userProcessManager->enqueueProcess($sendUserQueueWithXML);
$userProcessManager->enqueueProcess($sendUserToBank);


$userProcessList = $userProcessManager->listItems();

foreach ($userProcessList as $userProcess){
    echo $userProcess;
}

echo "\n\n----Print Items----\n\n";
$userProcessManager->printItems();

echo "\n\n----Executing queue----\n\n";
$userProcessManager->executeList();





