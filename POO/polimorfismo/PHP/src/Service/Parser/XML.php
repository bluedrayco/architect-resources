<?php

namespace Project\Service\Parser;
use Project\Service\Parser\UserParser;
use Project\Model\User;

class XML implements UserParser{

    public function parse(User $user):string{
        return "<root>\n".
            "    <name>{$user->getName()}</name>\n".
            "    <lastName>{$user->getLastName()}</lastName>\n".
            "    <age>{$user->getAge()}</age>\n".
            "</root>";
    }
}