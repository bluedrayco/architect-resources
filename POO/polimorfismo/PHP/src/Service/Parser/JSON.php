<?php

namespace Project\Service\Parser;
use Project\Service\Parser\UserParser;
use Project\Model\User;

class JSON implements UserParser{

    public function parse(User $user):string{
        return json_encode([
            "name"=>$user->getName(),
            "lastName"=>$user->getLastName(),
            "age"=>$user->getAge()
        ]);
    }
}