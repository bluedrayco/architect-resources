<?php

namespace Project\Service\Parser;
use Project\Model\User;

interface UserParser{
    public function parse(User $user):string;
}