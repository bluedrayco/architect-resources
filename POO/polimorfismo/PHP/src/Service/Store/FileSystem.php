<?php

namespace Project\Service\Store;
use Project\Service\Store\StoreDriver;
use Project\Model\User;

class FileSystem implements StoreDriver{
    private string $directory;

    public function __construct(string $directory){
        $this->directory=$directory;
    }
    public function open():void{
        echo "abriendo carpeta: {$this->directory}\n";
    }
    public function store(User $user):void{
        $json = json_encode([
            "name"=> $user->getName(),
            "lastName"=>$user->getLastName(),
            "age"=>$user->getAge()
        ]);
        echo "Almacenando user: {$json}\n";
    }
    public function close():void{
        echo "cerrando carpeta\n";
    }
}