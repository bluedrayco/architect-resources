<?php

namespace Project\Service\Store;
use Project\Service\Store\StoreDriver;
use Project\Model\User;

class S3 implements StoreDriver{
    private string $clientId;
    private string $secret;
    private string $bucket;

    public function __construct(string $clientId,string $secret,string $bucket){
        $this->clientId = $clientId;
        $this->secret = $secret;
        $this->bucket = $bucket;
    }
    public function open():void{
        echo "abriendo conexion a s3: {$this->clientId}, {$this->secret}\n";
    }
    public function store(User $user):void{
        $json = json_encode([
            "name"=> $user->getName(),
            "lastName"=>$user->getLastName(),
            "age"=>$user->getAge()
        ]);
        echo "guardando user en el bucket:{$this->bucket}: {$json}\n";
    }
    public function close():void{
        echo "cerrando conexion a s3\n";
    }
}