<?php

namespace Project\Service\Store;
use Project\Model\User;

interface StoreDriver{
    public function open():void;
    public function store(User $user):void;
    public function close():void;
}