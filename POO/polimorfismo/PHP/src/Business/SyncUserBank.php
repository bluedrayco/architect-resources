<?php

namespace Project\Business;

use Project\Business\UserProcess;
use Project\Service\Store\StoreDriver;
use Project\Model\User;

class SyncUserBank implements UserProcess
{
    private string $url;
    private string $token;

    public function __construct(string $url, string $token){
        $this->url = $url;
        $this->token = $token;
    }

    public function execute(User $user):void{
        $json = json_encode([
            "name"=>$user->getName(),
            "lastName"=>$user->getLastName(),
            "age"=>$user->getAge()
        ]);
        echo "send information to: {$this->url}/user with token: {$this->token}: {$json}\n";
    }

    public function __toString():string{
        return "SyncUserBank process.\n";
    }
}
