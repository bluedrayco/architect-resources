<?php

namespace Project\Business;
use Project\Model\User;
use Project\Business\UserProcess;

class UserProcessManager{
    private array $queue;
    private User $user;

    public function __construct(User $user){
        $this->user = $user;
    }

    public function enqueueProcess(UserProcess $process):void{
        $this->queue[]=$process;
    }

    public function listItems():array{
        return $this->queue;
    }

    public function printItems():void{
        foreach($this->queue as $element){
            echo $element;
        }
    }

    public function executeList():void{
        foreach($this->queue as $element){
            echo "\n";
            $element->execute($this->user);
            echo "\n";
        }
    }

}