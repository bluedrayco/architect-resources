<?php 

namespace Project\Business;
use Project\Model\User;

interface UserProcess{
    public function execute(User $user):void;
    public function __toString():string;
}