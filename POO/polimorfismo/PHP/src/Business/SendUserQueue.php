<?php

namespace Project\Business;
use Project\Business\UserProcess;
use Project\Service\Parser\UserParser;
use Project\Model\User;

class SendUserQueue implements UserProcess{
    private string $host;
    private string $username;
    private string $password;
    private UserParser $parser;

    public function __construct(string $host, string $username, string $password, UserParser $parser){
        $this->host=$host;
        $this->username=$username;
        $this->password=$password;
        $this->parser = $parser;
    }
    public function execute(User $user):void{
        $userParsed = $this->parser->parse($user);
        echo "Executing SendUserQueue process...\n";
        $this->open();
        $this->send($userParsed);
        $this->close();
    }
    public function __toString():string{
        return "---SendUserQueue process.\n";
    }
    private function open():void{
        echo "conectando a RabbitMQ -> {$this->host}:{$this->username}@{$this->password}\n";
    }
    private function send(string $userParsed):void{
        echo "sending data: {$userParsed}\n";
    }
    private function close():void{
        echo "cerrando conexion en RabbitMQ\n";
    }
}