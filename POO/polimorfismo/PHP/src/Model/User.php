<?php

namespace Project\Model;

class User{
    private string $name;
    private string $lastName;
    private int $age;

    public function __construct(string $name, string $lastName, int $age){
        $this->name=$name;
        $this->lastName = $lastName;
        $this->age = $age;
    }

    public function getName(){
        return $this->name;
    }

    public function getLastName(){
        return $this->lastName;
    }

    public function getAge(){
        return $this->age;
    }
}