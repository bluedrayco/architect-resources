<?php

namespace Project\Models;

class User{
    private string $name;
    private string $lastName;
    private int $age;

    public function __construct(string $name,string $lastName,int $age){
        $this->name=$name;
        $this->lastName=$lastName;
        $this->age=$age;
    }

    public function getName():string{
        return $this->name;
    }

    public function getLastName():string{
        return $this->lastName;
    }

    public function getAge():int{
        return $this->age;
    }
}