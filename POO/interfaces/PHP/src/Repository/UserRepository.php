<?php

namespace Project\Repository;
use Project\Models\User;

interface UserRepository{
    public function open():void;
    public function store(User $user):void;
    public function close():void;
}