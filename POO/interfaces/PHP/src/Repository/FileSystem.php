<?php

namespace Project\Repository;
use Project\Repository\UserRepository;
use Project\Models\User;
use SimpleXMLElement;


class FileSystem implements UserRepository{
    private string $directory;

    public function __construct(string $directory){
        $this->directory=$directory;
    }
    public function open():void{
        echo "Opening directory: {$this->directory}\n";
    }
    public function store(User $user):void{
        $userXML=new SimpleXMLElement('<root/>');
        $userElements = [
            "name"=>$user->getName(),
            "lastName"=>$user->getLastName(),
            "age"=>$user->getAge()
        ];
        foreach ($userElements as $key=>$value){
            $userXML->addChild($key,$value);
        }
        echo "Storing user in file :{$this->directory}/{$user->getName()}\n";
        echo "{$userXML->asXML()}";
    }
    public function close():void{
        echo "Closing file\n";
    }
}