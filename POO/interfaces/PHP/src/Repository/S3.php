<?php

namespace Project\Repository;
use Project\Repository\UserRepository;
use Project\Models\User;


class S3 implements UserRepository{
    private string $clientId;
    private string $secretKey;
    private string $bucket;

    public function __construct(string $clientId,string $secretKey,string $bucket){
        $this->clientId=$clientId;
        $this->secretKey=$secretKey;
        $this->bucket=$bucket;
    }
    public function open():void{
        echo "Opening connection to AWS S3 {$this->clientId}:{$this->secretKey}\n";
    }
    public function store(User $user):void{
        $user=json_encode([
            "name"=>$user->getName(),
            "lastName"=>$user->getLastName(),
            "age"=>$user->getAge()
        ]);
        echo "Storing user from bucket:{$this->bucket}: {$user}\n";
    }
    public function close():void{
        echo "Closing connection from AWS S3\n";
    }
}