package com.tecno.project.repository;

import com.tecno.project.repository.UserRepository;
import com.tecno.project.models.User;

public class FileSystem implements UserRepository {
    private String directory;

    public FileSystem(String directory) {
        this.directory = directory;
    }

    public void open() {
        System.out.println("Opening directory: " + this.directory);
    }

    public void store(User user) {
        String xml = "<root><name>" + user.getName() + "</name><lastName>" + user.getLastName() + "</lastName><age>"
                + user.getAge() + "</age></root>";
        System.out.println("Storing user in file :" + this.directory + "/" + user.getName());
        System.out.println(xml);
    }

    public void close() {
        System.out.println("Closing file");
    }
}