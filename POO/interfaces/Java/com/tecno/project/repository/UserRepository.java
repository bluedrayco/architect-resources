package com.tecno.project.repository;

import com.tecno.project.models.User;

public interface UserRepository {
    public void open();

    public void store(User user);

    public void close();
}