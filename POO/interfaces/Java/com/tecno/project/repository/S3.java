package com.tecno.project.repository;

import com.tecno.project.repository.UserRepository;
import com.tecno.project.models.User;

public class S3 implements UserRepository {
    private String clientId;
    private String secretKey;
    private String bucket;

    public S3(String clientId, String secretKey, String bucket) {
        this.clientId = clientId;
        this.secretKey = secretKey;
        this.bucket = bucket;
    }

    public void open() {
        System.out.println("Opening connection to AWS S3 " + this.clientId + ":" + this.secretKey);
    }

    public void store(User user) {
        String userJson = "{\n" + "name:" + user.getName() + ",\n" + "lastName:" + user.getLastName() + ",\n" + "age:"
                + user.getAge() + "\n" + "}";
        System.out.println("Storing user from bucket:" + this.bucket + ":\n" + userJson);
    }

    public void close() {
        System.out.println("Closing connection from AWS S3");
    }
}