package com.tecno.project.repository;

import com.tecno.project.repository.UserRepository;
import com.tecno.project.models.User;

public class Database implements UserRepository {
    private String host;
    private String user;
    private String password;

    public Database(String host, String user, String password) {
        this.host = host;
        this.user = user;
        this.password = password;
    }

    public void open() {
        System.out.println("Opening database connection: " + this.host + ":" + this.user + "@" + this.password);
    }

    public void store(User user) {
        System.out.println("Storing user in the database " + user.getName());
        System.out.println(
                "INSERT INTO USER VALUES('" + user.getName() + ",'" + user.getLastName() + "'," + user.getAge() + ")");
    }

    public void close() {
        System.out.println("Closing connection");
    }
}